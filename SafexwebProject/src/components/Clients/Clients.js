import React from "react";

import "assets/css/safexpay-web.css"
import "./Clients.css";

class Clients extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Clients mt-0 pb-5 mb-5">
                    <div className="container">

                        <h2 className="section-heading" data-aos="fade-up" data-aos-duration="1000">Thousands OF Business Trust Us</h2>
                        <div className="justify-content-center">
                        <div className="dv-logo" data-aos="fade-up" data-aos-duration="2000">
                            <img
                                alt=""
                                className="img-fluid bd-rght bd-bot"
                                src={require("assets/img/home/shell.png")}
                            />
                            <img
                                alt="Telr"
                                className="img-fluid bd-rght bd-bot"
                                src={require("assets/img/home/telr.png")}
                            />
                            <img
                                alt="d-Local"
                                className="img-fluid bd-rght bd-bot no-bd-rght67"
                                src={require("assets/img/home/dlocal.png")}
                            />
                            <img
                                alt="ZipGrid"
                                className="img-fluid bd-rght bd-bot no-bd-rght91 yes-bd-rght67"
                                src={require("assets/img/home/zipgrid.png")}
                            />
                            <img
                                alt="PC Jeweller"
                                className="img-fluid bd-rght bd-bot"
                                src={require("assets/img/home/pcj.png")}
                            />
                            <img
                                alt="Melorra"
                                className="img-fluid bd-bot yes-bd-rght91 no-bd-rght67"
                                src={require("assets/img/home/melorra.png")}
                            />
                            <img
                                alt="Commomn Folks"
                                className="img-fluid bd-rght yes-bd-bot91"
                                src={require("assets/img/home/commonfolks.png")}
                            />
                            <img
                                alt="CashFree"
                                className="img-fluid bd-rght no-bd-rght91 yes-bd-bot91 yes-bd-rght67"
                                src={require("assets/img/home/cashfree.png")}
                            />
                            <img
                                alt="mSwipe"
                                className="img-fluid bd-rght mswip no-bd-rght67 yes-bd-bot67"
                                src={require("assets/img/home/mswip.png")}
                            />
                            <img
                                alt="i-Money"
                                className="img-fluid bd-rght imoney"
                                src={require("assets/img/home/imoney.jpg")}
                            />
                            <img
                                alt="Quick Work"
                                className="img-fluid bd-rght quickw"
                                src={require("assets/img/home/quickw.png")}
                            />
                            <img
                                alt="MSME"
                                className="img-fluid"
                                src={require("assets/img/home/msme.png")}
                            />
                        </div>
                        </div>
                        </div>
                    
                </section>

            </>
        );
    }
}

export default Clients;
