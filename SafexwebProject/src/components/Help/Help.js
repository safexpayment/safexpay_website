import React from "react";

import "assets/css/safexpay-web.css"
import "./Help.css";
import Contactuspagenew from "components/Contactuspagehelp/Contactuspagehelp";




class Help extends React.Component {
    state={
        open1:false
    }

    showcontactuspage1 =(props)=>{
        console.log("button click.....");
        this.setState({
            open1:!this.state.open1
        })

    }

    closepage1=()=>{
        this.setState({
            open1:!this.state.open1
        })
    }


    render() {
        return (
            <>
                <Contactuspagenew open={this.state.open1} click={this.closepage1}/>
                <section className="dv-Help mt-0">
                    <div className="container">
                        <div className="row dv-call mt-4 mx-0">
                            <div className="col-md-7 col-12 text-md-left text-center" data-aos="fade-up" data-aos-duration="1000">
                                <h3 className="section-heading pb-0 text-md-left text-center">Ready to boost your business with Safexpay?</h3>
                                <p className="my-5">Need more information to help you decide, call our helpline or drop us a note and we will get back to you</p>
                                <div className="dv-btn my-3">
                                   <button className="btn btn-blue-rounded" onClick={this.showcontactuspage1}>TALK TO US</button>
                                    {/* <button className="btn btn-blue-rounded">Call Me Later</button> */}
                                </div>
                            </div>
                            <div className="col-md-5 col-12 text-md-left text-center">
                                <img
                                    alt="Customer Care"
                                    className="img-fluid img-call"
                                    src={require("assets/img/section/call.png")}
                                    data-aos="fade-up" data-aos-duration="1000"
                                /> 
                            </div>
                        </div>
                    </div>
                </section>

            </>
        );
    }
}

export default Help;
