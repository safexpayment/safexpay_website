import React from "react";

import "assets/css/safexpay-web.css"
import "./Bank-Partners.css";

class BankPartners extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Bank-Partners mt-0">
                    <div className="container">
                        <h2 className="section-heading pt-5 pb-2" data-aos="fade-up" data-aos-duration="1000">
                            Bank Partners 
                        </h2>
                        <div className="row" data-aos="fade-up" data-aos-duration="1000">
                            <div className="col-12 bank-partner">
                            <img
                                alt="Bank of Baroda"
                                className="img-fluid col-6"
                                src={require("assets/img/logos/bank-of-baroda.png")}
                            />
                            <img
                                alt="Lakshmi Vilas Bank"
                                className="img-fluid col-6"
                                src={require("assets/img/logos/lakshmi-vilas-bank.png")}
                            />
                            <img
                                alt="NSDL"
                                className="img-fluid col-6"
                                src={require("assets/img/logos/nsdl.png")}
                            />
                            <img
                                alt="IDFC Bank"
                                className="img-fluid col-6"
                                src={require("assets/img/logos/idfc-first-bank.png")}
                            /></div>
                            
                        </div>
                    </div>
                </section>

            </>
        );
    }
}

export default BankPartners;
