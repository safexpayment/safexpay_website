import React from "react";
import "assets/css/safexpay-web.css"
import "./Who.css";

class Who extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Who">
                    <div className="container" data-aos="fade-up" data-aos-duration="1000">
                        <h2 className="section-heading text-center pb-0 text-white">Who We Are</h2>
                        <div className="rounded-border-box">
                            <div className="content">                                
                                <img src={require("assets/img/icons/who.png")} className="card-img-top img-fluid" alt="" />
                                <h3><strong>Safexpay</strong> Started Operations In 2017. </h3>
                                {/* <p className="map mb-0"> Mumbai</p> */}
                            </div>
                        </div>
                    </div>
                </section>

            </>
        );
    }
}

export default Who;
