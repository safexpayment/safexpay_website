import React from "react";
import "assets/css/safexpay-web.css"
import "./Policy.css";

class Policy extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Policy-content">
                    <div className="container" data-aos="fade-up" data-aos-duration="1000">
                        <div className="row">
                        <div className="col-12">
                                    <p className="font-s19"><span className="font-w6">Safexpay India</span>, will always ensure that
                                        your privacy is protected and secured. You can
                                        be assured that we shall use your personal information only in accordance with
                                        this
                                        privacy policy. Please read and understand the Safexpay India Privacy Policy
                                        carefully to
                                        know our views and practices regarding your personal data and on how we use it.
                                        Safexpay India reserves the right to make changes to this policy at any point in
                                        time on
                                        its discretion and accordingly update this page. Please check this page
                                        periodically to
                                        make sure you are happy with the changes.</p>
                                    <h4 className="mt-5 font-w6">Information Safexpay requests from you</h4>
                                    <ul>
                                        <li>1) Name, designation and contact information including email address</li>
                                        <li>2) Business information</li>
                                        <li>3) Demographic information like postcodes, preferences and interests.</li>
                                        <li>4) Any other information related / relevant to our application process.</li>
                                    </ul>
                                    <h4 className="mt-5 font-w6">Why does Safexpay collect your information?</h4>
                                    <h6 className="mb-0 font-w6 pt-2">1. To personalize user experience:</h6>
                                    <p>We may use information to understand how our users as a group use the services
                                        and resources provided on Safexpayindia.com.
                                    </p>
                                    <h6 className="mb-0 font-w6 pt-2">2. To improve customer service:</h6>
                                    <p>Your information helps us to respond more effectively to your customer
                                        service requests, inquiries and support needs.
                                    </p>
                                    <h6 className="mb-0 font-w6 pt-2">3. To process your application:</h6>
                                    <p>We use the information collected to understand the nature and profile
                                        of your business and accordingly approve or decline your application.
                                    </p>
                                    <h6 className="mb-0 font-w6 pt-2">4. For marketing purposes:</h6>
                                    <p>We may use your information for promotional activities of our new products
                                        and features, special offers and for advertising our products.
                                    </p>
                                    <h6 className="mb-0 font-w6 pt-2">5. To send periodic emails:</h6>
                                    <p>The email address provided will be used to send information and updates
                                        pertaining
                                        to our site and our services. It may also be used to respond to your inquiries,
                                        and/or other requests. If the user decides to opt-in to our mailing list,
                                        they will receive emails that may include company news, updates, related product
                                        or service information, etc.
                                    </p>
                                    <h4 className="mt-5 font-w6">Safexpay Data Storage and access</h4>
                                    <p className="mb-0">The data we collect from you are stored in our system servers
                                        and will be treated with utmost
                                        confidentiality. Your personal information will be used only by Safexpay for all
                                        business transactions
                                        that require your information to complete the transactions. </p>
                                    <p>Safexpay may allow a third-party application to access your information in cases
                                        where a need arises for
                                        verification of your data and other validation purposes. Safexpay ensures that
                                        verification processes
                                        used by third party application will help to make your account safe from fraud
                                        or any money laundering.</p>
                                        <h4 className="mt-5 font-w6">Safexpay Security</h4>
                                    <p>Safexpay recognizes its responsibility to keep confidential and secure at all
                                        times any information that
                                        Safexpay receives in connection with a transaction. Safexpay has all the
                                        necessary electronic and
                                        physical procedures in place to keep your information secure in order to prevent
                                        any unauthorized
                                        access. When you log into your Safexpay account, all Internet communication is
                                        secured using Secure
                                        Socket Layer (SSL) technology with 256-bit encryption security. For your own
                                        safety, please make sure
                                        never to share your Safexpay login details with anyone.</p>
                                        <h4 className="mt-5 font-w6">Internet Cookies</h4>
                                    <p>Cookies are text files, used by your computer's browser, that store visitor
                                        session data. Cookies, by
                                        themselves, do not identify the individual user. Cookies are commonly used on
                                        the Internet and do not
                                        harm your system.</p>
                                    <p>Cookies are mainly used to measure web traffic, for keeping records and to let
                                        you know when you visit
                                        a particular site. But cookies never give us access to any other information
                                        other than what you choose
                                        to share with us.</p>
                                    <p>Cookies also help us in analyzing data to be used in improving our website,
                                        eventually giving you a
                                        better user experience. Users generally have the options to accept or decline
                                        cookies. However, please
                                        note that you will not get the full benefit of using the website if you chose to
                                        decline.
                                    </p>
                                    <h4 className="mt-5 font-w6">Third Party Websites</h4>
                                    <p>Users may find advertising or other content on our Site that link to the sites
                                        and services of our
                                        partners, suppliers, advertisers, sponsors, licensors and other third parties.
                                        Safexpay does not control
                                        the content or links that appear on these sites and are not responsible for the
                                        practices employed by
                                        those websites.</p>
                                    <p>Browsing and interaction on any other website, including websites which have a
                                        link to our Site,
                                        is subject to that website's own terms and policies. Safexpay does not guarantee
                                        protection and privacy
                                        of information that you may provide to such websites.</p>
                                        <h4 className="mt-5 font-w6">Updating your Information</h4>
                                    <p>If your personal or professional information (such as your name, address, or
                                        telephone number)
                                        needs change, you must update your details by contacting Safexpay on our contact
                                        us page.</p>
                                    <p>To avoid inconvenience to all, Safexpay strongly advises keeping all information
                                        up to date and correct.
                                        Safexpay will be exempt from any responsibility in the rare event where losses
                                        may arise from not
                                        updating your information.</p>
                                </div>
                           
                        </div>
                    </div>
                </section>

            </>
        );
    }
}

export default Policy;
