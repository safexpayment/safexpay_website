import React from "react";
import "assets/css/safexpay-web.css";
import "./PaymentMethods.css";
class PaymentMethods extends React.Component {

    render() {
        return (
            <>
                <section className="dv-PaymentMethods mt-0 pt-5 pb-5 mb-5">
                    <h2 className="section-heading font-w6" data-aos="zoom-in-up" data-aos-duration="1000">
                        Unlimited Payment Methods
                    </h2><div className="container">
                        <div className="row" data-aos="fade-up" data-aos-duration="1000">
                            <div className="col-12">
                                <p className="icon-heading">Debit , Credit &amp; International Cards</p>
                                <div className="dv-logos dv-cards">
                                    <img
                                        alt="Visa"
                                        className="img-fluid"
                                        src={require("assets/img/logos/visa.png")}
                                    />
                                    <img
                                        alt="Master Card"
                                        className="img-fluid"
                                        src={require("assets/img/logos/master-card.png")}
                                    />
                                    <img
                                        alt="Maestro"
                                        className="img-fluid"
                                        src={require("assets/img/logos/maestro.png")}
                                    />
                                    <img
                                        alt="Rupay"
                                        className="img-fluid"
                                        src={require("assets/img/logos/rupay.png")}
                                    />
                                    <img
                                        alt="American Express"
                                        className="img-fluid"
                                        src={require("assets/img/logos/american-express.png")}
                                    />
                                </div>

                            </div>
                        </div>

                        <div className="row" data-aos="fade-up" data-aos-duration="1000">
                            <div className="col-12">
                                <p className="icon-heading">50+ Netbanking Options</p>
                                <div className="dv-logos dv-NetBanking">
                                    <img
                                        alt="SBI Netbanking"
                                        className="img-fluid"
                                        src={require("assets/img/logos/sbi.png")}
                                    />
                                    <img
                                        alt="Hdfc Netbanking"
                                        className="img-fluid"
                                        src={require("assets/img/logos/hdfc.png")}
                                    />
                                    <img
                                        alt="ICICI Netbanking"
                                        className="img-fluid"
                                        src={require("assets/img/logos/icici.png")}
                                    />
                                    <img
                                        alt="Axis Netbanking"
                                        className="img-fluid"
                                        src={require("assets/img/logos/axis.png")}
                                    />
                                    <img
                                        alt="Kotak Netbanking"
                                        className="img-fluid"
                                        src={require("assets/img/logos/kotak.png")}
                                    />
                                    <img
                                        alt="Yes Netbanking"
                                        className="img-fluid"
                                        src={require("assets/img/logos/yes.png")}
                                    />
                                    <img
                                        alt="BOB Netbanking"
                                        className="img-fluid"
                                        src={require("assets/img/logos/bob.png")}
                                    />
                                </div>

                            </div>
                        </div>

                        <div className="row" data-aos="fade-up" data-aos-duration="1000">
                            <div className="col-12">
                                <p className="icon-heading">UPI</p>
                                <div className="dv-logos dv-upi">
                                    <img
                                        alt="UPI"
                                        className="img-fluid"
                                        src={require("assets/img/logos/upi.png")}
                                    />
                                    <img
                                        alt="BHIM UPI"
                                        className="img-fluid"
                                        src={require("assets/img/logos/bhim.png")}
                                    />
                                    <img
                                        alt="G-Pay"
                                        className="img-fluid"
                                        src={require("assets/img/logos/g-pay.png")}
                                    />
                                    <img
                                        alt="PhonePe"
                                        className="img-fluid"
                                        src={require("assets/img/logos/phone-pe.png")}
                                    />
                                    <img
                                        alt="BHIM SBI"
                                        className="img-fluid"
                                        src={require("assets/img/logos/bhim-sbi.png")}
                                    />
                                    <img
                                        alt="ICICI"
                                        className="img-fluid"
                                        src={require("assets/img/logos/upi-icici.png")}
                                    />
                                </div>

                            </div>
                        </div>
                    
                        <div className="row" data-aos="fade-up" data-aos-duration="1000">
                            <div className="col-12">
                                <p className="icon-heading">Wallets &amp; Pay Later</p>
                                <div className="dv-logos dv-wallets">
                                    <img
                                        alt="Amazon Pay"
                                        className="img-fluid"
                                        src={require("assets/img/logos/amazon-pay.png")}
                                    />
                                    <img
                                        alt="Paytm"
                                        className="img-fluid"
                                        src={require("assets/img/logos/paytm.png")}
                                    />
                                    <img
                                        alt="Mobikwik"
                                        className="img-fluid"
                                        src={require("assets/img/logos/mobikwik.png")}
                                    />
                                    <img
                                        alt="Airtel Money"
                                        className="img-fluid"
                                        src={require("assets/img/logos/airtel-money.png")}
                                    />
                                    <img
                                        alt="Free Charge"
                                        className="img-fluid"
                                        src={require("assets/img/logos/freecharge.png")}
                                    />
                                    <img
                                        alt="LazyPay"
                                        className="img-fluid"
                                        src={require("assets/img/logos/lazypay.png")}
                                    />
                                    <img
                                        alt="Simpl"
                                        className="img-fluid"
                                        src={require("assets/img/logos/simpl.png")}
                                    />
                                    <img
                                        alt="Jio-Money"
                                        className="img-fluid"
                                        src={require("assets/img/logos/jio-money.png")}
                                    />
                                    <img
                                        alt="Ola-Money"
                                        className="img-fluid"
                                        src={require("assets/img/logos/ola-money.png")}
                                    />
                                    <img
                                        alt="Oxigen"
                                        className="img-fluid"
                                        src={require("assets/img/logos/oxigen.png")}
                                    />
                                    <img
                                        alt="e-Pay Later"
                                        className="img-fluid"
                                        src={require("assets/img/logos/epaylater.png")}
                                    />
                                </div>

                            </div>
                        </div>
                    
                        <div className="row" data-aos="fade-up" data-aos-duration="1000">
                            <div className="col-12">
                                <p className="icon-heading"></p>
                                <div className="dv-logos dv-icons">
                                    <div className="icons">
                                    <img
                                        alt="Currency"
                                        className="img-fluid"
                                        src={require("assets/img/icons/currencies.png")}
                                    />
                                        <label className="lbl">50+ Currencies</label>
                                    </div>
                                    <div className="icons">
                                    <img
                                        alt="EMI"
                                        className="img-fluid"
                                        src={require("assets/img/icons/emi.png")}
                                    />
                                        <label className="lbl">
                                            EMI
                                            <span>(Cards &amp; Cardless)</span>
                                        </label>
                                    </div>
                                    <div className="icons">
                                    <img
                                        alt="Bharat QR"
                                        className="img-fluid"
                                        src={require("assets/img/icons/bharat-qr.png")}
                                    />
                                        <label className="lbl">Bharat QR</label>
                                    </div>
                                </div>

                            </div>
                        </div>

                    
                    </div>
                </section>
            </>
        );
    }
}

export default PaymentMethods;
