import React, { Component } from "react";
//import "assets/css/safexpay-web.css"
import "./ApplyForm.css";
import axios from 'axios';



class ApplyForm extends Component {

    state={
        name:'',
        emailid:'',
        mobilenumber:'',
        cv:'',
        header:''
    }

    flag = 0;
    datasend = false;

    componentDidMount(){
        if(this.props.header!=undefined)
        {
        console.log("props===================",this.props.header.name);
        this.state.header = this.props.header.name;
        var headerfirst = this.props.header.name;
        document.getElementById("formhead1").innerHTML = headerfirst;
        }
        console.log("props========================>",this.props.header);
    }


    postdatahandler=(e)=>
    {
        e.preventDefault();

        var emailid = this.state.emailid;
        var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/; 
        console.log('====>>>>>>>', pattern.test(emailid));
        var validemail = pattern.test(emailid);

        /*console.log("cv------------",this.state.cv);
        console.log("cv size------------",this.state.cv.fileName);
        console.log("name",document.getElementById("txtFile").files[0].name);
        console.log("filesize",document.getElementById("txtFile").files[0].size);
        console.log("filesize",document.getElementById("txtFile").files[0].type);
        console.log("filesize",document.getElementById("txtFile").files[0].path);*/


        if(this.state.name == "" || this.state.mobilenumber == "" || this.state.emailid == "" || this.state.cv == "")
        {
            this.datasend = false;
           
        }
        else{
            this.datasend = true
        }
        
        const data ={
            "full_name": this.state.name,
            "email_id": this.state.emailid,
            "mobile_no": this.state.mobilenumber,
            "form_type":"Support",
            "description":this.state.description
        }
        
        if(this.datasend)
        {
        if(validemail)
        {
        console.log("data",data);
        this.flag=1;
        document.getElementById("submitform").innerHTML = "Loading.....";
           
        axios.post('https://www.avantgardepayments.com/agadmin/api/emailSender/post',data).then(response=>{ //https://
            console.log(response);
            this.datasend=false;

            if(response.data.message = "Success")
            {
            document.getElementById("submitform").style.backgroundColor = 'var(--darkgreen)';
            document.getElementById("submitform").innerHTML = "Send";
            document.getElementById("confmsg2").style.visibility = "visible"
            }
            if(response.data.message = "Success")
        {
            document.getElementById("txtFirstName").value = '';
            document.getElementById("txtEmail").value = '';
            document.getElementById("txtMobile").value = '';
            document.getElementById("txtFile").value = '';
        }
        if(response.data.message = "Success"){
            setTimeout(() => {
            document.getElementById("submitform").style.backgroundColor = 'var(--blue)';
            document.getElementById("submitform").innerHTML = "Submit a ticket";
            document.getElementById("confmsg2").style.visibility = "hidden";
                
            }, 4000);
        }
        })
    }
    else{
        alert("Please Enter Valid Email Id");
    }
}
else{
    alert("please provide data and upload cv then enter submit button");
}


       



        
    }
    render(){
    var attchedClasses =["dv-Registration-content", "Open"];
    if(this.props.open)
    attchedClasses = ["dv-Registration-content","Open"];
    
    //if(this.props.header!=undefined)
    //{
    console.log("props===================",this.props.header.name);
    this.state.header = this.props.header.name;
    var headerfirst = this.props.header.name;
    //document.getElementById("formhead1").innerHTML = headerfirst;
    //}
    console.log("props========================>",this.props.header);

        return (
            <>
            <div className="blueback"></div>
            
                <section className={attchedClasses.join(' ')}>      
                <form>     
                            
                                <div className="dv-registration-form dv-form pt-5">
                                    <div className="form-group">
                                    <div className="sphd">
                                        <div className="section-heading sphd1" id="formhead1">{{headerfirst}}</div>
                                    </div>
                                        <label htmlFor="txtFirstName">Name <span className="req">*</span></label>
                                        <input type="text" className="form-control form-options" id="txtFirstName" placeholder="Enter Name" onChange={(event)=>this.setState({name:event.target.value})}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtEmail">Email Address <span className="req">*</span></label>
                                        <input type="text" className="form-control form-options" id="txtEmail" placeholder="Enter Email Address" onChange={(event) => this.setState({emailid:event.target.value})}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtMobile">Mobile Number <span className="req">*</span></label>
                                        <input type="number" className="form-control form-options" id="txtMobile" placeholder="Enter Mobile Number" onChange={(event) => this.setState({mobilenumber:event.target.value})}/>
                                    </div>
                                    {/*<div className="form-group">
                                        <label htmlFor="txtFirstName">Description <span className="req">*</span></label>
                                        <textarea type="text" className="form-control form-options" id="txtTextarea" placeholder="How can we assist you? Explain your query in brief." onChange={(event) => this.setState({description:event.target.value})}/>
        </div>*/}
                                    <div className="form-group">
                                        <label htmlFor="txtFile">Upload CV<span className="req">*</span></label>
                                        <input type="file" className="form-control form-options" id="txtFile" name="filename" onChange={(event) => this.setState({cv:event.target.value})}/>
                                    </div>
                                    <h6 className="confmsg2" id="confmsg2">Thankyou,Your message has been successfully sent.</h6>
                                    <div className="text-center">
                                        <button className="submitbtn1" id="submitform" onClick={(event)=>this.postdatahandler(event)}>
                                        Submit
                                        </button>
                                    </div>
                                    <text className="texthr">Or apply to hr@safexpay.com</text>
                                   
                                    
                                </div>
                </form>
                           
                       
                   
                </section>

            </>
        );
    }
}

export default ApplyForm;
