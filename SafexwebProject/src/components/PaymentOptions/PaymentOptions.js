import React from "react";
import "assets/css/safexpay-web.css";
import "./PaymentOptions.css";
class PaymentOptions extends React.Component {

    render() {
        return (
            <>
                <section className="dv-PaymentOptions mt-0">
                    <h2 className="section-heading font-w6 pb-0 pb-md-5 mb-md-5 pt-0" data-aos="fade-up" data-aos-duration="1000">
                        Multiple Options To Accept Payments
                    </h2>
                    <div className="dv-first-payment-bg1" id="dv-first-payment-bg" data-aos="fade-up" data-aos-duration="1000">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-4 col-12 text-center offset-md-1">
                                    <img
                                        alt=""
                                        className="img-fluid"
                                        src={require("assets/img/section/custom-payment.png")}
                                    />
                                </div>
                                <div className="col-md-6 offset-md-1 col-12 text-md-right text-center text-white1 py-5">
                                    <h2 className="content-heading text-white1 font-w6">
                                    Fully Customised Payment Experience
                                    </h2>
                                    <p className="font-s17">
                                    Give power to your designers and developers to create something magical.
                                    </p>
                                    <p>
                                        <a href="/" className="d-block color-lightblue small-link small-link-after">Website</a>
                                        <a href="/" className="d-block color-lightblue small-link small-link-after">Mobile App</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="dv-second-payment-bg1" id="dv-second-payment-bg" data-aos="fade-up" data-aos-duration="1000">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-6 col-12 text-md-left text-center pt-5 order-2 order-md-1">
                                    <h2 className="content-heading pt-0">
                                        <strong>Fast, Standard And Beautiful <br /> Checkout Experience</strong>
                                    </h2>
                                    <p className="font-s17">
                                        Fast integration, all error validations in built, super success rates, minor customisations!
                                    </p>
                                    <p>
                                        <a href="/" className="d-block small-link small-link-before color-lightblue">Website</a>
                                        <a href="/" className="d-block small-link small-link-before color-lightblue">Mobile App</a>
                                    </p>
                                </div>
                                <div className="col-md-6 col-12 text-center order-1 order-md-2">
                                    <img
                                        alt=""
                                        className="img-fluid w-50"
                                        src={require("assets/img/section/checkout.png")}
                                    />
                                </div>
                            </div>

                            <div className="row mt-5 pt-0" id="PluginsToDevelop">
                                <div className="col-md-5 col-12 text-center mb-3">
                                    <img
                                        alt=""
                                        className="img-fluid"
                                        src={require("assets/img/section/cart.png")}
                                    />
                                </div>
                                <div className="col-md-6 col-12 offset-md-1 text-md-right text-center">
                                    <h2 className="content-heading">
                                        <strong>Using Plugins To Develop Your Cart Or Application?</strong>
                                    </h2>
                                    <p className="font-s17">
                                        Give power to your designers and developers to create something magical.
                                    </p>
                                    <p>
                                        <a href="/" className="d-block small-link small-link-after color-lightblue">Website</a>
                                        <a href="/" className="d-block small-link small-link-after color-lightblue">Mobile App</a>
                                        <a href="/" className="d-block small-link small-link-after color-lightblue">SDK &amp; Plugins</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="dv-third-payment-bg1" id="dv-third-payment-bg" data-aos="fade-up" data-aos-duration="1000">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-6 col-12 text-md-left text-center text-white1 order-2 order-md-1">
                                    <h2 className="content-heading text-white1 pt-5">
                                        Send Link On Sms Or Email
                                    </h2>
                                    <p className="font-s17">
                                        Use our dashboard to send links individually or in bulk OR integrate our APIs and automate your  payment requests.
                                    </p>
                                    <p>
                                        <a href="/" className="d-block small-link small-link-before text-white1 color-lightblue">Dashboard</a>
                                        <a href="/" className="d-block small-link small-link-before text-white1 color-lightblue">Merchant App</a>
                                        <a href="/" className="d-block small-link small-link-before text-white1 color-lightblue">API</a>
                                    </p>
                                </div>
                                <div className="col-md-6 col-12 text-center order-1 order-md-2">
                                    <img
                                        alt=""
                                        className="img-fluid"
                                        src={require("assets/img/section/sms.png")}
                                    />
                                </div>

                            </div>
                        </div>
                    </div>
                    <div className="dv-fourth-payment-bg1" id="dv-fourth-payment-bg" data-aos="fade-up" data-aos-duration="1000">
                        <div className="dv-fourth-payment-bg-left1"></div>
                        <div className="dv-fourth-payment-bg-right1"></div>
                        <div className="container">
                            <div className="row">
                                <div className="col-md-5 col-12 text-center pb-0 pb-md-5">
                                    <img
                                        alt=""
                                        className="img-fluid w-50 imgpay"
                                        src={require("assets/img/section/pay.png")}
                                    />
                                </div>
                                <div className="col-md-5 col-12 rep-text-center text-right offset-md-2 pt-5 ">
                                    <h2 className="content-heading pt-0 pt-md-5">
                                        Make Your Payment Button In &lt; 2 Minutes
                                    </h2>
                                    <p className="font-s17">
                                        No developer no problem. Just use our dashboard and make a payment button to add on any webpage.
                                    </p>
                                    <p>
                                        <a href="/" className="d-block small-link small-link-after color-lightblue">Dashboard</a>
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
            </>
        );
    }
}

export default PaymentOptions;
