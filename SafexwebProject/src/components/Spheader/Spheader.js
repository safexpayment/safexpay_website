import React, { Component } from 'react';
import classes from './Spheader.module.css';
import Logo from "../images/SafexpayLogo.svg";

class Spheader extends Component{
    render(){
        return(
            <>
           
                <div className={classes.top}>
                <div className="container">
                    <img className={classes.logo}  src={Logo} alt="SafexpayLogo"></img>
                    <span className={classes.doct}>support</span>
                    <a  href="/" className={classes.apixp}>Back to Home</a>
                    <div className="clearfix"></div>
                </div>
            </div>
            </>
        );
    }
}

export default Spheader;