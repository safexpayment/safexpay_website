import React from "react";
import "assets/css/safexpay-web.css"
import "./Features.css";

class Features extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Features">
                    <div className="container">
                        <h2 className="section-heading">Think beyond banking experience</h2>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="card" data-aos="fade-up"
                                    data-aos-duration="1000">
                                    <div className="row no-gutters">
                                        <div className="col-md-5 dv-img">
                                            <img src={require("assets/img/section/accept-payments.png")} className="card-img-top" alt="Accept Payments" />
                                        </div>
                                        <div className="col-md-7"><div className="card-body">
                                            <h5 className="card-title">Accept payments from anyone anywhere</h5>
                                            <p className="card-text">100+ payment options, simple integrations, easy customisation, <br /> 24 X 7 availability.</p>
                                        </div>
                                            <div className="card-footer">
                                                <a className="more" href="/business/accept-payments">Learn more</a>
                                            </div></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="card" data-aos="fade-up"
                                    data-aos-duration="1000">
                                    <div className="row no-gutters">
                                        <div className="col-md-5 dv-img">
                                            <img src={require("assets/img/section/make-payouts.png")} className="card-img-top" alt="" />
                                        </div>
                                        <div className="col-md-7"><div className="card-body">
                                            <h5 className="card-title">Send money in Milliseconds to anyone</h5>
                                            <p className="card-text">Instant credit in bank account or UPI handle, send in bulk, create workflows with our APIs.
                                            
                                            </p>
                                        </div>
                                            <div className="card-footer">
                                                <a className="more" href="/business/make-payouts">Learn more</a>
                                            </div></div>
                                    </div> </div>

                            </div>
                            <div className="col-md-6">
                                <div className="card" data-aos="fade-up"
                                    data-aos-duration="1000">
                                    <div className="row no-gutters">
                                        <div className="col-md-5 dv-img">
                                            <img src={require("assets/img/section/neo-bank.png")} className="card-img-top" alt="" />
                                        </div><div className="col-md-7"> <div className="card-body">
                                            <h5 className="card-title">Be your own neo bank or payments platform</h5>
                                            <p className="card-text">Our financial partner network and technology solutions allow enterprises to operate under their own or under 3rd parties with bank License</p>
                                        </div>
                                            <div className="card-footer">
                                                <a className="more" href="/business/digital-banking">Learn more</a>
                                            </div></div>
                                    </div></div>

                            </div>
                            <div className="col-md-6">
                                <div className="card" data-aos="fade-up"
                                    data-aos-duration="1000">
                                    <div className="row no-gutters">
                                        <div className="col-md-5 dv-img">
                                            <img src={require("assets/img/section/digital-banking.png")} className="card-img-top" alt="" />
                                        </div>
                                        <div className="col-md-7">
                                            <div className="card-body">
                                                <h5 className="card-title">Your brand, <br /> Your Platform!</h5>
                                                <p className="card-text">Create your own payment gateway, use our fully customised banking platform to serve your customers or partners.</p>
                                            </div>
                                            <div className="card-footer">
                                                <a className="more" href="/white-label/payment-gateway">Learn more</a>
                                            </div></div>
                                    </div> </div>

                            </div>
                        </div>
                    </div>

                </section>
            </>
        );
    }
}

export default Features;
