import React from "react";
//import "assets/css/safexpay-web.css"
import "./ContactUs.css";
import Backdrop from "../Backdrop/Backdrop";

const ContactUs =(props)=> {

    var attchedClasses =["dv-Registration-content", "close"];
    if(props.open)
    attchedClasses = ["dv-Registration-content","Open"];

        return (
            <>
            <Backdrop show={props.open} clicked={props.click}/>
                <section className={attchedClasses.join(' ')}>
        
                        
                            
                                <div className="dv-registration-form dv-form pt-5">
                                    <div className="form-group">
                                    <div  data-aos="fade-right" data-aos-duration="1000">
                                <label className="section-heading">Please provide us a few details so our sales team can reach out to you
</label>
                            </div>
                                        <label htmlFor="txtFirstName">First Name <span className="req">*</span></label>
                                        <input type="text" className="form-control" id="txtFirstName" placeholder="Enter Name" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtLastName">Last Name <span className="req">*</span></label>
                                        <input type="text" className="form-control" id="txtLastName" placeholder="Enter Last Name" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtEmail">Email Address <span className="req">*</span></label>
                                        <input type="text" className="form-control" id="txtEmail" placeholder="Enter Email Address" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtMobile">Mobile Number <span className="req">*</span></label>
                                        <input type="text" className="form-control" id="txtMobile" placeholder="Enter Mobile Number" />
                                    </div>
                                    <div className="form-group mt-4">
                                        <label htmlFor="txtCompanyName">Industry Type </label>
                                        <select type="text" className="form-control" id="txtIndustryType" placeholder="Eg. Safexpay">
                                            <option value="airlines">Airlines</option>
                                            <option value="b2b">B2B</option>
                                            <option value="donation">Donation</option>
                                            <option value="ecommerce">Ecommerce</option>
                                            <option value="education">Education</option>
                                            <option value="financialservices">Financial Services</option>
                                            <option value="hospitality">Hospitality</option>
                                            <option value="travel">Travel</option>
                                            <option value="utilities">Utilities</option>
                                            <option value="other">other</option>                   
                                        </select>
                                    </div>
                                    <div className="form-group mt-4">
                                        <label htmlFor="txtEntityType">Entity Type </label>
                                        <select type="text" className="form-control" id="txtEntityType" placeholder="Eg. Safexpay">
                                            <option>Unregistered</option>
                                            <option>Individual Proprietor</option>
                                            <option>Partnership</option>
                                            <option>Private Limited</option>
                                            <option>Public Limited</option>
                                        </select>
                                    </div>
                                    <div className="form-group mt-4">
                                        <label htmlFor="txtCompanyName">Company Name </label>
                                        <input type="text" className="form-control" id="txtCompanyName" placeholder="Enter Company Name" />
                                    </div>
                                    <div className="form-group mt-4">
                                        <label htmlFor="txtCompanyName">Company Website </label>
                                        <input type="text" className="form-control" id="txtCompanyName" placeholder="Enter Company Website" />
                                    </div>
                                    <div className="form-group mt-4">
                                        <label htmlFor="txtCompanyName">Product Requirements</label>
                                        <select type="text" className="form-control" id="txtCompanyName">
                                            <option>Unregistered</option>
                                            <option>Individual Proprietor</option>
                                            <option>Partnership</option>
                                            <option>Private Limited</option>
                                            <option>Public Limited</option>
                                        </select>
                                    </div>
                                    <div className="form-group form-check text-center my-4">
                                        <input type="checkbox" className="form-check-input" id="chkAgreeTC"/>
                                        <label className="form-check-label" htmlFor="chkAgreeTC">I agree to be contacted on the mobile number and email id provided </label>
                                    </div>
                                    <div className="btn-holder text-center">
                                        <a href="/"  className="btn btn-create-account btn-gradient green">
                                            Submit
                                        </a>
                                    </div>
                                    <div className="dv-bottom-bg">
                                    <div className="first-bottom-bg"></div>
                                        <div className="second-bottom-bg"></div>
                                    </div>
                                </div>
                           
                       
                   
                </section>

            </>
        );
    }

export default ContactUs;
