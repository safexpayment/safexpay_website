import React, { Component } from "react";
//import "assets/css/safexpay-web.css"
import "./ContactUs.css";
import Backdrop from "../Backdrop/Backdrop";
import axios from 'axios';

class ContactUs extends Component{
    state={
        name:'',
        mobilenumber:'',
        emailid:'',
        industrytype:'',
        i_agree:"false"
    }

    flag = 0;
    checkflag= false;
    datasend = false;


    postdatahandler=()=>{
        var emailid = this.state.emailid;
        var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/; 
        console.log('====>>>>>>>', pattern.test(emailid));
        var validemail = pattern.test(emailid);

        


        const data ={
            "full_name": this.state.name,
            "email_id": this.state.emailid,
            "mobile_no": this.state.mobilenumber,
            "industry_type": this.state.industrytype,
            "form_type":"ContactUs",
            "i_agree": this.state.i_agree
        };

        console.log("data.......>",data);
       if(this.state.name != "" && this.state.mobilenumber != "" && this.state.emailid != "")
        {
            this.datasend = true;
           
        }
        else{
            this.datasend = false
        }
        console.log("state-i_agree",this.state.i_agree);

        if(this.datasend)
        {
        if(validemail)
        {
        if(this.state.i_agree==true)
        {
           // if(this.datasend)
            console.log("data send");
            this.checkflag = true;
             
            document.getElementById("submitbtn1").style.display = "none";
            //document.getElementById("spinner").style.visibility = "visible";
            document.getElementById("spinner").style.display="block";

            //setTimeout(() => { document.getElementById('spinner').style.visibility = "visible"; }, 4000);

            axios.post('https://www.avantgardepayments.com/agadmin/api/emailSender/post',data).then(response=>{
            console.log("response is",response.data.message);
            this.datasend = false;

            if(response.data.message = "Success")
            {
               // document.getElementById("spinner").style.visibility = "hidden";
                document.getElementById("spinner").style.display="none";
                document.getElementById("submitbtn1").style.display = "inline";
                document.getElementById("submitbtn1").style.backgroundColor = 'var(--darkgreen)';
                document.getElementById("submitbtn1").innerHTML = "Send";
               // document.getElementById("confmsg").style.visibility = "visible";
                document.getElementById("confmsg").style.display = "block";
            }

           /* if(response.data.message = "Success")
            {
            setTimeout(function(){
            //document.getElementById("fullform10").style.display = "none";
            console.log("both will disappear");
            //document.getElementById("fullform10").classList.toggle("close11");
            //document.getElementById("backdrop1").classList.toggle("close11");
            //document.getElementById("fullform11").style.visibility="hidden";
            //document.getElementById("backdrop1").style.display = "hidden";
            document.getElementById("fullform10").classList.add("close11");
            document.getElementById("backdrop11").classList.add("close11");
            },2500)
            }*/

            if(response.data.message = "Success")
            {
            setTimeout(function(){
            document.getElementById("submitbtn1").style.backgroundColor = 'var(--lightblue)';
            document.getElementById("submitbtn1").innerHTML = "Submit";
            //document.getElementById("confmsg").style.visibility = "hidden";
            document.getElementById("confmsg").style.display = "none";
            //document.getElementById("fullform11").style.display = "inline-block";
            //document.getElementById("backdrop1").style.display = "inline-block";
            //document.getElementById("fullform11").style.visibility = "hidden";
            //document.getElementById("backdrop1").style.visibility = "hidden";
            //document.getElementById("fullform11").style.display = "block";
            //document.getElementById("backdrop1").style.display = "block";
            //window.location.reload();
             },5000)
             }

            if(response.data.message = "Success")
            {
            if(this.checkflag){
            document.getElementById("txtFirstName1").value = '';
            document.getElementById("txtEmail11").value = '';
            document.getElementById("txtMobile11").value = '';
            document.getElementById("chkAgreeTC1").checked = false;
            document.getElementById("txtIndustryType1").selectedIndex = 0;
            this.flag=this.flag+1;
            if(this.flag%2 == 0)
            {
                this.setState({
                    i_agree:false
                })
            }
            else{
                this.setState({
                    i_agree:true
                })
            }
            this.checkflag=false;
        }
    }
           
           
        })
           //var x = document.getElementById("submitbtn");
            //x.style.backgroundColor = "red";
        }
        else{
            alert("please click in checkbox before click on submit button");
            this.datasend = false;
           
        }
    }
    else{
        alert("Please Enter Valid Email Id");
        this.datasend = false;
    }
}
else{
        alert("please provide data then enter submit button");
        this.datasend = false;
}


        /*setTimeout(function(){ document.getElementById("submitbtn1").style.backgroundColor = 'var(--lightblue)';
                               document.getElementById("submitbtn1").innerHTML = "Submit"; }, 5000);*/


        console.log("this.checkflag",this.checkflag);
       /* if(!this.props.open)
        {
            document.getElementById("submitbtn1").style.backgroundColor = 'var(--lightblue)';
            document.getElementById("submitbtn1").innerHTML = "Submit";
        }*/     
    }

        clickhandler=()=>{
            this.flag=this.flag+1;
            if(this.flag%2 == 0)
            {
                this.setState({
                    i_agree:false
                })
            }
            else{
                this.setState({
                    i_agree:true
                })
            }
            
        }


     /*   componentDidMount(){
            document.getElementById("txtEmail11").setAttribute("type","email")
        }*/

       render(){
        
        var attchedClasses =["dv-Registration-content", "close11"];
        var backdropcl1 = ["Backdrop3","close11"];
        if(this.props.open)
        {
        attchedClasses = ["dv-Registration-content","Open1"];
        backdropcl1 = ["Backdrop3"];
        }
    
            return (
            <>
   
           {/* <Backdrop show={this.props.open} className={backdropcl1.join(' ')} clicked={this.props.click} id="backdrop1"/>*/}
                <div className={backdropcl1.join(' ')} id="backdrop11" onClick={this.props.click}></div>
               
                <section className={attchedClasses.join(' ')} >      
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
        
                                <div className="dv-registration-form dv-form pt-5" id="fullform10" >
                                    <button className="close" onClick={this.props.click}>
                                        <span aria-hidden={true}>×</span>
                                    </button>
                                    <div className="form-group">
                                    <div className="contus">
                                        {/*<label className="section-heading">Please provide us a few details so our sales team can reach out to you</label>*/}
                                        <h1 className="section-heading border-heading">Contact Us</h1>
                                    </div>
                                    
                                        <label htmlFor="txtFirstName">Full Name <span className="req">*</span></label>
                                        <input type="text" className="form-control form-option"  id="txtFirstName1" placeholder="Enter Name" onChange={(event)=>this.setState({name:event.target.value})} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtEmail">Email Address <span className="req">*</span></label>
                                        <input type="email" required className="form-control form-option" id="txtEmail11" placeholder="Enter Email Address" onChange={(event) => this.setState({emailid:event.target.value})}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtMobile">Mobile Number <span className="req">*</span></label>
                                        <input type="number" className="form-control form-option" id="txtMobile11" placeholder="Enter Mobile Number" onChange={(event) => this.setState({mobilenumber:event.target.value})}/>
                                    </div>
                                    <div className="form-group mt-4">
                                        <label htmlFor="txtCompanyName">Industry Type </label>
                                        <select type="text" className="form-control form-option" id="txtIndustryType1"  placeholder="Eg. Safexpay"  onChange={(event) => this.setState({industrytype:event.target.value})}>
                                            <option value="select">Select</option>
                                            <option value="airlines">Airlines</option>
                                            <option value="b2b">B2B</option>
                                            <option value="donation">Donation</option>
                                            <option value="ecommerce">Ecommerce</option>
                                            <option value="education">Education</option>
                                            <option value="financialservices">Financial Services</option>
                                            <option value="hospitality">Hospitality</option>
                                            <option value="travel">Travel</option>
                                            <option value="utilities">Utilities</option>
                                            <option value="other">other</option>                   
                                        </select>
                                    </div>
                                    <div className="form-group form-check text-center my-4">
                                        <input type="checkbox" className="form-check-input" id="chkAgreeTC1" onClick={this.clickhandler}/>
                                        {/*onChange={(event) => this.setState({i_agree:"true"})}*/}
                                        <label className="form-check-label" htmlFor="chkAgreeTC">I agree to be contacted on the mobile number and email id provided </label>
                                    </div>
                                   {/* <div className="btn-holder text-center"  onClick={this.postdatahandler}>
                                        <button  className="btn btn-create-account btn-gradient green" id="submitbtn1" >
                                            Submit
                                        </button>                                    
            </div>*/}     
                                    <h6 className="confmsg" id="confmsg">Thankyou,Your message has been successfully sent.</h6>
                                   {/* <button class="buttonload">
                                          <i class="fa fa-spinner fa-spin"></i>Loading
        </button>   */}          
                                    <div className="text-center">
                                        <button  className="submitbtn01" id="submitbtn1" type="submit" onClick={(e)=>this.postdatahandler(e)}>
                                        Submit 
                                        </button>
                                    </div>
                                    <div className="text-center myspin" id="spinner">
                                        <button  className="submitbtn01">
                                        Loading <span class="fa fa-refresh fa-spin"></span>
                                        </button>
                                    </div>
                                    <div className="dv-bottom-bg">
                                    <div className="first-bottom-bg"></div>
                                        <div className="second-bottom-bg"></div>
                                    </div>
                                </div>
          
                                
                </section>
        
              
            </>
        );
            }
    }

export default ContactUs;
