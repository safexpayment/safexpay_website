import React, { Component } from "react";
//import "assets/css/safexpay-web.css"
//import "./Applyformjd.css";
import axios from 'axios';
import { cleanData } from "jquery";



class ApplyFormJdnew extends Component {

    state={
        name:'',
        emailid:'',
        mobilenumber:'',
        cv:null,
        headername:'',
        i_agree:"false",
        location:'',
        experience:''
    }

    flag = 0;
    datasend = false;

    componentDidMount(){
       /* if(this.props.header!=undefined)
        {
        console.log("props===================",this.props.header.name);
        this.state.header = this.props.header.name;
        var headerfirst = this.props.header.name;
        document.getElementById("formhead").innerHTML = headerfirst;
        }*/
        console.log("props========================>",this.props.pos);
        this.setState(
            {
                headername:this.props.pos,
                location:this.props.loc,
                experience:this.props.exp

            }
        )
    }

    onchange=(e)=>{
        let files = e.target.files;
        let reader = new FileReader();
        reader.readAsDataURL(files[0]);
        reader.onload=(e)=>{
            this.setState({
                cv:e.target.result
            })
        }
    }


    postdatahandler=(e)=>
    {
        e.preventDefault();

        var emailid = this.state.emailid;
        var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/; 
        //console.log('====>>>>>>>', pattern.test(emailid));
        var validemail = pattern.test(emailid);

        /*console.log("cv------------",this.state.cv);
        console.log("cv size------------",this.state.cv.fileName);
        console.log("name",document.getElementById("txtFile").files[0].name);
        console.log("filesize",document.getElementById("txtFile").files[0].size);
        console.log("filesize",document.getElementById("txtFile").files[0].type);
        console.log("filesize",document.getElementById("txtFile").files[0].path);*/

        var checkfg = document.getElementById("chkAgree1").checked;
        console.log("checked==============>",checkfg);


        if(this.state.name == "" || this.state.mobilenumber == "" || this.state.emailid == "" || this.state.cv == null)
        {
            this.datasend = false;
           
        }
        else{
            this.datasend = true
        }

        const filedata = new FormData();

        filedata.append("designation",this.state.headername);
        filedata.append("locations",this.state.location);
        filedata.append("experience", this.state.experience);
        filedata.append("full_name", this.state.name);
        filedata.append("email_id", this.state.emailid);
        filedata.append("mobile_no", this.state.mobilenumber);
        filedata.append("resume",this.state.cv);






        //const filen = document.getElementById("fileupld").files[0];

        console.log("cv ",this.state.cv);

       /* this.setState({
            resume: filen
        });*/

        console.log("state",this.state);
        
        const data ={
            "designation":this.state.headername,
            "locations":this.state.location,
            "experience": this.state.experience,
            "full_name": this.state.name,
            "email_id": this.state.emailid,
            "mobile_no": this.state.mobilenumber,
            "resume":this.state.cv 
        }

        //console.log("checktype===========>",this.state.i_agree);
      /*  console.log("data..............................",data);
        console.log("file is",document.getElementById("fileupld"));
        console.log("name",document.getElementById("fileupld").files[0].name);
        console.log("filesize",document.getElementById("fileupld").files[0].size);
        console.log("filetype",document.getElementById("fileupld").files[0].type);*/
        //console.log("filepath",document.getElementById("fileupld").files[0].path)

        if(checkfg)
        {
        if(this.datasend)
        {
        if(validemail)
        {
        console.log("data",data);
        this.flag=1;
        document.getElementById("submitform").innerHTML = "Loading.....";
           
        axios.post('https://www.avantgardepayments.com/agadmin/api/hrEmailSender/post',filedata,{
                headers:{'Content-Type': 'multipart/form-data'},

        }).then(response=>{ 
            console.log(response);
            this.datasend=false;

            if(response.data.message = "Success")
            {
            document.getElementById("submitform").style.backgroundColor = 'var(--darkgreen)';
            document.getElementById("submitform").innerHTML = "Send";
           // document.getElementById("confmsg2").style.visibility = "visible"
            }
            if(response.data.message = "Success")
        {
            document.getElementById("txtFirstName").value = '';
            document.getElementById("txtEmail").value = '';
            document.getElementById("txtMobile").value = '';
            document.getElementById("fileupld").value = null;
            document.getElementById("chkAgree1").checked = false;
            this.setState({
                cv:null
            })
        }
        if(response.data.message = "Success"){
            setTimeout(() => {
            //document.getElementById("submitform").style.backgroundColor = 'var(--blue)';
            document.getElementById("submitform").innerHTML = "Apply Now";
           // document.getElementById("confmsg2").style.visibility = "hidden";
                
            }, 4000);
        }
        })
    }
    else{
        alert("Please Enter Valid Email Id");
    }
}
else{
    alert("please provide data and upload cv then enter submit button");
}
}
else{
    alert("please click in checkbox before click on Apply Now button");
}
}

    render(){
    var attchedClasses =["dv-Registration-content", "Open","formjd"];
   /* if(this.props.open)
    attchedClasses = ["dv-Registration-content","Open"];*/
    console.log("props========================>",this.props);

        return (
            <>
            
            <h1 className="section-heading apply-txt">Join our team, Apply Now !</h1>
                                <form className="apply-container">     
                                    <div className="apply-job-form pt-5">
                                        <div className="form-group">
                                            <label htmlFor="txtFirstName">Name <span className="req">*</span></label>
                                            <input type="text" className="form-control form-options" id="txtFirstName" placeholder="Enter Full Name" onChange={(event)=>this.setState({name:event.target.value})}/>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="txtEmail">Email Id <span className="req">*</span></label>
                                            <input type="text" className="form-control form-options" id="txtEmail" placeholder="Enter Email Id" onChange={(event) => this.setState({emailid:event.target.value})}/>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="txtMobile">Mobile Number <span className="req">*</span></label>
                                            <input type="number" className="form-control form-options" id="txtMobile" placeholder="Enter Mobile Number" onChange={(event) => this.setState({mobilenumber:event.target.value})}/>
                                        </div>
                                        {/* <div className="form-group">
                                            <label htmlFor="txtFile">Upload CV <span className="req">*</span></label>
                                            <input type="file" className="form-control form-options" id="txtFile" name="filename" onChange={(event) => this.setState({cv:event.target.value})}/>
                                        </div> */}
                                        <div className="form-group ulpd-div">
                                            <label>Upload CV <span className="req">*</span></label>
                                            <form class="form form-control" enctype="multipart/form-data">
                                                <div className="file-upload-wrapper">
                                                    <input type="file" name="fileupld" id="fileupld" class="inputfile file-upload-field" data-multiple-caption="{count} files selected" onChange={(event) => this.setState({cv:event.target.files[0]})} />
                                                    {/*<input type="file" name="fileupld" id="fileupld" class="inputfile file-upload-field" data-multiple-caption="{count} files selected" onChange={(event) => this.onchange(event)} />*/}
                                                    <label className="upld-Btn" for="file-7"><span></span> Upload | <i class="fa fa-upload" aria-hidden="true"></i></label>
                                                </div>
                                            </form>
                                            <span className="upld-file-ft">Upload File .pdf .doc .docx .rtf | Max.: 10Mb</span>
                                        </div>
                                        <div className="form-group form-check text-center">
                                            <input type="checkbox" className="form-check-input" id="chkAgree1" onClick={this.clickhandler}/>
                                            <label className="form-check-label" htmlFor="chkAgreeTC">I agree to be contacted on the Mobile Number and Email ID provided </label>
                                        </div>
                                        <h6 className="confmsg3" id="confmsg3">Thankyou,Your message has been successfully sent.</h6>
                                        <div className="text-center">
                                            <button className="submitbtn1" id="submitform" onClick={(event)=>this.postdatahandler(event)}>
                                            Apply Now
                                            </button>
                                        </div>
                                    </div>
                                </form>

            </>
        );
    }
}

export default ApplyFormJdnew;
