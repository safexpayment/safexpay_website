import React from "react";
import "assets/css/safexpay-web.css";
import "./ValueAdded.css"; 
class ValueAdded extends React.Component {

    render() {
        return (
            <>
                <section className="dv-ValueAdded">
                    <div className="container">
                        <div className="row">
                        <div className="col-md-6 col-12">
<h2 className="section-heading" data-aos="fade-up" data-aos-duration="1000">White Label Our ValueAdded To Sell To Other Businesses.</h2>

                            <div className="row" data-aos="fade-up" data-aos-duration="1000">
                                <div className="col-6 rep-wd-100">
                                    <div className="dv-points payment-gateway-points">
                                        <ul className="ul-points ul-payment-gateway-points">
                                            <li className="items"><p>
                                                Bill Payments</p>
                                                </li>
                                            <li className="items"><p>Flight Tickets</p></li>
                                            <li className="items"><p>Bus Tickets</p></li>
                                        </ul>
                                    </div>

                                </div>
                                <div className="col-6 rep-wd-100">
                                    <div className="dv-points payout-solutions-points">
                                        <ul className="ul-points ul-payout-solutions-points">
                                            <li className="items"><p>Gift Cards</p></li>
                                            <li className="items"><p>Mobile Recharges</p></li>
                                            <li className="items"><p>Money Transfer</p></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                        <div className="col-md-6 col-12" data-aos="fade-up" data-aos-duration="1000">
                            
                        <img src={require("assets/img/section/value-added.png")} className="card-img-top sp-img" alt="Value Added" />
                                    
                        </div>
                        
                        </div>
                    </div>
                </section>
            </>
        );
    }
}

export default ValueAdded;
