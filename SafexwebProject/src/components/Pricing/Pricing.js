import React from "react";
import "assets/css/safexpay-web.css";
import "./Pricing.css";
class Pricing extends React.Component {

    render() {
        return (
            <>
                <section className="dv-pricing">
                <div className="dv-pricing-bg-left"></div>
                <div className="dv-pricing-bg-right"></div>
                    <div className="container">
                        <h2 className="section-heading text-white ubuntuMedium pt-5" data-aos="fade-up" data-aos-duration="1000">Aﬀordable Pricing To Send Money In Milliseconds.</h2>
                        <div className="rounded-border-box" data-aos="fade-up" data-aos-duration="1000">
                            
                        <div className="row m-0 dv-header">
                            <div className="col-md-6 col-12">
                                <h2 className="heading">Super Payout Oﬀer</h2>
                                <p className="sub-heading">Send money worth <span className="rupee">50,000/-</span>*</p>
                            </div>
                            <div className="col-md-6 align-self-center text-center col-12">
                                <button className="btn btn-free">FREE</button>
                            </div>
                        </div>

                        <div className="row m-0">
                            <div className="col-md-6 col-12 dv-price-left">
                                <div className="dv-price border-0">
                                    <label className="lbl-name">IMPS | UPI</label>
                                    <label className="lbl-price">5</label>
                                    <label className="lbl-amount">Payout upto <span className="rupee">25,000</span></label>
                                </div>
                                <div className="dv-price pt-0">
                                    <label className="lbl-price">8</label>
                                    <label className="lbl-amount">Payout above <span className="rupee">25,000</span></label>
                                </div>
                                <div className="dv-price border-0">
                                    <label className="lbl-name">NEFT | RTGS</label>
                                    <label className="lbl-price">8</label>
                                </div>
                            </div>
                            <div className="col-md-6 col-12 dv-price-right">
                                <div className="dv-price">
                                    <label className="lbl-name">BENEFICIARY VALIDATION </label>
                                    <label className="lbl-price">2</label>
                                </div>       
                                <div className="dv-price">
                                    <label className="lbl-name">VPA VALIDATION </label>
                                    <label className="lbl-price">2</label>
                                </div>  
                                <div className="dv-price border-0">
                                    <label className="lbl-name">VIRTUAL ACCOUNT CREATION</label>
                                    <label className="lbl-price">2</label>
                                </div>                           
                            </div>
                        </div>
                        <div className="row m-0 dv-note">
                            <div className="col-md-6 col-12">
                                
                            </div>
                            <div className="col-md-6 col-12">
                                <p className="note">(*) Pricing per transaction &amp; GST additional.</p>
                            </div>
                        </div>
                        </div>

                        <div className="rounded-border-box" data-aos="fade-up" data-aos-duration="1000">
                        <div className="dv-facility">
                            <ul className="ul-facility">
                                <li>No Setup fees</li>
                                <li>No Annual Maintainance fees</li>
                                <li>No fees for support</li>
                            </ul>
                        </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}

export default Pricing;
