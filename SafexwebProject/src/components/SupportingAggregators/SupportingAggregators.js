import React from "react";

import "assets/css/safexpay-web.css"
import "./SupportingAggregators.css";

class SupportingAggregators extends React.Component {

    render() {
        return (
            <>
                <section className="dv-SupportingAggregators mt-0">
                    <div className="first-bg"></div>
                    <div className="second-bg"></div>
                    <div className="container">

                        <h2 className="section-heading pt-5" data-aos="fade-up" data-aos-duration="1000">Supporting Aggregators across the globe! </h2>

                        <div className="dv-logo" data-aos="fade-up" data-aos-duration="1000">
                            <img
                                alt="CashFree"
                                className="img-fluid"
                                src={require("assets/img/logos/cashfree.png")}
                            />
                            <img
                                alt="AsiaPay"
                                className="img-fluid"
                                src={require("assets/img/logos/asiapay.png")}
                            />
                            <img
                                alt="Telr"
                                className="img-fluid"
                                src={require("assets/img/logos/telr.png")}
                            />
                            <img
                                alt="BharatiPay"
                                className="img-fluid"
                                src={require("assets/img/logos/bhartipay.png")}
                            />
                            <img
                                alt="PayLive"
                                className="img-fluid"
                                src={require("assets/img/logos/paylive.png")}
                            />
                            <img
                                alt="GrezPAy"
                                className="img-fluid"
                                src={require("assets/img/logos/grezpay.png")}
                            />
                            <img
                                alt="MozzPay"
                                className="img-fluid"
                                src={require("assets/img/logos/mozzpay.png")}
                            />
                            <img
                                alt="MobilPay"
                                className="img-fluid"
                                src={require("assets/img/logos/mobilpay.png")}
                            />
                            <img
                                alt="Froogal"
                                className="img-fluid"
                                src={require("assets/img/logos/froogal.png")}
                            />
                            <img
                                alt="PayPik"
                                className="img-fluid"
                                src={require("assets/img/logos/paypik.png")}
                            />
                        </div>
                    </div>
                </section>

            </>
        );
    }
}

export default SupportingAggregators;
