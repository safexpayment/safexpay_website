import React from "react";
import "assets/css/safexpay-web.css"
import "./Banking-experience.css";

class Bankingexperience extends React.Component {
    render() {
        return (
            <>
                <section className="dv-Banking-experience mb-5 pb-5">
                    <div className="container">
                        <h2 className="section-heading text-center" data-aos="fade-up" data-aos-duration="1000">
                            Think beyond banking experience
                        </h2>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="card" data-aos="fade-up"
                                    data-aos-duration="1000">
                                    <div className="row no-gutters">
                                        <div className="col-md-4 dv-img">
                                            <img src={require("assets/img/icons/instant-payouts.png")} className="card-img-top" alt="Instant Payouts" />
                                        </div>
                                        <div className="col-md-8"><div className="card-body">
                                            <p className="card-text">Instant Payouts</p>
                                        </div>
                                            <div className="card-footer">
                                                <a className="more" href="#">Learn more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="card" data-aos="fade-up"
                                    data-aos-duration="1000">
                                    <div className="row no-gutters">
                                        <div className="col-md-4 dv-img">
                                            <img src={require("assets/img/icons/instant-refunds.png")} className="card-img-top" alt="Instant Refunds" />
                                        </div>
                                        <div className="col-md-8"><div className="card-body">
                                            <p className="card-text">Instant Refunds</p>
                                        </div>
                                            <div className="card-footer">
                                                <a className="more" href="#">Learn more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div className="col-md-6">
                                <div className="card" data-aos="fade-up"
                                    data-aos-duration="1000">
                                    <div className="row no-gutters">
                                        <div className="col-md-4 dv-img">
                                            <img src={require("assets/img/icons/payment-without-user.png")} className="card-img-top" alt="Payment" />
                                        </div><div className="col-md-8"> <div className="card-body">
                                            <p className="card-text">Payment Without User Addition</p>
                                        </div>
                                            <div className="card-footer">
                                                <a className="more" href="#">Learn more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div className="col-md-6">
                                <div className="card" data-aos="fade-up"
                                    data-aos-duration="1000">
                                    <div className="row no-gutters">
                                        <div className="col-md-4 dv-img">
                                            <img src={require("assets/img/icons/powerful-api.png")} className="card-img-top" alt="Powerful API" />
                                        </div>
                                        <div className="col-md-8">
                                            <div className="card-body">
                                                <p className="card-text">Powerful API and Intuitive dashboard</p>
                                            </div>
                                            <div className="card-footer">
                                                <a className="more" href="#">Learn more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}
export default Bankingexperience;
