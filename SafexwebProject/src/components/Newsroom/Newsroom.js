import React from "react";
import "assets/css/safexpay-web.css"
import "./Newsroom.css";

class Newsroom extends React.Component {
    render() {
        return (
            <>
                <section className="dv-Newsroom">
                    <div className="container">
                        <h2 className="section-heading text-center mb-0 pb-0" data-aos="fade-up" data-aos-duration="1000">
                            Newsroom
                        </h2>
                        <p className="text-center pb-3" data-aos="fade-up" data-aos-duration="1000">
                            To know us more better!
                        </p>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="card" data-aos="fade-up"
                                    data-aos-duration="1000">
                                    <div className="row no-gutters">
                                        <div className="col-md-5 dv-img">
                                            <img src={require("assets/img/logos/indiatoday.png")} className="card-img-top" alt="" />
                                        </div>
                                        <div className="col-md-7"><div className="card-body">
                                            <p className="card-text">These 3 start-ups will get Rs 25 lakh seed funding
                                            each: TIEDS &amp; IIT Roorkee</p>
                                        </div>
                                            <div className="card-footer">
                                                <a className="more" href=" https://www.indiatoday.in/education-today/news/story/these-3-start-ups-will-get-rs-25-lakh-seed-funding-each-tieds-iit-roorkee-1578517-2019-08-08" target="_blank">Learn more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="card" data-aos="fade-up"
                                    data-aos-duration="1000">
                                    <div className="row no-gutters">
                                        <div className="col-md-5 dv-img">
                                            <img src={require("assets/img/logos/enterprise.png")} className="card-img-top" alt="" />
                                        </div>
                                        <div className="col-md-7"><div className="card-body">
                                            <p className="card-text">Deepak Kalambkar joins Safexpay as AVP Infrastructure</p>
                                        </div>
                                            <div className="card-footer">
                                                <a className="more" href="https://www.enterpriseitworld.com/deepak-kalambkar-joins-safexpay-as-avp-infrastructure/" target="_blank">Learn more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div className="col-md-6">
                                <div className="card" data-aos="fade-up"
                                    data-aos-duration="1000">
                                    <div className="row no-gutters">
                                        <div className="col-md-5 dv-img">
                                            <img src={require("assets/img/logos/siliconindia.png")} className="card-img-top" alt="" />
                                        </div><div className="col-md-7"> <div className="card-body">
                                            <p className="card-text">10 Most Promising E-Payment Services Provider - 2017</p>
                                        </div>
                                            <div className="card-footer">
                                                <a className="more" href="https://finance.siliconindia.com/ranking/epayment-services-provider-2017-rid-255.html" target="_blank">Learn more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div className="col-md-6">
                                <div className="card" data-aos="fade-up"
                                    data-aos-duration="1000">
                                    <div className="row no-gutters">
                                        <div className="col-md-5 dv-img">
                                            <img src={require("assets/img/logos/forbes.png")} className="card-img-top" alt="" />
                                        </div>
                                        <div className="col-md-7">
                                            <div className="card-body">
                                                <p className="card-text">GMI &amp; Forbes India release the list of Top 100 Great People Managers 2020</p>
                                            </div>
                                            <div className="card-footer">
                                                <a className="more" href="https://medium.com/@greatmanagerinstitute/forbes-india-and-gmi-honours-entrepreneurs-in-the-times-of-crisis-be4c37be0564" target="_blank">Learn more</a>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>                           
                        </div>

                        <div className="container dv-morenews">
                            <h3 className="section-heading text-left pb-0" data-aos="fade-up" data-aos-duration="1000">
                                More News
                            </h3>
                            <div className="row">
                                <div className="col-md-2 dv-img">
                                    <img src={require("assets/img/logos/siliconindia-s.png")} className="card-img-top" alt="" />
                                </div>
                                <div className="col-md-8 dv-text">
                                    <p>SafexPay: Facilitates White &amp; Private - Label Payments via
                                        Innovative Solutions &amp; Technologies</p>
                                    <span className="lblDate">22 December, 2020</span>
                                </div>
                                <div className="col-md-2 dv-read">
                                    <a className="more" href="https://finance.siliconindia.com/vendor/safexpay-facilitates-white-private-label-payments-via-innovative-solutions-technologies--cid-6064.html" target="_blank">Read more</a>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-2 dv-img">
                                    <img src={require("assets/img/logos/mirrorreview.png")} className="card-img-top" alt="" />
                                </div>
                                <div className="col-md-8 dv-text">
                                    <p>SafexPay: Providing World-class White Label Payment Solutions</p>
                                    <span className="lblDate">&nbsp;</span>
                                </div>
                                <div className="col-md-2 dv-read">
                                    <a className="more" href="https://www.mirrorreview.com/safexpay-world-class-payment-solutions/" target="_blank">Read more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}
export default Newsroom;
