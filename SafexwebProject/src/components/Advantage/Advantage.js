import React from "react";
import "assets/css/safexpay-web.css"
import "./Advantage.css";

class Advantage extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Advantage mt-3 pt-3">
                    <div className="container">
                        <h2 className="section-heading">Safexpay Payment Gateway advantages</h2>
                        <div className="row">
                            <div className="col-md-4 col-12" data-aos="fade-up"
     data-aos-duration="1000">
                                <div className="card">
                                    <img src={require("assets/img/icons/business.png")} className="card-img-top" alt="Multiple Payment Methods" />
                                    <div className="card-body">
                                        <h5 className="card-title">100+ Payment Options</h5>
                                        <p className="card-text">Widest range of payment options such as Credit Cards, Debit Cards, Netbanking, UPI, EMI, International cards etc for your customers.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12" data-aos="fade-up"
     data-aos-duration="1000">
                                <div className="card">
                                    <img src={require("assets/img/icons/business-size.png")} className="card-img-top" alt="Made For All Business Sizes" />
                                    <div className="card-body">
                                        <h5 className="card-title">Made For All Business Sizes</h5>
                                        <p className="card-text">Any business size can collect payment across all platform web or mobile. Our support teams are available for all your needs.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12" data-aos="fade-up"
     data-aos-duration="1000">
                                <div className="card">
                                    <img src={require("assets/img/icons/uptime.png")} className="card-img-top" alt="24X7 Uptime" />
                                    <div className="card-body">
                                        <h5 className="card-title">24X7 Uptime</h5>
                                        <p className="card-text">Our platform is Up all the time, letting your customers transact at any time of the day or month.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12" data-aos="fade-up"
     data-aos-duration="1000">
                                <div className="card">
                                    <img src={require("assets/img/icons/honest.png")} className="card-img-top" alt="Honest &amp; Flexible Pricing" />
                                    <div className="card-body">
                                        <h5 className="card-title">Honest &amp; Flexible Pricing</h5>
                                        <p className="card-text">Our flexible pricing plans let you achieve scale and profitablity so you can build sustainable business<br /><br /> </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12" data-aos="fade-up"
     data-aos-duration="1000">
                                <div className="card">
                                    <img src={require("assets/img/icons/secure.png")} className="card-img-top" alt="Secure &amp; Reliable" />
                                    <div className="card-body">
                                        <h5 className="card-title">Secure &amp; Reliable</h5>
                                        <p className="card-text">World class AES 256 bit encryption for all your transactions. Our platform is PCI DSS 3.2.1 compliant. Tokenization to handle saved cards.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12" data-aos="fade-up"
     data-aos-duration="1000">
                                <div className="card">
                                    <img src={require("assets/img/icons/report.png")} className="card-img-top" alt="Analytics &amp; Reporting" />
                                    <div className="card-body">
                                        <h5 className="card-title">Analytics &amp; Reporting</h5>
                                        <p className="card-text">Be on top of your cash flows, get clear reports on your settlements and fees. Customise your reports to analyse your business.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </>
        );
    }
}

export default Advantage;
