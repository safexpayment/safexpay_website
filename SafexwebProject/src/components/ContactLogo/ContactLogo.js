import React from 'react';
import "./ContactLogo.css";

const ContactLogo = (props)=>{
    var attachclasses = "Contact"
    if(props.flag)
    attachclasses = "move"
    return(
       
            <img id="logo-contact" className={attachclasses} onClick={props.click} alt="Contact-US" src={require("assets/img/icons/contact-us.png")} />
    )
}

export default ContactLogo;
