import React, { Component } from "react";
//import "assets/css/safexpay-web.css"
import "./Applyformjd.css";
import axios from 'axios';



class Applyformjd extends Component {

    state={
        name:'',
        emailid:'',
        mobilenumber:'',
        cv:'',
        header:'',
        i_agree:"false"
    }

    flag = 0;
    datasend = false;

    componentDidMount(){
       /* if(this.props.header!=undefined)
        {
        console.log("props===================",this.props.header.name);
        this.state.header = this.props.header.name;
        var headerfirst = this.props.header.name;
        document.getElementById("formhead").innerHTML = headerfirst;
        }*/
        console.log("props========================>",this.props.pos);
        this.setState(
            {
                header:this.props.pos
               // header:this.props.header.name

            }
        )
    }


    postdatahandler=(e)=>
    {
        e.preventDefault();

        var emailid = this.state.emailid;
        var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/; 
        //console.log('====>>>>>>>', pattern.test(emailid));
        var validemail = pattern.test(emailid);

        /*console.log("cv------------",this.state.cv);
        console.log("cv size------------",this.state.cv.fileName);
        console.log("name",document.getElementById("txtFile").files[0].name);
        console.log("filesize",document.getElementById("txtFile").files[0].size);
        console.log("filesize",document.getElementById("txtFile").files[0].type);
        console.log("filesize",document.getElementById("txtFile").files[0].path);*/

        var checkfg = document.getElementById("chkAgree1").checked;
        console.log("checked==============>",checkfg);


        if(this.state.name == "" || this.state.mobilenumber == "" || this.state.emailid == "" || this.state.cv == "")
        {
            this.datasend = false;
           
        }
        else{
            this.datasend = true
        }
        
        const data ={
            "full_name": this.state.name,
            "email_id": this.state.emailid,
            "mobile_no": this.state.mobilenumber,
            "form_type":"Support",
            "description":this.state.description
        }

        //console.log("checktype===========>",this.state.i_agree);
        console.log("props========================>",this.props.pos);
        console.log("props========================>",this.state.header);


        if(checkfg)
        {
        if(this.datasend)
        {
        if(validemail)
        {
        console.log("data",data);
        this.flag=1;
        document.getElementById("submitform").innerHTML = "Loading.....";
           
        axios.post('https://www.avantgardepayments.com/agadmin/api/emailSender/post',data).then(response=>{ //https://
            console.log(response);
            this.datasend=false;

            if(response.data.message = "Success")
            {
            document.getElementById("submitform").style.backgroundColor = 'var(--darkgreen)';
            document.getElementById("submitform").innerHTML = "Send";
            document.getElementById("confmsg2").style.visibility = "visible"
            }
            if(response.data.message = "Success")
        {
            document.getElementById("txtFirstName").value = '';
            document.getElementById("txtEmail").value = '';
            document.getElementById("txtMobile").value = '';
            document.getElementById("txtFile").value = '';
            document.getElementById("chkAgree1").checked = false;
        }
        if(response.data.message = "Success"){
            setTimeout(() => {
            document.getElementById("submitform").style.backgroundColor = 'var(--blue)';
            document.getElementById("submitform").innerHTML = "Apply Now";
            document.getElementById("confmsg2").style.visibility = "hidden";
                
            }, 4000);
        }
        })
    }
    else{
        alert("Please Enter Valid Email Id");
    }
}
else{
    alert("please provide data and upload cv then enter submit button");
}
}
else{
    alert("please click in checkbox before click on Apply Now button");
}
}

    render(){
    var attchedClasses =["dv-Registration-content", "Open","formjd"];
   /* if(this.props.open)
    attchedClasses = ["dv-Registration-content","Open"];*/

        return (
            <>
            
                <section className={attchedClasses.join(' ')}>      
                <form>     
                            
                                <div className="dv-registration-form dv-form pt-5">
                                    <div className="form-group">
                                    <div className="sphd">
                                        <div className="section-heading sphd1" id="formhead">Join our team. Apply Now!</div>
                                    </div>
                                        <label htmlFor="txtFirstName">Name <span className="req">*</span></label>
                                        <input type="text" className="form-control form-options" id="txtFirstName" placeholder="Enter Name" onChange={(event)=>this.setState({name:event.target.value})}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtEmail">Email Address <span className="req">*</span></label>
                                        <input type="text" className="form-control form-options" id="txtEmail" placeholder="Enter Email Address" onChange={(event) => this.setState({emailid:event.target.value})}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtMobile">Mobile Number <span className="req">*</span></label>
                                        <input type="number" className="form-control form-options" id="txtMobile" placeholder="Enter Mobile Number" onChange={(event) => this.setState({mobilenumber:event.target.value})}/>
                                    </div>
                                    {/*<div className="form-group">
                                        <label htmlFor="txtFirstName">Description <span className="req">*</span></label>
                                        <textarea type="text" className="form-control form-options" id="txtTextarea" placeholder="How can we assist you? Explain your query in brief." onChange={(event) => this.setState({description:event.target.value})}/>
        </div>*/}
                                    <div className="form-group">
                                        <label htmlFor="txtFile1">Upload CV<span className="req">*</span></label>
                                        <input type="file" className="form-control form-options" id="txtFile1" max-size="10000" name="filename" onChange={(event) => this.setState({cv:event.target.value})}/>
                                    </div>
                                    <div className="form-group form-check text-center my-4">
                                        <input type="checkbox" className="form-check-input" id="chkAgree1"/>
                                        {/*onChange={(event) => this.setState({i_agree:"true"})}*/}
                                        <label className="form-check-label chk-input" htmlFor="chkAgreeTC">I agree to be contacted on the mobile number and email id provided </label>
                                    </div>
                                    <h6 className="confmsg2" id="confmsg2">Thankyou,Your message has been successfully sent.</h6>
                                    <div className="text-center">
                                        <button className="submitbtn01" id="submitform" onClick={(event)=>this.postdatahandler(event)}>
                                        Apply Now
                                        </button>
                                    </div>
                                   {/* <text className="texthr">Or apply to hr@safexpay.com</text>*/}
                                   
                                    
                                </div>
                </form>
                           
                       
                   
                </section>

            </>
        );
    }
}

export default Applyformjd;
