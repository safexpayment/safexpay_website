import React from "react";
import "assets/css/safexpay-web.css";
import "./Rates.css";
import Contactuspage from "components/Contactuspage/Contactuspage";

class Rates extends React.Component {

    state={
        open:false
    }

    showcontactuspage =(props)=>{
        console.log("button click.....");
        this.setState({
            open:!this.state.open
        })

    }

    closepage=()=>{
        this.setState({
            open:!this.state.open
        })
    }

    render() {
        return (
            <>
                <Contactuspage open={this.state.open} click={this.closepage}/>
                <section className="dv-Rates">
                    <div className="container">
                        <h2 className="section-heading text-white pb-0" data-aos="fade-up" data-aos-duration="1000">We Have The Best Rates In The Country!</h2>
                        <p className="text-center text-white" data-aos="fade-up" data-aos-duration="1000">Honest &amp; ﬂexible pricing for your business. </p>
                        <div className="rounded-border-box" data-aos="fade-up" data-aos-duration="1000">
                        <div className="dv-Rates-content">
                            <div className="row m-0">
                            <div className="col-md-12 col-12">
                                <h2 className="section-heading">Aﬀordable Pricing To <br /> Accept Payments From Anyone</h2>
                            </div>
                            <div className="col-md-6 col-12 d-flex align-items-center border-right">
                                <div className="dv-price">
                                    <label className="lbl-price">FREE</label>
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="dv-price">
                                    <ul className="ul-points ul-rate-points">
                                        <li className="items">One Time Setup</li>
                                        <li className="items">Annual Maintenance</li>
                                    </ul>
                                </div> 
                                </div>
                            <div className="col-md-6 col-12 d-flex align-items-center border-right">
                                <div className="dv-price">
                                    <label className="lbl-price">1.90%</label>
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="dv-price">
                                    <ul className="ul-points ul-rate-points">
                                        <li className="items">Net Banking 
                                            <span className="note">(SBI<sup>+</sup>, HDFC<sup>+</sup>, ICICI<sup>+</sup>, Axis<sup>+</sup>, Kotak &amp; 50 more.)</span>
                                        </li>
                                        <li className="items">Indian Debit &amp; Credit Card </li>
                                        <li className="items">UPI
                                            <span className="note">(Google Pay, PhonePe, Paytm, BHIM &amp; other UPI apps)</span>
                                        </li>
                                        <li className="items">Wallets
                                            <span className="note">(Amazon Pay<sup>+</sup>, Paytm, Freecharge, Mobikwik,etc )  </span>
                                        </li>
                                    </ul>
                                </div> 
                                </div>
                                <div className="col-md-6 col-12 d-flex align-items-center border-right">
                                <div className="dv-price">
                                    <label className="lbl-price">2.95%</label>
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="dv-price">
                                    <ul className="ul-points ul-rate-points">
                                        <li className="items">Amex Card<sup>+</sup></li>
                                        <li className="items">EMI<sup>+</sup>
                                            <span className="note">(Axis, ICICI, Standard Chartered, Kotak &amp; more...)</span>
                                        </li>
                                        <li className="items">pay Later<sup>+</sup>
                                            <span className="note">(LazyPay, Simpl, ePayLater, Ola Money)</span>
                                        </li>
                                    </ul>
                                </div>                           
                            </div>
                        </div>
                        </div>
                        <div className="row m-0">
                            <div className="col-md-12 col-12 text-right dv-note">
                                <p className="note">(*) Pricing per transaction &amp; GST additional. | (+) Subject to approval from banking partners. </p>
                            </div>
                        </div>
                        </div>

                        <div className="shadow-box" data-aos="fade-up" data-aos-duration="1000">
                        <h2 className="section-heading text-center text-md-left py-0">Looking For Customized Pricing Or International Payments? </h2>
                        <button className="btn btn-blue-rounded mt-4" onClick={this.showcontactuspage}>TALK TO US</button>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}

export default Rates;
