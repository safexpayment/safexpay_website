import React from "react";
import "assets/css/safexpay-web.css";
import "./PaymentSolutions.css";

//Owl Carousel Libraries and Module
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
//Owl Carousel Settings

import Contactuspage from "components/Contactuspagepay/Contactuspagepay";

const options = {
    margin: 10,
    responsiveClass: true,
    nav: true,
    autoplay: true,
    rewind: true,
    navText: ["&#8249;", "&#8250;"],
    smartSpeed: 1000,
    dots: true,
      dotsEach: true,
      dotData: true,
    loop: true,
    responsive: {
        0: {
            items: 1,
        },
        400: {
            items: 1,
        },
        600: {
            items: 1,
        },
        767: {
            items: 1,
        },
        768: {
            items: 3,
        }
  },
};
class PaymentSolutions extends React.Component {
    state={
        open:false
    }

    showcontactuspage =(props)=>{
        console.log("button click.....");
        this.setState({
            open:!this.state.open
        })

    }

    closepage=()=>{
        this.setState({
            open:!this.state.open
        })
    }

    render() {
        return (
            <>
                <Contactuspage open={this.state.open} click={this.closepage}/>
                <section className="dv-PaymentSolutions">
                    <div className="container">
                        {/* <h2 className="section-heading" data-aos="zoom-in-up" data-aos-duration="1000">We Understand Your Business Needs</h2> */}
                        <h2 className="section-heading" data-aos="zoom-in-up" data-aos-duration="1000">One Price For Any Of Our Payment Solutions </h2>
                        <div className="PaymentSolutionsBoxSlider" data-aos="fade-up" data-aos-duration="1000">
                        <OwlCarousel className="slider-items owl-carousel" {...options}>
                      <div className="item">     
                            <div className="card">
                                <img src={require("assets/img/icons/custom.png")} className="card-img-top" alt="" />
                                <div className="card-body">
                                    <h5 className="card-title">Fully Customised Payment Experience</h5>
                                    <p className="card-text">Give power to your designers and developers to create something magical.
                                    <br />
                                    </p>
                                </div>
                                {/* <div className="card-footer">
                                    <a className="more" href="/business/accept-payments#dv-first-payment-bg">Learn more</a>
                                </div> */}
                            </div>
                            </div>
                            <div className="item">
                            <div className="card">
                                <img src={require("assets/img/icons/quick.png")} className="card-img-top" alt="" />
                                <div className="card-body">
                                    <h5 className="card-title">Quick &amp; Easy Standard Checkout Experience</h5>
                                    <p className="card-text">Fast integration, all error validations in built, super success rates, minor customisations! 
                                    <br />
                                    </p>
                                </div>
                                {/* <div className="card-footer">
                                    <a className="more" href="/business/accept-payments#dv-second-payment-bg">Learn more</a>
                                </div> */}
                            </div>
                            </div>
                            <div className="item">
                            <div className="card">
                                <img src={require("assets/img/icons/plugin.png")} className="card-img-top" alt="" />
                                <div className="card-body">
                                    <h5 className="card-title">Plugins to Develop Cart or Apps</h5>
                                    <p className="card-text">Just enter the keys provided and get accepting payments in minutes. <br /><br /></p>
                                </div>
                                {/* <div className="card-footer">
                                    <a className="more" href="/business/accept-payments#PluginsToDevelop">Learn more</a>
                                </div> */}
                            </div>
                            </div>
                            <div className="item">
                            <div className="card">
                                <img src={require("assets/img/icons/pay.png")} className="card-img-top" alt="" />
                                <div className="card-body">
                                    <h5 className="card-title">Loan Disbursal</h5>
                                    <p className="card-text">Instant loan disbursal with zero wait time for ﬁnancial organizations to disburse loans to consumers and businesses  24*7 .</p>
                                </div>
                                {/* <div className="card-footer">
                                    <a className="more" href="/business/accept-payments#dv-fourth-payment-bg">Learn more</a>
                                </div> */}
                            </div>
                            </div>
                            <div className="item">
                            <div className="card">
                                <img src={require("assets/img/icons/insurance.png")} className="card-img-top" alt="" />
                                <div className="card-body">
                                    <h5 className="card-title">Insurance claims</h5>
                                    <p className="card-text">Reduce wait for your customers to get their insurance payouts. You approve the insurance claim &amp; we help you settle it instantly. </p>
                                </div>
                                {/* <div className="card-footer">
                                    <a className="more" href="/business/accept-payments#dv-fourth-payment-bg">Learn more</a>
                                </div> */}
                            </div>
                            </div>
                       
                        </OwlCarousel>
                        </div>
                        <div className="row" data-aos="zoom-in-up" data-aos-duration="1000">
                            <div className="col-md-9">
                                <h2 className="section-heading rep-text-center text-left">Looking For Customized Pricing Or
International Payments?</h2>                        
                            </div>
                            <div className="col-md-3 pt-2">
                                <div className="btnDiv rep-text-center">
                                    <button className="btn btn-blue-rounded mg-tp0 mt-5"  onClick={this.showcontactuspage}>Talk To Us</button>
                                </div>
                            </div>
                        </div>
                        </div>
                </section>
            </>
        );
    }
}

export default PaymentSolutions;
