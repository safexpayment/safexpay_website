import React from "react";
import "assets/css/safexpay-web.css";
import "./ToStart.css";
class ToStart extends React.Component {

    render() {
        return (
            <>
                <section className="dv-ToStart pt-5">
                <div className="container">
                        <h2 className="section-heading pt-5" data-aos="fade-up" data-aos-duration="1000">
                            <span className="color-darkgreen d-block pb-5">PAYMENT GATEWAY</span>
                            Everything That You Need To Start An <br /> Online Payments Business.
                        </h2>
                        <div className="row" data-aos="fade-up" data-aos-duration="1000">
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/merchant.png")} className="card-img-top" alt="" />
                                    <div className="card-body">
                                        <h5 className="card-title">Merchant Lifecycle Management</h5>
                                        <p className="card-text">Perform Risk Check &amp; KYC, Conﬁgure Pricing &amp; Surcharges, Issue or Revoke
                                            API, Credentials, Edit Limits for Transactions, Change Settlement
Account, Send important Alerts, Hold
Settlements &amp; much more.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/payment-option.png")} className="card-img-top" alt="" />
                                    <div className="card-body">
                                        <h5 className="card-title">Payment Options &amp; Smart Routing</h5>
                                        <p className="card-text">Enable/Disable 100+ Payment Modes with support for Cards, Multi-Currency &amp; Local Payment Options,
Recurring Payments. Create Smart Routing Rules on the basis of Pricing,
Success Rates &amp; Downtimes. </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/reconciliation.png")} className="card-img-top" alt="" />
                                    <div className="card-body">
                                        <h5 className="card-title">Flexible Reconciliation &amp; Settlement</h5>
                                        <p className="card-text">Get automatically reconcilied transactions with bank &amp; gateway
partners. Setup settlement rules per merchant which can be settled by you or by us. Create Rolling Reserves to
handle future chargebacks &amp; refunds.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/integration.png")} className="card-img-top" alt="" />
                                    <div className="card-body">
                                        <h5 className="card-title">Integration Kits &amp; Developer Guides</h5>
                                        <p className="card-text">Multiple integration options - Hosted checkout page, Merchant hosted
page, API based, Payment by SMS/Email Links, QR Codes, Payment Buttons, E-commerce Plugins, Server
side SDKs, Android, iOS &amp; React SDKs. </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/dashboard-3.png")} className="card-img-top" alt="" />
                                    <div className="card-body">
                                        <h5 className="card-title">Inbuilt Dashboards or APIs to build your own</h5>
                                        <p className="card-text">Dashboards for your team &amp; your
merchants - Live Transactions, Payment Analytics, User &amp; Role Management, Partial/Bulk Refunds,
Chargeback Management &amp; Dispute Resolution.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/security.png")} className="card-img-top" alt="" />
                                    <div className="card-body">
                                        <h5 className="card-title">Security &amp; Certiﬁcations</h5>
                                        <p className="card-text">AES 256 bit encryption &amp; TLS 1.2 for secure data transmission, We are PCI
DSS 3.2.1, the global standard in card entry &amp; transmission, With ISO 27001 : 2013 certiﬁed we have strict access
rules and use VPN / Firewalls to protect against cyber attacks</p> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}

export default ToStart;
