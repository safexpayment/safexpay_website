import React from "react";
import "assets/css/safexpay-web.css"
import "./ForPartners.css";

class ForPartners extends React.Component {

    render() {
        return (
            <>
                <section className="dv-ForPartners mt-md-5 pt-md-5">
                    <div className="container mt-md-5 pt-md-5">
                        <div className="row">
                            <div className="col-lg-3 col-md-12 col-12 pt-5" data-aos="fade-right" data-aos-duration="1000">
                                <img src={require("assets/img/icons/for-partners.png")} className="card-img img-partner" alt="" />
                                <h2 className="section-heading rep-text-center text-left">Our Partners Come In All Shapes &amp; Sizes</h2>
                            </div>
                            <div className="col-lg-9 col-md-12 col-12 pr-0" data-aos="fade-left" data-aos-duration="1000">
                                <div className="row">
                                    <div className="col-md-6 col-12">
                                        <div className="row no-gutters">
                                            <div className="col-md-3">
                                                <img src={require("assets/img/icons/developer.png")} className="card-img" alt="" />

                                            </div>
                                            <div className="col-md-9">
                                                <div className="card-body pt-0">
                                                    <h5 className="card-title">Developer, Designer or Freelancer</h5>
                                                    <p className="card-text">Making a website or mobile app? Use our simple integration for all your projects to create a secure &amp; customised payment acceptance interface. Reuse your code &amp; designs. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-12">
                                        <div className="row no-gutters">
                                            <div className="col-md-3">
                                                <img src={require("assets/img/icons/enterprise.png")} className="card-img" alt="" />

                                            </div>
                                            <div className="col-md-9">
                                                <div className="card-body pt-0">
                                                    <h5 className="card-title">Enterprise Platform Provider or Integrator</h5>
                                                    <p className="card-text">Create payment collection and disbursal ﬂows in your enterprise or SAAS software to optimise money movement processes for your clients. Let us help you enhance your capabilities. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-12">
                                        <div className="row no-gutters">
                                            <div className="col-md-3">
                                                <img src={require("assets/img/icons/social.png")} className="card-img" alt="" />

                                            </div>
                                            <div className="col-md-9">
                                                <div className="card-body pt-0">
                                                    <h5 className="card-title">Social Selling &amp; Automated Buying Experiences</h5>
                                                    <p className="card-text">Creating tools or bots for sellers to sell on Instagram, Whatsapp or Messenger? Embed our payment gateway in your tools and see conversions sky rocket for your customers.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-12">
                                        <div className="row no-gutters">
                                            <div className="col-md-3">
                                                <img src={require("assets/img/icons/digital.png")} className="card-img" alt="" />

                                            </div>
                                            <div className="col-md-9">
                                                <div className="card-body pt-0">
                                                    <h5 className="card-title">Digital India &amp; Next Billion Users</h5>
                                                    <p className="card-text">Working on projects or platforms to empower Bharat to transact digitally. Get access to the latest payment methods at aﬀordable pricing. Let us join hands in your noble mission.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-12">
                                        <div className="row no-gutters">
                                            <div className="col-md-3">
                                                <img src={require("assets/img/icons/automation.png")} className="card-img" alt="" />

                                            </div>
                                            <div className="col-md-9">
                                                <div className="card-body pt-0">
                                                    <h5 className="card-title">Last Mile Payment Automation</h5>
                                                    <p className="card-text">Creating services to automate collection of payments in kirana stores or delivery solutions? Build on top of our payments services so that you can focus on creating the best experience for your customers.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-12">
                                        <div className="row no-gutters">
                                            <div className="col-md-3">
                                                <img src={require("assets/img/icons/startup.png")} className="card-img" alt="" />

                                            </div>
                                            <div className="col-md-9">
                                                <div className="card-body pt-0">
                                                    <h5 className="card-title">Startup Accelerators, Enablers, Incubators</h5>
                                                    <p className="card-text">Running a hackathon or competition? Or simply need a robust solution for your incubatee companies. Speak to us and we will provide you with every tool you need!</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </section>

            </>
        );
    }
}

export default ForPartners;
