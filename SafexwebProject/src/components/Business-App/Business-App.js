import React from "react";
import "assets/css/safexpay-web.css";
import "./Business-App.css";

class BusinessApp extends React.Component {
    render() {

        return (
            <>
                <section className="dv-Business-App">
                    <div className="first-bg"></div>
                    <div className="second-bg"></div>
                    <div className="container-fluid pl-2" data-aos="fade-up" data-aos-duration="1000">
                        <h2 className="section-heading pt-5 mb-5 pb-5 text-center">
                            <span className="color-darkgreen d-block pb-5">BUSINESS APP</span>
                        </h2>
                        <div className="row">
                            <div className="col-md-5 offset-md-1 col-12">
                                <h2 className="section-heading mt-4 text-center text-md-left text-white rep-pm-0">
                                    <br /> Your App, Your Brand,
                                Your Look &amp; Feel</h2>

                                <p className="rep-text-center mg-t">
                                    With Business App, You can collect payment by sending
                                    payment links over email, SMS , QR code and collect payment
                                    instantly.
                                    <br />
                                    Using payment Links, You can easily receive payment from
                                    anytime from anywhere. <br />
                                    Link can be shared for ﬁxed amount and variable amount.
                                    Enable to make payment From remote Location.</p>
                                                                    <p className="ubuntuBold rep-text-center">
                                                                        <strong className="font-s20">
                                                                            Features</strong>
                                                                            <br/>
                                    Faster payments |  Instant Activation
                                    No requirement of website  |  Analytics  |  Refund</p>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="img-business-app text-center">
                                    <img
                                        alt="Business Application"
                                        className="img-fluid sp-img"
                                        src={require("assets/img/section/business-app.png")}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}

export default BusinessApp;
