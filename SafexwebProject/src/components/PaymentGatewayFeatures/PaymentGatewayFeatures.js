import React from "react";
import "assets/css/safexpay-web.css";
import "./PaymentGatewayFeatures.css";

class PaymentGatewayFeatures extends React.Component {
    render() {
        return (
            <>
                <section className="dv-PaymentGatewayFeatures">
                    <div className="first-bg"></div>
                    <div className="container-fluid pl-2" data-aos="fade-up" data-aos-duration="1000">
                        <div className="row">
                            <div className="col-md-4 offset-md-1 col-12">
                                <h2 className="section-heading mt-4 text-center text-md-left text-white">
                                    <img
                                        alt="Payment Gateway Service"
                                        className="img-fluid img-paymentfeature"
                                        src={require("assets/img/icons/payment-feature.png")}
                                    /> <br />
                                    Payment Gateway Features
                                    </h2>
                                <p className="text-white rep-text-center">Now Available for All</p>

                            </div>
                            <div className="col-md-7 col-12 pr-md-0">
                                <div className="rounded-border-box container dv-GatewayFeatures pr-md-5">
                                    <div className="dv-GatewayFeaturesHolder mr-md-5">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="dv-points">
                                                <h3 className="heading">
                                                Tokenisation
                                                </h3>
                                                <p className="desc">
                                                Storing your card details in vault,
helps faster checkout and seamless
experience to customers.
                                                </p>
                                            </div>
                                            <div className="dv-points">
                                                <h3 className="heading">
                                                Retry Option
                                                </h3>
                                                <p className="desc">
                                                Retry your payments from other
payment mode, without going back
again to merchant site. This features
help to improve success ratio by
more than 10%.
                                                </p>
                                            </div>
                                            <div className="dv-points">
                                                <h3 className="heading">
                                                Multi Account Settlement
                                                </h3>
                                                <p className="desc">
                                                Helps to Split the settlement to
multiple account for  various sellers
or products.
                                                </p>
                                            </div>
                                            <div className="dv-points">
                                                <h3 className="heading">
                                                Multi-Currency Payments
                                                </h3>
                                                <p className="desc">
                                                Supports 100+ currencies for
collecting the payment from customer
across the world with faster
settlements. <br /><br />
                                                </p>
                                            </div>
                                            <div className="dv-points">
                                                <h3 className="heading">
                                                Bulk Refunds / Partial Refunds
                                                </h3>
                                                <p className="desc">
                                                You can refund the entire amount of
a transaction or portions of it.
                                                </p>
                                            </div>
                                            <div className="dv-points">
                                                <h3 className="heading">
                                                Dashboard
                                                </h3>
                                                <p className="desc">
                                                Dashboards for your team &amp; your
merchants - Live Transactions,
Payment Analytics, User &amp; Role
Management, Partial/Bulk Refunds,
Chargeback Management &amp;
Dispute Resolution.
                                                </p>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="dv-points">
                                                <h3 className="heading">
                                                International Payments
                                                </h3>
                                                <p className="desc">
                                                Process Your payments from international
cards across 200+ countries with
faster settlements
                                                </p>
                                            </div>
                                            <div className="dv-points">
                                                <h3 className="heading">
                                                Smart Routing
                                                </h3>
                                                <p className="desc">
                                                Create Smart Routing Rules on the
basis of Pricing, Success Rates &amp;
Downtimes among diﬀerent Gateway
to Improve the Success Ratio.
                                                </p>
                                            </div>
                                            <div className="dv-points">
                                                <h3 className="heading">
                                                Surcharge
                                                </h3>
                                                <p className="desc">
                                                Oﬀer merchant to pass transaction
charges to user as per business
needs.
                                                </p>
                                            </div>
                                            <div className="dv-points">
                                                <h3 className="heading">
                                                Education and society Payment
                                                </h3>
                                                <p className="desc">
                                                Collect payment from user without
any Website or ERP requirements..
various inbuilt Features for collection
for education institutes and Housing
societies
                                                </p>
                                            </div>
                                            <div className="dv-points">
                                                <h3 className="heading">
                                                Authorized and Capture Flow
                                                </h3>
                                                <p className="desc">
                                                Capture the payments as per
delivery.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
</div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </section>
            </>
        );
    }
}

export default PaymentGatewayFeatures;
