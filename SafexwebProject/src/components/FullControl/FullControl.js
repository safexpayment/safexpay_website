import React from "react";
import "assets/css/safexpay-web.css"
import "./FullControl.css";

class FullControl extends React.Component {

    render() {
        return (
            <>
                <section className="dv-FullControl">
                    <div className="first-bg"></div>
                    <div className="container">

                        <h2 className="section-heading" data-aos="fade-up" data-aos-duration="1000">Full Control Of Users &amp; Their Experience.</h2>
                        <div className="row" data-aos="fade-up" data-aos-duration="1000">
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/full-brand.png")} className="card-img-top" alt="PRomote Brand" />
                                    <div className="card-body">
                                        <p className="card-text">Use our whitelabelled portal &amp; mobile apps with your own branding.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/full-api.png")} className="card-img-top" alt="Api" />
                                    <div className="card-body">
                                        <p className="card-text">Use APIs to build your own native experiences.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/full-manage.png")} className="card-img-top" alt="Management" />
                                    <div className="card-body">
                                        <p className="card-text">Manage agents, distributors &amp; their commissions.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/full-kyc.png")} className="card-img-top" alt="KYC" />
                                    <div className="card-body">
                                        <p className="card-text">Complete KYC approval process in built.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/full-customer.png")} className="card-img-top" alt="Transactions" />
                                    <div className="card-body">
                                        <p className="card-text">Manage customer, card &amp; transaction limits.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/full-report.png")} className="card-img-top" alt="Reports" />
                                    <div className="card-body">
                                        <p className="card-text">Get reports &amp; analytics to improve spends &amp; usage.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>

            </>
        );
    }
}

export default FullControl;
