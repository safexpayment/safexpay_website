import React from "react";

import "assets/css/safexpay-web.css"
import "./Neo-banking.css";

class Neobanking extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Neo-banking mt-0">
                    <div className="container">
                        <h2 className="section-heading pt-5" data-aos="fade-up" data-aos-duration="1000">
                            <span className="color-darkgreen d-block pb-5">NEO BANKING</span>
                            Your Bank, Your Brand , Your Look &amp; Feel, Your Platform! </h2>
                        <p className="text-center">Our ﬁnancial partner network and technology solutions allow enterprises, banks,
and ﬁntech to operate under their own or under 3rd parties with bank Licenses.</p>
                        <div className="dv-img-Neo-Banking">
                        <img
                                alt="Neo Banking"
                                className="img-fluid"
                                src={require("assets/img/section/Neo-Banking.png")}
                            />
                        </div>


                        <h2 className="section-heading pt-5" data-aos="fade-up" data-aos-duration="1000">
                        Functionality And Features</h2>
                        <div className="dv-Neo-features row" data-aos="fade-up" data-aos-duration="1000">
                            <div className="col-4 neo-features">
                            <img
                                alt="Neo Bank App 1"
                                className="img-fluid"
                                src={require("assets/img/section/Neo-Bank-App-01.png")}
                            /></div>
                            <div className="col-4 neo-features">
                            <img
                                alt="Neo Bank App 2"
                                className="img-fluid"
                                src={require("assets/img/section/Neo-Bank-App-02.png")}
                            /></div>
                            <div className="col-4 neo-features">
                            <img
                                alt="Neo Bank App 3"
                                className="img-fluid"
                                src={require("assets/img/section/Neo-Bank-App-03.png")}
                            /></div>
                            <div className="col-4 neo-features">
                            <img
                                alt="Neo Bank App 4"
                                className="img-fluid"
                                src={require("assets/img/section/Neo-Bank-App-04.png")}
                            /></div>
                            <div className="col-4 neo-features">
                            <img
                                alt="Neo Bank App 5"
                                className="img-fluid"
                                src={require("assets/img/section/Neo-Bank-App-05.png")}
                            /></div>
                            <div className="col-4 neo-features">
                            <img
                                alt="Neo Bank App 6"
                                className="img-fluid"
                                src={require("assets/img/section/Neo-Bank-App-06.png")}
                            /></div>
                            <div className="col-4 neo-features">
                            <img
                                alt="Neo Bank App 7"
                                className="img-fluid"
                                src={require("assets/img/section/Neo-Bank-App-07.png")}
                            /></div>
                            <div className="col-4 neo-features">
                            <img
                                alt="Neo Bank App 8"
                                className="img-fluid"
                                src={require("assets/img/section/Neo-Bank-App-08.png")}
                            /></div>
                            <div className="col-4 neo-features">
                            <img
                                alt="Neo Bank App 9"
                                className="img-fluid"
                                src={require("assets/img/section/Neo-Bank-App-09.png")}
                            /></div>
                        </div>
                    </div>
                </section>

            </>
        );
    }
}

export default Neobanking;
