import React from "react";
import "assets/css/safexpay-web.css";
import "./Platform.css";
import Contactuspage from "components/Contactuspagepay/Contactuspagepay";

class Platform extends React.Component {

    state={
        open:false
    }

    showcontactuspage =(props)=>{
        console.log("button click.....");
        this.setState({
            open:!this.state.open
        })

    }

    closepage=()=>{
        this.setState({
            open:!this.state.open
        })
    }

    render() {
        return (
            <>
                <Contactuspage open={this.state.open} click={this.closepage}/>
                <section className="dv-Platform" id="Platform">
                    <div className="first-bg"></div>
                    <div className="second-bg"></div>
                    <div className="container">
                        <h2 className="section-heading" data-aos="fade-up" data-aos-duration="1000">White Label Our Platform To Sell To Other Businesses.</h2>
                        <div className="rounded-border-box">
                            <div className="row">
                                <div className="col-md-6 col-12 dv-payment-gateway" data-aos="fade-up" data-aos-duration="1000">
                                    <div className="card text-center">
                                        <img src={require("assets/img/icons/payment-gateway.png")} className="card-img-top" alt="Payment Gateway" />
                                        <div className="card-body">
                                            <h5 className="card-title">Setup Your Own<br />Payment Gateway!</h5>
                                            <p className="card-text">Install on a private cloud or on our public cloud infrastructure. We manage the entire deployment and upgrade regularly with newer features</p>
                                        </div>
                                    </div>
                                    <div className="dv-points payment-gateway-points">
                                        <ul className="ul-points ul-payment-gateway-points">
                                            <li className="items"><p>Everything that you need to start an online payments business.</p></li>
                                            <li className="items pt-hgt"><p>Fully customised  for your brand.</p></li>
                                            <li className="items"><p>Supporting payment aggregators across the globe!</p>
                                            <button className="btn btn-blue-rounded" onClick={this.showcontactuspage}>TALK TO US</button></li>
                                        </ul>
                                    </div>

                                </div>
                                <div className="col-md-6 col-12 dv-payout-solutions" data-aos="fade-up" data-aos-duration="1000">
                                    <div className="card text-center">
                                        <img src={require("assets/img/icons/payout-solutions.png")} className="card-img-top" alt="Payment Solutions" />
                                        <div className="card-body">
                                            <h5 className="card-title">Create Your Own<br />Payout Solutions</h5>
                                            <p className="card-text">Build solutions for moving money instantly. Get a full toolkit with our APIs or directly reusing our dashboards to create these experiences.</p>
                                        </div>
                                    </div>

                                    <div className="dv-points payout-solutions-points">
                                        <ul className="ul-points ul-payout-solutions-points">
                                            <li className="items"><p>Get started in minutes with our set of incredible features.</p></li>
                                            <li className="items"><p>Design your solution for any industry.</p></li>
                                            <li className="items"><p>Keep your brand ﬂag ﬂying high!</p>
                                            <button className="btn btn-blue-rounded" onClick={this.showcontactuspage}>TALK TO US</button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </>
        );
    }
}

export default Platform;
