import React,{ Component } from 'react';
import "./Supportus.css"
import Spheader from '../Spheader/Spheader';
import Spform from '../Spform/Spform';


class Supportus extends Component{
    render(){
        return(
            <div className="supportus">
            <Spheader/>
            <Spform/>
            </div>
        );
    }
}

export default Supportus;
