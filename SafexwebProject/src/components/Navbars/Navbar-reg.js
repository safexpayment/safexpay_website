import React, { Component } from "react";
import "./Navbar.css";
// reactstrap components
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from "reactstrap";

class TopNavbar extends Component {

    constructor(props) {
      super(props);
      this.toggle = this.toggleNavbar.bind(this);
      
      this.state = {
        isOpen: false
      };
  
      if (typeof window !== 'undefined') {
        window.onscroll = function () {
          let pageScrollpos = window.pageYOffset;
          if (pageScrollpos > 100) {
            document.getElementById('header-global').classList.add("fixed-white-bg");
          } else {
            const el = document.getElementById('header-global');
            if (el.classList.contains("fixed-white-bg")) {
              el.classList.remove("fixed-white-bg");
            }
          }
        };
      }
    }
  
    state = {
      isOpen: false,
    };
  
    toggleNavbar() {
      this.setState({
        isOpen: !this.state.isOpen,
      });
    }
  
    /*freshChatClick() {
        console.log("event trigger");
    }*/
  

  render() {
    return (
      <>
        <header className="header-global" id="header-global">
          <div className="container px-0">
            <Navbar id="navbar" className="topNav px-0" expand="md">
              <NavbarBrand href="/">
                <img
                  id="logo-white"
                  alt="Safexpay"
                  src={require("assets/img/logos/logo_wh.png")}
                  className="white-logo"
                />
                <img
                  id="logo-color"
                  alt="Safexpay"
                  src={require("assets/img/logos/logo.png")}
                  className="color-logo"
                />
              </NavbarBrand>
              <NavbarToggler className="mr-2" onClick={() => this.setState({ isOpen: true })} />
              <Collapse isOpen={this.state.isOpen} navbar>
              <div className="nav-dropdown ml-md-auto">
              <NavbarToggler className="mr-2" onClick={() => this.setState({ isOpen: false })} />
                                <Nav className="ml-auto" navbar>

                  <NavItem className="profile-nav">
                    <NavLink href="/myprofile">View Profile</NavLink>
                  </NavItem>
                  
                  <NavItem className="login-nav">
                    <NavLink href="/login" className="btn-support btn-Login">Login</NavLink>
                  </NavItem>
                  <NavItem className="logout-nav">
                    <NavLink href="/login" className="btn-support btn-Logout">Logout</NavLink>
                  </NavItem>
                </Nav>
                </div>
              </Collapse>
            </Navbar>
          </div>
        </header>
      </>
    );
  }
}

export default TopNavbar;
