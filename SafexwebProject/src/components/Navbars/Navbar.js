import React, { Component } from "react";
import "./Navbar.css";
import Contact from "components/ContactLogo/ContactLogo";
// reactstrap components
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu
} from "reactstrap";
import ContactUs from "components/ContactUS/ContactUs";

class TopNavbar extends Component {

  constructor(props) {
    super(props);
    this.toggle = this.toggleNavbar.bind(this);

    this.state = {
      isOpen: false,
      open: false
    };

    if (typeof window !== 'undefined') {
      window.onscroll = function () {
        let pageScrollpos = window.pageYOffset;
        if (pageScrollpos > 85) {
          document.getElementById('header-global').classList.add("fixed-white-bg");
        } 
        else {
          //const el = document.getElementById('header-global');
          /*if (el.classList.contains("fixed-white-bg")) {
            el.classList.remove("fixed-white-bg");
          }*/
        }
      }
    }
  }

  state = {
    isOpen: false,
  };

  toggleNavbar() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  toggleContactUs = () => {
    this.setState({
      open: !this.state.open
    });
  }

  openContactUs = () => {
    this.setState({
      open: true
    });
  }

  /*freshChatClick() {
      console.log("event trigger");
  }*/

  render() {
    return (
      <>
        <header className="header-global" id="header-global">
          <div className="container px-0">
            <Navbar id="navbar" className="topNav px-0" expand="lg">
              <NavbarBrand href="/">
                <img
                  id="logo-white"
                  alt="Safexpay"
                  src={require("assets/img/logos/logo_wh.png")}
                  className="white-logo"
                />
                <img
                  id="logo-color"
                  alt="Safexpay"
                  src={require("assets/img/logos/logo.png")}
                  className="color-logo"
                />
              </NavbarBrand>
              <NavbarToggler className="mr-2" onClick={() => this.setState({ isOpen: true })} />
              <Collapse isOpen={this.state.isOpen} navbar>
                <div className="nav-dropdown ml-lg-auto">
                  <NavbarToggler className="mr-2" onClick={() => this.setState({ isOpen: false })} />
                  <Nav className="ml-auto" navbar>
                    <UncontrolledDropdown className="dv-product-nav">
                      <DropdownToggle nav caret>
                        Product
                    </DropdownToggle>
                      <DropdownMenu right className="dv-product-nav-child">
                        <div className="">
                          <div className="dv-menu">
                            <div className="menu-heding">
                              <img
                                alt=""
                                src={require("assets/img/icons/business.png")}
                                className="img-menu sp-img"
                              />
                            </div>
                            <div className="menu-items">
                              <h5 className="menu-heading">For Business</h5>
                              <NavLink href="/business/accept-payments">
                                Accept Payments
                    </NavLink>
                              <NavLink href="/business/make-payouts">
                                Make Payouts
                    </NavLink>
                              <NavLink href="/business/digital-banking">
                                Neo Bank
                </NavLink>
                            </div>
                          </div>

                          <div className="dv-menu dv-whitelabel">
                            <div className="menu-heding">
                              <img
                                alt=""
                                src={require("assets/img/icons/white-label.png")}
                                className="img-menu ng-mglt sp-img"
                              />
                            </div>
                            <div className="menu-items">
                              <h5 className="menu-heading">White Label Platform</h5>
                              <NavLink href="/white-label/payment-gateway">
                                Payment Gateway
                    </NavLink>
                              <NavLink href="/business/digital-banking#Platform">
                                Payouts
                    </NavLink>
                            </div>
                          </div>

                          <div className="dv-menu">
                            <div className="menu-heding ng-mglt">
                              <img
                                alt=""
                                src={require("assets/img/icons/partner.png")}
                                className="img-menu sp-img"
                              />
                            </div>
                            <div className="menu-items">
                              <h5 className="menu-heading">Partner Program</h5>
                              <NavLink href="/partner/reseller">
                                Reseller
                    </NavLink>
                            </div>
                          </div>
                        </div>
                      </DropdownMenu>
                    </UncontrolledDropdown>

                    <NavItem>
                      <NavLink href="/pricing">Pricing</NavLink>
                    </NavItem>
                    <UncontrolledDropdown className="dv-company-nav">
                      <DropdownToggle nav caret>
                        Company
                  </DropdownToggle>
                      <DropdownMenu right className="dv-company-nav-child">
                        <div className="d-md-flex">
                          <div className="dv-menu">
                            <div className="menu-heding">
                              <img
                                alt=""
                                src={require("assets/img/icons/company.png")}
                                className="img-menu sp-img"
                              />
                            </div>
                            <div className="menu-items">
                              <NavLink href="/company/about-us">
                                About Us
                              </NavLink>
                              <NavLink href="/career">
                                Career
                              </NavLink>
                              <NavLink href="/company/privacy-policy">
                                Privacy Policy
                              </NavLink>
                              <NavLink href="/company/terms-conditions">
                                Terms &amp; Conditions
                              </NavLink>
                            </div>
                          </div></div>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                    {/* <NavItem className="login-nav">
                    <NavLink href="/register" className="btn-Login">Login</NavLink>
                  </NavItem> */}
                  <NavItem className="support-nav">
                    <NavLink href="/supportus" className="btn-support">Support</NavLink>
                  </NavItem>
                  </Nav>
                </div>
              </Collapse>
            </Navbar>
          </div>
        </header>
        <Contact click={this.toggleContactUs} flag={this.state.open} />
        <ContactUs open={this.state.open} click={this.toggleContactUs} />
      </>
    );
  }
}

export default TopNavbar;
