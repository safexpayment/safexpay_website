import React from "react";
import { Accordion, Button } from 'react-bootstrap';

import "assets/css/safexpay-web.css"
import "./Launcher.css";
import Contactuspage from 'components/Contactuspagenew/Contactuspagenew';

class Launcher extends React.Component {
    state={
        open:false
    }

    showcontactuspage =(props)=>{
        console.log("button click.....");
        this.setState({
            open:!this.state.open
        })

    }

    closepage=()=>{
        this.setState({
            open:!this.state.open
        })
    }

    render() {
        return (
            <>
                <Contactuspage open={this.state.open} click={this.closepage}/>
                <section className="dv-Launcher">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 offset-md-1 col-12">
                                <h2 className="section-heading mt-4 text-center text-md-left text-white">New Product Launch</h2>

                                <div className="accordion acc-FullyCustomised">
                                    <Accordion defaultActiveKey="0">
                                        <div
                                            className="acc-launcher"

                                        >
                                            <Accordion.Toggle as={Button} variant="link" htmlFor="FlexiQR" eventKey="0" className="heading opened"
                                                onClick={() => this.setState({ imageURL: 'assets/img/icons/one.png' })}>
                                                <strong>FlexiQR, </strong> the most flexible QR solution!
                                </Accordion.Toggle>
                                            <Accordion.Collapse eventKey="0">
                                                <div className="description">
                                                    No need for your customers to install any app, just scan using the phone camera and pay with 100+ payment modes. Create a QR on the fly with our APIs or generate in bulk to display in physical stores.
                                                    <button className="btn btn-white-rounded" onClick={this.showcontactuspage}>TALK TO US &#10230;</button>
                                                </div>
                                            </Accordion.Collapse>
                                        </div>

                                        <div className="acc-launcher"
                                            onClick={this.email_Image}
                                        >
                                            <Accordion.Toggle as={Button} variant="link" htmlFor="UPI Intent Flow" eventKey="1" className="heading">
                                                <strong>UPI Intent Flow, </strong> UPI payment made easy!
                                </Accordion.Toggle>
                                            <Accordion.Collapse eventKey="1">
                                                <div className="description">
                                                Accept payments from mobile users via UPI payment apps on without any hassles of handling SMS notifications or remembering UPI ID. Just use the preferred app eg. PhonePe, BHIM &amp; accept payments.
                                                <button className="btn btn-white-rounded" onClick={this.showcontactuspage}>TALK TO US &#10230;</button>
                                                </div>
                                            </Accordion.Collapse>
                                        </div>
                                        <div className="acc-launcher">
                                            <Accordion.Toggle as={Button} variant="link" htmlFor="UPI Autopay" eventKey="2" className="heading"
                                                onClick={this.checkout_Image}>
                                                <strong>UPI Autopay, </strong> Automate your collection.
                                </Accordion.Toggle>
                                            <Accordion.Collapse eventKey="2">
                                                <div className="description">
                                                    Customers can now subscribe for recurring payments using just their UPI App. Sign up today to give your customers a genx experience.
                                                <button className="btn btn-white-rounded" onClick={this.showcontactuspage}>TALK TO US &#10230;</button>
                                                </div>
                                            </Accordion.Collapse>
                                        </div>

                                        <div className="acc-launcher">
                                            <Accordion.Toggle as={Button} variant="link" htmlFor="Digital Onboarding" eventKey="3" className="heading"
                                                onClick={this.checkout_Image}>
                                                <strong>Digital Onboarding, </strong> Paperless &amp; Secure registration
                                </Accordion.Toggle>
                                            <Accordion.Collapse eventKey="3">
                                                <div className="description">
                                                    Get registered and onboard in mminutes with the new state of the art safe &amp; secure Digital Onboarding process. Simply verify your documents and go live.
                                                    <button className="btn btn-white-rounded" onClick={this.showcontactuspage}>TALK TO US &#10230;</button>
                                                </div>
                                            </Accordion.Collapse>
                                        </div>

                                    </Accordion>
                                </div>
                            </div>
                            <div className="col-md-5 col-12 pr-md-0 resp-none">
                                <div className="dv-img-show">
                                    <div className="img-gif">

                                    </div>
                                    <div className="img-flexiqr">
                                        <img
                                            alt="Flexi QR"
                                            className="img-fluid"
                                            src={require("assets/img/section/flexiqr.png")}
                                        />
                                    </div>
                                    <div className="img-upiintentflow d-none">
                                        <img
                                            alt="UPI Intent Flow"
                                            className="img-fluid"
                                            src={require("assets/img/section/upiintentflow.png")}
                                        />
                                    </div>
                                    <div className="img-upiautopay d-none">
                                        <img
                                            alt="UPI Autopay"
                                            className="img-fluid"
                                            src={require("assets/img/section/upiautopay.png")}
                                        />
                                    </div>
                                    <div className="img-digitalonboarding d-none">
                                        <img
                                            alt="Digital On Boarding"
                                            className="img-fluid"
                                            src={require("assets/img/section/digitalonboarding.png")}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </section>

            </>
        );
    }
}

export default Launcher;
