import React from "react";
import {
    Modal
} from "reactstrap";
import "assets/css/safexpay-web.css"
import "./Registration.css";

class BusinessDoc extends React.Component {
    state = {};
    toggleModal = state => {
        this.setState({
            [state]: !this.state[state]
        });
    };

    render() {

        return (
            <>
                <section className="dv-Registration-content doc-main-div">
                    <div className="first-bg htg"></div>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 dv-instruction" data-aos="fade-right" data-aos-duration="1000">
                                <h1 className="section-heading">Quick &amp; Easy Activation Process</h1>
                                <div className="row">
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step completed">
                                            <div className="step">Step 1</div>
                                            <div className="text">
                                                Check email for Username &amp; OTP.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step completed">
                                            <div className="step">Step 2</div>
                                            <div className="text">
                                                Enter Username &amp; OTP. <br />
                                                Set a desirable password.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step">
                                            <div className="step">Step 3</div>
                                            <div className="text">
                                                Fill in all the details &amp; upload documents to start activation process.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul className="tick mt-5 mr-5">
                                    <li className="completed">Business Overview <span className="hr-white"></span> </li>
                                    {/* <li className="completed">Business Contact Details <span className="hr-white"></span></li> */}
                                    <li className="completed">Bank Account Details <span className="hr-white"></span></li>
                                    <li>Agreement &amp; KYC Documents</li>
                                </ul>
                            </div>
                            <div className="col-md-6 form-div" data-aos="fade-up" data-aos-duration="1000">
                                <div className="dv-login-form dv-form">
                                    <h1 className="section-heading border-heading">{this.props.name}</h1>
                                    <div className="dv-indicator">
                                        <ul className="indicator">
                                            <li className="box completed"></li>
                                            <li className="line"></li>
                                            <li className="box"></li>
                                            <li className="line"></li>
                                            <li className="box"></li>
                                        </ul>
                                    </div>
                                    <h4 className="text-center doc-txt">Business Documents</h4>

                                    <div className="dv-doc dv-ind db-block">
                                        <div className="dv-particular">
                                            <label className="lbl">GST Certificate</label>
                                            <span className="note">GST registration compulsory above 40 lakhs</span>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModal")}>UPLOAD</button>
                                        </div>
                                        <div className="clearfix"></div>
                                        <div className="custom-control custom-radio custom-control-inline my-3">
                                            <input type="radio" id="txtgstno1" name="txtgstno" className="custom-control-input" defaultChecked />
                                                <label className="custom-control-label" htmlFor="txtgstno1">Yes</label>
</div>
                                            <div className="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="txtgstno2" name="txtgstno" className="custom-control-input" />
                                                    <label className="custom-control-label" htmlFor="txtgstno2">No</label>
</div>
                                        <input type="text" className="form-control" id="txtgstno" placeholder="Enter GSTIN number" />
                                        </div>
                                    
                                            
                                    
                                    <div className="dv-doc dv-ind">
                                        <div className="dv-particular">
                                            <label className="lbl">Company Pancard<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue">UPLOAD</button>
                                        </div>
                                    </div>

                                    <div className="dv-doc dv-ind">
                                        <div className="dv-particular">
                                            <label className="lbl">Udyog Aadhaar<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModalAadhaar")}>UPLOAD</button>
                                        </div>
                                    </div>

                                    {/* <div className="dv-doc dv-ind">
                                        <div className="dv-particular">
                                            <label className="lbl">Cancelled cheque </label>
                                            <span className="note">Uplaod the cheque of the registed bank</span>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-green">VERIFIED</button>
                                        </div>
                                    </div> */}
                                    
                                    {/* <div className="dv-doc dv-prop">
                                        <div className="dv-particular">
                                            <label className="lbl">Cancelled cheque </label>
                                            <span className="note">Uplaod the cheque of the registed bank</span>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue">UPLOAD</button>
                                        </div>
                                    </div> */}
                                    <div className="dv-doc dv-prop db-block">
                                        <div className="dv-particular">
                                            <label className="lbl">GST Certificate</label>
                                            <span className="note">GST registration compulsory above 40 lakhs</span>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModal")}>UPLOAD</button>
                                        </div>
                                        <div className="clearfix"></div>
                                        <div className="custom-control custom-radio custom-control-inline my-3">
                                            <input type="radio" id="txtgstno1" name="txtgstno" className="custom-control-input" defaultChecked />
                                                <label className="custom-control-label" htmlFor="txtgstno1">Yes</label>
</div>
                                            <div className="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="txtgstno2" name="txtgstno" className="custom-control-input" />
                                                    <label className="custom-control-label" htmlFor="txtgstno2">No</label>
</div>
                                        <input type="text" className="form-control" id="txtgstno" placeholder="Enter GSTIN number" />
                                        </div>
                                    
                                            
                                    
                                    <div className="dv-doc dv-prop">
                                        <div className="dv-particular">
                                            <label className="lbl">Company Pancard<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue">UPLOAD</button>
                                        </div>
                                    </div>

                                    <div className="dv-doc dv-prop">
                                        <div className="dv-particular">
                                            <label className="lbl">Udyog Aadhaar<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModalAadhaar")}>UPLOAD</button>
                                        </div>
                                    </div>

                                    
                                    {/* <div className="dv-doc dv-part">
                                        <div className="dv-particular">
                                            <label className="lbl">Cancelled Cheque </label>
                                            <span className="note">Uplaod the cheque of the registed bank</span>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue">UPLOAD</button>
                                        </div>
                                    </div> */}
                                    <div className="dv-doc dv-part db-block">
                                        <div className="dv-particular">
                                            <label className="lbl">GST Certificate</label>
                                            <span className="note">GST registration compulsory above 40 lakhs</span>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModal")} disabled>UPLOAD</button>
                                        </div>
                                        <div className="clearfix"></div>
                                        <div className="custom-control custom-radio custom-control-inline my-3">
                                            <input type="radio" id="gstno1" name="gstno" className="custom-control-input" />
                                                <label className="custom-control-label" htmlFor="gstno1">Yes</label>
</div>
                                            <div className="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="gstno2" name="gstno" className="custom-control-input" />
                                                    <label className="custom-control-label" htmlFor="gstno2">No</label>
</div>
                                        {/* <input type="text" className="form-control" id="txtgstno" placeholder="Enter GSTIN number" /> */}
                                        </div>
                                    <div className="dv-doc dv-part">
                                        <div className="dv-particular">
                                            <label className="lbl">Company PAN Card<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModalPan")}>UPLOAD</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-part">
                                        <div className="dv-particular">
                                            <label className="lbl">Paternership Deed<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModal")}>UPLOAD</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-part">
                                        <div className="dv-particular">
                                            <label className="lbl">Corporate Identification number (CIN)<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModal")}>UPLOAD</button>
                                        </div>
                                    </div>

                                    
                                   
                                    <div className="dv-doc dv-pub db-block">
                                        <div className="dv-particular">
                                            <label className="lbl">GST Certificate</label>
                                            <span className="note">GST registration compulsory above 40 lakhs</span>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModal")}>UPLOAD</button>
                                        </div>
                                        <div className="clearfix"></div>
                                        <div className="custom-control custom-radio custom-control-inline my-3">
                                            <input type="radio" id="txtgstno1" name="txtgstno" className="custom-control-input" defaultChecked />
                                                <label className="custom-control-label" htmlFor="txtgstno1">Yes</label>
</div>
                                            <div className="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="txtgstno2" name="txtgstno" className="custom-control-input" />
                                                    <label className="custom-control-label" htmlFor="txtgstno2">No</label>
</div>
                                        <input type="text" className="form-control" id="txtgstno" placeholder="Enter GSTIN number" />
                                    </div>
                                    <div className="dv-doc dv-pub">
                                    <div className="dv-particular">
                                            <label className="lbl">Company Pancard<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue">UPLOAD</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-pub">
                                        <div className="dv-particular">
                                            <label className="lbl">Board Resolution<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue">UPLOAD</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-pub">
                                        <div className="dv-particular">
                                            <label className="lbl">Corporate Identification number (CIN)<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModal")}>UPLOAD</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-pub">
                                        <div className="dv-particular">
                                            <label className="lbl">DIN<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModalPdf")}>UPLOAD</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-pub">
                                        <div className="dv-particular">
                                            <label className="lbl">Memorandum of Association<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModalPdf")}>UPLOAD</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-pub">
                                        <div className="dv-particular">
                                            <label className="lbl">Article of Associations<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModalPdf")}>UPLOAD</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-pub">
                                        <div className="dv-particular">
                                            <label className="lbl">Udyog Aadhaar<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModalAadhaar")}>UPLOAD</button>
                                        </div>
                                    </div>

                                    <div className="dv-doc dv-soc db-block">
                                        <div className="dv-particular">
                                            <label className="lbl">GST Certificate</label>
                                            <span className="note">GST registration compulsory above 40 lakhs</span>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModal")}>UPLOAD</button>
                                        </div>
                                        <div className="clearfix"></div>
                                        <div className="custom-control custom-radio custom-control-inline my-3">
                                            <input type="radio" id="txtgstno1" name="txtgstno" className="custom-control-input" defaultChecked />
                                                <label className="custom-control-label" htmlFor="txtgstno1">Yes</label>
                                        </div>
                                            <div className="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="txtgstno2" name="txtgstno" className="custom-control-input" />
                                                    <label className="custom-control-label" htmlFor="txtgstno2">No</label>
                                            </div>
                                        <input type="text" className="form-control" id="txtgstno" placeholder="Enter GSTIN number" />
                                    </div>
                                    <div className="dv-doc dv-soc">
                                        <div className="dv-particular">
                                            <label className="lbl">Company PAN Card<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModalPan")}>UPLOAD</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-soc">
                                        <div className="dv-particular">
                                            <label className="lbl">Trust Act Certificate<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue"  onClick={() => this.toggleModal("uploadModalPdf")}>UPLOAD</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-soc">
                                        <div className="dv-particular">
                                            <label className="lbl">Authorization Letter Or Board Resolution<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue">UPLOAD</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-soc">
                                        <div className="dv-particular">
                                            <label className="lbl">Trust Deed<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue">UPLOAD</button>
                                        </div>
                                    </div>
                                    
                                    {/* <div className="my-5 py-5"></div> */}

                                    <div className="btn-holder mt-5 text-center">
                                        <a href="/agreement" className="btn btn-next btn-gradient">
                                            Next
                                        </a>
                                    </div>

                                    <div className="dv-bottom-bg mt-5">
                                        <div className="dv-help">
                                            Need help? <a href="/" className="lnk-contact">Contact Us</a>
                                        </div>
                                        <div className="first-bottom-bg"></div>
                                        <div className="second-bottom-bg"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <Modal
                    className="modal-dialog-centered modal-lg uploadModal"
                    isOpen={this.state.uploadModal}
                    toggle={() => this.toggleModal("uploadModal")}
                >
                    <div className="modal-body p-4">
                        <button
                            aria-label="Close"
                            className="close"
                            data-dismiss="modal"
                            type="button"
                            onClick={() => this.toggleModal("uploadModal")}
                        >
                            <span aria-hidden={true}>×</span>
                        </button>
                        <div className="dv-upload">
                            <div className="dv-upload-header">
                                <h5>Upload Image</h5>
                            </div>
                            <div className="dv-upload-img">
                                <p>Upload Image</p>
                            </div>
                            <div className="btn-holder mt-3 text-center">
                                <button  className="btn btn-gradient bt-wd">
                                   Sumbit
                                </button>
                            </div>
                            {/* <div className="dv-upload-progress">
                                <p>Uploaded image is getting veriﬁed , Please wait.</p>
                                <div className="progress">
                                    <div className="progress-bar progress-bar-striped bg-success" role="progressbar" style={{width: "80%"}} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div> */}
                        </div>
                    </div>
                </Modal>
                <Modal
                    className="modal-dialog-centered modal-lg uploadModalPan"
                    isOpen={this.state.uploadModalPan}
                    toggle={() => this.toggleModal("uploadModalPan")}
                >
                    <div className="modal-body p-4">
                        <button
                            aria-label="Close"
                            className="close"
                            data-dismiss="modal"
                            type="button"
                            onClick={() => this.toggleModal("uploadModalPan")}
                        >
                            <span aria-hidden={true}>×</span>
                        </button>
                        <div className="dv-upload">
                            <div className="dv-upload-header">
                                <h5>Upload Your PAN Card Copy</h5>
                                <p>Please upload front side</p>
                            </div>
                            <div className="dv-upload-img">
                                    <p>Upload Image</p>
                            </div>
                            <div className="filledDet">
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="txtname">Name</label>
                                            <input type="text" className="form-control" id="txtname" placeholder="" disabled />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="ftname">Father Name</label>
                                            <input type="text" className="form-control" id="ftname" placeholder="" disabled />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="txtno">PAN No.</label>
                                            <input type="text" className="form-control" id="txtno" placeholder="" disabled />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="txtdb">Date of Birth</label>
                                            <input type="text" className="form-control" id="txtdb" placeholder="" disabled />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="btn-holder mt-3 text-center">
                                <button  className="btn btn-gradient bt-wd">
                                   Sumbit
                                </button>
                            </div>
                            {/* <div className="dv-upload-progress">
                                <p>Uploaded image is getting veriﬁed , Please wait.</p>
                                <div className="progress">
                                    <div className="progress-bar progress-bar-striped bg-success" role="progressbar" style={{width: "80%"}} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div> */}
                        </div>
                    </div>
                </Modal>

                <Modal
                    className="modal-dialog-centered modal-lg uploadModalAadhaar"
                    isOpen={this.state.uploadModalAadhaar}
                    toggle={() => this.toggleModal("uploadModalAadhaar")}
                >
                    <div className="modal-body p-4">
                        <button
                            aria-label="Close"
                            className="close"
                            data-dismiss="modal"
                            type="button"
                            onClick={() => this.toggleModal("uploadModalAadhaar")}
                        >
                            <span aria-hidden={true}>×</span>
                        </button>
                        <div className="dv-upload">
                            <div className="dv-upload-header">
                                <h5>Upload Your AADHAAR Card Copy</h5>
                                <p>Please upload front &amp; back side</p>
                            </div>
                            <div className="row ad-upld">
                                <div className="col-md-6">
                                    <div className="dv-upload-img">
                                        <p>Upload Image</p>
                                    </div>
                                    <p className="text-center mod-txt">Front Side</p>
                                </div>
                                <div className="col-md-6">
                                    <div className="dv-upload-img">
                                        <p>Upload Image</p>
                                    </div>
                                    <p className="text-center mod-txt">Back Side</p>
                                </div>
                            </div>
                            <div className="filledDet">
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="txtname">Name</label>
                                            <input type="text" className="form-control" id="txtname" placeholder="" disabled />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="txtno">Aadhaar Card No.</label>
                                            <input type="text" className="form-control" id="txtno" placeholder="" disabled />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="ftname">Address</label>
                                            <textarea className="form-control addtxtarea" name="" disabled></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="btn-holder mt-3 text-center">
                                <button  className="btn btn-gradient bt-wd">
                                   Sumbit
                                </button>
                            </div>
                            {/* <div className="dv-upload-progress">
                                <p>Uploaded image is getting veriﬁed , Please wait.</p>
                                <div className="progress">
                                    <div className="progress-bar progress-bar-striped bg-success" role="progressbar" style={{width: "80%"}} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div> */}
                        </div>
                    </div>
                </Modal>
                <Modal
                    className="modal-dialog-centered modal-lg uploadModalPdf"
                    isOpen={this.state.uploadModalPdf}
                    toggle={() => this.toggleModal("uploadModalPdf")}
                >
                    <div className="modal-body p-4">
                        <button
                            aria-label="Close"
                            className="close"
                            data-dismiss="modal"
                            type="button"
                            onClick={() => this.toggleModal("uploadModalPdf")}
                        >
                            <span aria-hidden={true}>×</span>
                        </button>
                        <div className="dv-upload">
                            <div className="dv-upload-header">
                                <h5>Upload Document</h5>
                                <p>Upload document in PDF format</p>
                            </div>
                            <div className="dv-upload-img">
                                <p>Upload PDF</p>
                            </div>
                            <div className="btn-holder mt-3 text-center">
                                <button  className="btn btn-gradient bt-wd">
                                   Sumbit
                                </button>
                            </div>
                            {/* <div className="dv-upload-progress">
                                <p>Uploaded image is getting veriﬁed , Please wait.</p>
                                <div className="progress">
                                    <div className="progress-bar progress-bar-striped bg-success" role="progressbar" style={{width: "80%"}} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div> */}
                        </div>
                    </div>
                </Modal>

            </>
        );
    }
}

export default BusinessDoc;
