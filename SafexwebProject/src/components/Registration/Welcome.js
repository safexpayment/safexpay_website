import React from "react";
import "assets/css/safexpay-web.css"
import "./Registration.css";

class Welcome extends React.Component {

    render() {
        return (
            <>
                <table border="0" cellPadding="0" cellSpacing="0" style={{ border: '1px solid #d2d2d2', margin: '0 auto', fontFamily: 'Verdana', fontSize: '16px', lineHeight: '25px' }} width="750px">
                    <tr>
                        <td>
                            <img src={require("assets/img/email/img-01.png")} alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src={require("assets/img/email/img-02.png")} alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#dddedf">
                            <table border="0" cellPadding="0" cellSpacing="0" bgcolor="#ffffff" style={{ margin: '0 auto' }} width="658px">
                                <tr>
                                    <td style={{ paddingLeft: '50px', paddingRight: '50px', fontFamily: 'Verdana', fontSize: '16px', lineHeight: '25px' }}>
                                        <br />
                                        Hi Roshan Gupta,
                                        <br /><br />
                                        Get started with our quick on boarding experience with 3 easy steps.
                                        Login with the below User ID &amp; OTP , enter your credencials &amp;
                                        upload + verify your documents.
                                        <br /><br />

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style={{ background: '#d7d8d9', height: '1px', marginTop: '20px', marginBottom: '20px', marginLeft: '50px', marginRight: '50px' }}>&nbsp;</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{ paddingLeft: '50px', paddingRight: '50px', fontFamily: 'Verdana', fontSize: '16px', lineHeight: '25px' }}>
                                        <br />
                                        <strong>Here is your Login information.</strong><br /><br />
                                        <strong>User ID : UXW123</strong><br />
                                        <strong>OTP : 12550</strong>
                                        <br /><br />

                                        <a href="/" style={{ textDecoration: 'none' }} ><img src={require("assets/img/email/img-03.png")} alt="" /></a>
                                        <br /><br /> </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style={{ background: '#d7d8d9', height: '1px', marginTop: '20px', marginBottom: '10px', marginLeft: '50px', marginRight: '50px' }}>&nbsp;</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{ paddingLeft: '50px', paddingRight: '50px', fontFamily: 'Verdana', fontSize: '16px', lineHeight: '25px' }}>
                                        <br />
                                        Thanks for choosing Safexpay.<br />
- Safexpay Team.
                                        <br /><br />
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#dddedf">
                            <br />
                            <p style={{ textAlign: 'center' }}>
                                <a href="/" style={{ textDecoration: 'none' }} ><img src={require("assets/img/email/tw.png")} alt="" /></a>
                                <a href="/" style={{ textDecoration: 'none' }} ><img src={require("assets/img/email/fb.png")} alt="" /></a>
                                <a href="/" style={{ textDecoration: 'none' }} ><img src={require("assets/img/email/ins.png")} alt="" /></a>
                                <a href="/" style={{ textDecoration: 'none' }} ><img src={require("assets/img/email/link.png")} alt="" /></a>

                            </p>

                            <p style={{ textAlign: 'center', fontSize: '14px' }}>
                                <strong> Questions?</strong><br />

Reply to this email or click on the given link.
<a href="/">Support.</a>

                            </p>


                        </td>



                    </tr>
                </table>
                <br /><br />
            </>
        );
    }
}

export default Welcome;
