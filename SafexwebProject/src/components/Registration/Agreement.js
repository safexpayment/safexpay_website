import React from "react";
import {
    Modal
} from "reactstrap";
import "assets/css/safexpay-web.css"
import "./Registration.css";

class AgreementDoc extends React.Component {
    state = {};
    toggleModal = state => {
        this.setState({
            [state]: !this.state[state]
        });
    };

    render() {
        return (
            <>
                <section className="dv-Registration-content doc-main-div">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12 form-div1" data-aos="fade-up" data-aos-duration="1000">
                                <div className="dv-login-form dv-form">
                                    <h1 className="section-heading border-heading">Individual</h1>
                                    <div className="dv-indicator">
                                        <ul className="indicator">
                                            <li className="box completed"></li>
                                            <li className="line"></li>
                                            <li className="box completed"></li>
                                            <li className="line pid"></li>
                                            <li className="box pid"></li>
                                        </ul>
                                    </div>
                                    <h4 className="text-center doc-txt">Merchant Service Agreement</h4>

                                    <div className="agreementContent">
                                        <p>This Agreement is entered into by and between  PAYGATE INDIA PVT. LTD. having its oﬃce at 879, Udyog Vihar, Phase-V, Gurgaon, Haryana- 122016 (hereinafter referred to as “Paygate” or
“the Company”); and the Merchant listed in “Company Name ________________________ having its oﬃce at Address_______________________________ ”</p>
                                        <p>THE PARTIES AGREE ON TERMS AND CONDITIONS AS FOLLOWS:</p>
                                        <ol>
                                            <li>1. PURPOSE<br />
The Merchant desires to use payment gateway service for receiving payment of goods and/or services from customers and for its convenience making payment through available channels including but
not limited to credit card, debit card, digital wallet, banking payment,UPI and Other payment Services provided by the Company (“Services”).</li>
                                            <li>2. COMMENCEMENT OF SERVICES<br />
The Merchant agrees to submit an application form correctly and completely ﬁlled in together with supporting documents required by the Company on the date hereof for the Company’s consideration.
The Merchant shall be entitled to use the Services for receiving the payments for goods and/or services of the Merchant only upon obtaining the Company’s permission.
    </li>
                                            <li>3. ACKNOWLEDGEMENT OF RISKS IN ELECTRONIC TRANSACTIONS<br />
The Merchant has carefully considered and realizes that electronic transactions are of risk and accepts any consequent damages incurred from such electronic transactions. The Merchant agrees to strictly
comply with the following:
<ol>
                                                    <li>3.1 In case of receipt of the payments for goods and/or services, the Merchant must keep, not less than eighteen
                                                    (18) months, sales record or proof of trade, delivery of goods/services and receipt of payments such as tax invoice, shipping slip, receipt, etc. as proof in any disputes happen thereafter. If the Company
                                                    desires to verify the Merchant’s transactions, the Merchant agrees to provide the Company with all relevant information and documents immediately or within a speciﬁed period. The Merchant also agrees
                                                    to assist the Company in order to acquire such information and documents and shall not, in any circumstances, obstruct, weaken, destroy or hinder the Company from acquiring such information and
documents.</li>
                                                    <li>3.2 In case of refusal and/or non-receivable payment, card cancellation or hold, or having a reasonable suspicion cause regarding the dishonest use of debit card/credit card/bank account/wallet or
                                                    any other reasons causing the Company’s money to be deducted or compensated or claimed as fees, ﬁnes, penalties or damages by any banks, ﬁnancial institutions, card networks or other persons, the
                                                    Merchant agrees to reimburse to the Company in full amount which and that the Company is entitled to immediately set-oﬀ such amount with the amount of payment for goods and/or services payable to
                                                    the Merchant without any objection or attempt to decline the said responsibility in all respects.
    </li>
                                                </ol>
                                            </li>
                                            <li>4. SERVICE FEE
                                                <ol>
                                                    <li>4.1 The Company is entitled to the service fee for performance of obligations hereunder according to the rate and payment method speciﬁed in the application.
        </li>
                                                    <li>4.2 The Merchant agrees that the Company is entitled to immediately set-oﬀ the amount of payments for goods.
                                                    and/or services to be transferred to the Merchant by the Company with the amount of service fee including expenses or any other fees that the Merchant is required to pay or reimburse to the Company
                                                    hereunder prior to transferring such amount to the Merchant.
        </li>
                                                    <li>4.3 During the period of this Agreement, upon revision from the Banks, ﬁnancial institutions or card networks, the Company reserve the right to amend the rate of service fee by giving at least ﬁfteen
                                                    (15) days advance notice to the Merchant and such notice shall be deemed an integral part of this agreement.
    </li>
                                                </ol>
                                            </li>
                                            <li>5. PAYGATE SERVICE
                                                <ol>
                                                    <li>5.1 The Company shall gather all the payments for goods and/or services on behalf of the Merchant and, after deducting of the service fee according to Clause 4 and any expenses (if any) such as
                                                    credit card fees, etc., remit it to the Merchant’s account within the speciﬁed period and according to the payment method as per the application. The Merchant agrees to be responsible for all fees incurred
by such remittance.</li>
                                                    <li>5.2 In event that funds in the Merchant’s bank account are not suﬃcient, the Company reserves the right to terminate this Agreement and close the Merchant ID immediately.
        </li>
                                                    <li>5.3 The Merchant ID is the user account created by the Company for the Merchant to use Services.
        </li>
                                                    <li>5.4 Unless otherwise provided herein, the Company reserves the right to open only one Merchant ID for the Merchant, if it appears that the Merchant has or obtains more than one Merchant ID, the
                                                    Company is entitled to cancel other Merchant ID(s) and retain only one of them for the Merchant.
    </li>
                                                </ol>
                                            </li>
                                            <li>6. PAYGATE SERVICE
                                                <ol>
                                                    <li>6.1 The Merchant has a duty to deliver information of goods and/or services according to form and method as stipulated by the Company immediately when customers make an order of goods and/or
services.</li>
                                                    <li>6.2 The Merchant represents and warrants that any information delivered to the Company, under Clause 6.1 of this Agreement, is correct. The Company shall not be responsible for verifying such
information.</li>
                                                    <li>6.3 In case having received such monies from customers and it is found that the paid amount is incorrect as a result of information in Clause 6.1 of this Agreement, the Company shall not be responsible
                                                    for such error. The Company shall not return such monies to
                                                    customers, the Merchant shall be solely responsible for such error
to its customers.</li>
                                                    <li>6.4 The Merchant acknowledges that as a risk management
                                                    tool, the Company and/or the Acquiring Banks reserve the right
                                                    to limit or restrict transaction size, amount and/or monthly volume
                                                    or
                                                    suspend
                                                    the Service at any time. Further, as a security measure,
                                                    the Company may at its sole discretion block any card number,
                                                    account numbers, group of cards or transactions from any speciﬁc
                                                    blocked
                                                    or
                                                    blacklisted customer cards, accounts, speciﬁc, group of IP addresses,
                                                    devices, geographic locations and / or any such risk mitigation
                                                    measures it wishes to undertake.
</li>
                                                    <li>6.5 The Merchant also acknowledges that the arrangement
                                                    between one or more Acquiring Banks and Paygate may terminate at
any time and services by such Acquiring Banks may be withdrawn.</li>
                                                </ol>
                                            </li>
                                        </ol>


                                        <p>
                                            <strong>
                                                PAYMENT SETTLEMENT PERIOD</strong><br />
Nodal Bank (T*+3)**<br />
*T – Transaction Day<br />
** Settlement timeline shall be subject to the regulatory guidelines and receipt of clear funds from the Banks.</p>
                                        <p><strong>
                                            TERM OF THE CONTRACT</strong><br />
Term: Three (3) Years and stands renewed automatically for the same period unless terminated Minimum Term: One (1) Year.</p>
                                        <p><strong>
                                            FEES AND CHARGE</strong><br />
Merchant Name:<br />
Merchant Website:<br />
Merchant Category(Pvt/Ltd/LLP/Single Owner):
</p>



                                        <div className="dv-bestSeller row mx-0" data-aos="fade-down" data-aos-duration="1000" id="dv-bestSeller">
                                            <div className="col-lg-5">
                                                <div className="dv-percentage">
                                                    <label>1.65%<sup>*</sup></label>
                                                </div>
                                            </div>
                                            <div className="col-lg-7">
                                                <h2 className="section-heading">Domestic Payments</h2>
                                                <ul className="tick">
                                                    <li>
                                                        <strong>Net Banking</strong>
                                                        <span>SBI<sup>+</sup>, HDFC<sup>+</sup>, ICICI<sup>+</sup>, Axis<sup>+</sup>, Kotak &amp; 50 more.</span>
                                                    </li>
                                                    <li><strong>Indian Debit &amp; Credit Card</strong></li>
                                                    <li>
                                                        <strong>UPI</strong>
                                                        <span>Google Pay, PhonePe, Paytm, BHIM &amp; other UPI apps</span>
                                                    </li>
                                                    <li>
                                                        <strong>Wallets</strong>
                                                        <span>Amazon Pay<sup>+</sup>, Paytm, Freecharge, Mobikwik, etc</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div className="col-12 dv-note text-right">
                                                (*) Pricing per transaction &amp; GST additional. | (+) Subject to approval from banking partners.
                                    </div>
                                            <div className="col-12 dv-facility pl-md-5">
                                                <ul className="ul-facility row mx-0">
                                                    <li className="col-4 py-3">No Setup fees</li>
                                                    <li className="col-4 py-3">No Annual Maintainance fees</li>
                                                    <li className="col-4 py-3">No fees for transfer</li>
                                                    <li className="col-4 py-3">No fees for failed payments</li>
                                                    <li className="col-4 py-3">No fees for refunds</li>
                                                    <li className="col-4 py-3">No fees for support</li>
                                                </ul>
                                            </div>
                                        </div>



                                        <p>
                                            IN WITNESS WHEREOF this Agreement is made in two (2) copies. The parties have thoroughly read and comprehended the contents hereof and found that these correctly meet their intention. The parties
                                            have consequently executed and aﬃxed their company seal in the presence of witness and kept on copy each.
</p>

                                        <div className="form-group form-check mx-0 px-0">
                                            <input type="checkbox" className="form-check-input cont-chk-inpt ml-0" id="chkAgreeTC" />
                                            <label className="form-check-label cont-chk ml-4 pl-2" htmlFor="chkAgreeTC">I acknowledge that I have read, and do hereby accept the terms and conditions contained in this Agreement.</label>
                                        </div>



                                        <div className="btn-holder mt-5 text-center">
                                            <a href="/success" className="btn btn-next btn-gradient">
                                                Submit
                                            </a>
                                            <a href="/success" className="btn btn-next btn-gradient">
                                                Next
                                            </a>
                                        </div>
                                    </div>

                                    <div className="dv-bottom-bg mt-5">
                                        <div className="dv-help">
                                            Need help? <a href="/" className="lnk-contact">Contact Us</a>
                                        </div>
                                        <div className="first-bottom-bg"></div>
                                        <div className="second-bottom-bg"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <Modal
                    className="modal-dialog-centered modal-lg uploadModal contactus"
                    isOpen={this.state.uploadModal}
                    toggle={() => this.toggleModal("uploadModal")}
                >
                    <div className="modal-body">
                        <button
                            aria-label="Close"
                            className="close"
                            data-dismiss="modal"
                            type="button"
                            onClick={() => this.toggleModal("uploadModal")}
                        >
                            <span aria-hidden={true}>×</span>
                        </button>
                        <div className="dv-Registration-content dv-form">
                            <div className="form-group">

                                <label className="section-heading">Please provide us a few details so our sales team can reach out to you
</label>

                                <label htmlFor="txtFirstName">First Name <span className="req">*</span></label>
                                <input type="text" className="form-control" id="txtFirstName" placeholder="Enter Name" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="txtLastName">Last Name <span className="req">*</span></label>
                                <input type="text" className="form-control" id="txtLastName" placeholder="Enter Last Name" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="txtEmail">Email Address <span className="req">*</span></label>
                                <input type="text" className="form-control" id="txtEmail" placeholder="Enter Email Address" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="txtMobile">Mobile Number <span className="req">*</span></label>
                                <input type="text" className="form-control" id="txtMobile" placeholder="Enter Mobile Number" />
                            </div>
                            <div className="form-group mt-4">
                                <label htmlFor="txtCompanyName">Industry Type </label>
                                <select type="text" className="form-control" id="txtIndustryType" placeholder="Eg. Safexpay">
                                    <option value="airlines">Airlines</option>
                                    <option value="b2b">B2B</option>
                                    <option value="donation">Donation</option>
                                    <option value="ecommerce">Ecommerce</option>
                                    <option value="education">Education</option>
                                    <option value="financialservices">Financial Services</option>
                                    <option value="hospitality">Hospitality</option>
                                    <option value="travel">Travel</option>
                                    <option value="utilities">Utilities</option>
                                    <option value="other">other</option>
                                </select>
                            </div>
                            <div className="form-group mt-4">
                                <label htmlFor="txtEntityType">Entity Type </label>
                                <select type="text" className="form-control" id="txtEntityType" placeholder="Eg. Safexpay">
                                    <option>Unregistered</option>
                                    <option>Individual Proprietor</option>
                                    <option>Partnership</option>
                                    <option>Private Limited</option>
                                    <option>Public Limited</option>
                                </select>
                            </div>
                            <div className="form-group mt-4">
                                <label htmlFor="txtCompanyName">Company Name </label>
                                <input type="text" className="form-control" id="txtCompanyName" placeholder="Enter Company Name" />
                            </div>
                            <div className="form-group mt-4">
                                <label htmlFor="txtCompanyName">Company Website </label>
                                <input type="text" className="form-control" id="txtCompanyName" placeholder="Enter Company Website" />
                            </div>
                            <div className="form-group mt-4">
                                <label htmlFor="txtCompanyName">Product Requirements</label>
                                <select type="text" className="form-control" id="txtCompanyName">
                                    <option>Unregistered</option>
                                    <option>Individual Proprietor</option>
                                    <option>Partnership</option>
                                    <option>Private Limited</option>
                                    <option>Public Limited</option>
                                </select>
                            </div>
                            <div className="form-group form-check text-center my-4">
                                <input type="checkbox" className="form-check-input cont-chk-inpt" id="chkAgreeTC" />
                                <label className="form-check-label cont-chk" htmlFor="chkAgreeTC">I agree to be contacted on the mobile number and email id provided </label>
                            </div>
                            <div className="btn-holder text-center">
                                <a href="/success" className="btn btn-create-account btn-gradient green">
                                    Submit
                                        </a>
                            </div>
                            <div className="dv-bottom-bg">
                                <div className="first-bottom-bg"></div>
                                <div className="second-bottom-bg"></div>
                            </div>
                        </div>
                    </div>
                </Modal>

            </>
        );
    }
}

export default AgreementDoc;
