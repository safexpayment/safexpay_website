import React from "react";
import "assets/css/safexpay-web.css"
import "./Registration.css";

class ContantactDetails extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Registration-content">
                    <div className="first-bg"></div>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 dv-instruction" data-aos="fade-right" data-aos-duration="1000">
                                <h1 className="section-heading">Simplify your payment &amp; grow your business.</h1>
                                <ul className="tick">
                                    <li>Easily accept payments from your customers.</li>
                                    <li>Streamline payments to your vendors or partners.</li>
                                    <li>Create your own payments or banking platform.</li>
                                </ul>
                            </div>
                            <div className="col-md-6 form-div" data-aos="fade-up" data-aos-duration="1000">
                                <div className="dv-login-form dv-form pt-4">
                                    <h5 className="frm-heading">Contact Details</h5>
                                    <div className="form-group">
                                        <label htmlFor="txtFullName">Full Name<span className="red">*</span></label>
                                        <input type="text" className="form-control" id="txtFullName" placeholder="Eg. Full Name" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtEmail">Email Address<span className="red">*</span></label>
                                        <input type="email" className="form-control" id="txtEmail" placeholder="Eg. rahul@safexpay.com" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtMobNum">Full Name<span className="red">*</span></label>
                                        <input type="text" className="form-control" id="txtMobNum" placeholder="+91 ******0101" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtCompName">Company Name<span className="red">*</span></label>
                                        <input type="text" className="form-control" id="txtCompName" placeholder="Eg. Safexpay" />
                                    </div>
                                    <div className="form-group sel-cust">
                                        <label htmlFor="ddlBusinessType">Business Type</label>
                                        <select className="custom-select" id="ddlBusinessType">
                                            <option>Please select</option>
                                            <option value="Individual">Individual</option>
                                            <option value="Proprietorship">Proprietorship</option>
                                            <option value="Partnership/LLP">Partnership / LLP</option>
                                            <option value="PrivateLtd./PublicLtd.">Private Ltd. / Public Ltd.</option>
                                            <option value="Trust/Society">Trust/ Society</option>
                                        </select>
                                    </div>
                                    
                                   <div className="form-group">
                                        <label className="d-block" htmlFor="regInd1">Is your business registered in india?</label>
                                        <div className="custom-control custom-radio custom-control-inline my-3">
                                            <input type="radio" id="regInd1" name="regInd" className="custom-control-input" />
                                                <label className="custom-control-label" htmlFor="regInd1">Yes</label>
                                        </div>
                                        <div className="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="regInd2" name="regInd" className="custom-control-input" />
                                                <label className="custom-control-label" htmlFor="regInd2">No</label>
                                        </div>
                                        {/* <input type="text" className="form-control" id="txtWebsite" placeholder="Company.com" /> */}
                                    
                                    </div>

                                    <div className="form-group">
                                        <div className="iagree"><input id="terms" type="checkbox" name="terms" value="on" className="chk-inpt"/><p className="txtagr">I agree to <a href="/" className="btn btn-link tnc">Terms and Conditions*</a></p></div>
                                    </div>

                                            <div className="btn-holder mt-4 text-center">
                                                <a href="/thankyou" className="btn btn-next btn-gradient">
                                                Create Account
                                                </a>
                                            </div>

                                            <div className="dv-bottom-bg mt-5">
                                                <div className="dv-help">
                                                    Need help? <a href="/" className="lnk-contact">Contact Us</a>
                                                </div>
                                                <div className="first-bottom-bg"></div>
                                                <div className="second-bottom-bg"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                </section>

            </>
        );
    }
}

export default ContantactDetails;
