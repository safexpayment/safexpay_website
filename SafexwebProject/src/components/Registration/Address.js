import React from "react";
import "assets/css/safexpay-web.css"
import "./Registration.css";

class Address extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Registration-content">
                    <div className="first-bg"></div>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 dv-instruction" data-aos="fade-right" data-aos-duration="1000">
                                <h1 className="section-heading">Quick &amp; Easy Activation Process</h1>
                                <div className="row">
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step completed">
                                            <div className="step">Step 1</div>
                                            <div className="text">
                                                Check email for Username &amp; OTP.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step completed">
                                            <div className="step">Step 2</div>
                                            <div className="text">
                                                Enter Username &amp; OTP. <br />
                                                Set a desirable password.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step">
                                            <div className="step">Step 3</div>
                                            <div className="text">
                                                Fill in all the details &amp; upload documents to start activation process.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul className="tick mt-5 mr-5">
                                    <li className="completed">Business Overview <span className="hr-white"></span> </li>
                                    <li>Business Contact Details <span className="hr-white"></span></li>
                                    <li>Bank Account Details <span className="hr-white"></span></li>
                                    <li>Agreement &amp; KYC Documents</li>
                                </ul>
                            </div>
                            <div className="col-md-6" data-aos="fade-up" data-aos-duration="1000">
                                <div className="dv-login-form dv-form pt-5">

                                    <div className="form-group">
                                        <label htmlFor="txtCompanyContactNumber">Company Contact Number</label>
                                        <input type="text" className="form-control" id="txtCompanyContactNumber" placeholder="Enter" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="ddlCountry">Country</label>
                                        <select className="custom-select" id="ddlCountry">
                                            <option>INDIA</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtPincode">Pincode</label>
                                        <input type="text" className="form-control" id="txtPincode" placeholder="Eg.400601" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="ddlState">State</label>
                                        <select className="custom-select" id="ddlState">
                                            <option>Please select</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtCity">City</label>
                                        <input type="text" className="form-control" id="txtCity" placeholder="Eg. Mumbai" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtRegisteredAddress">Registered Address</label>
                                        <input type="text" className="form-control" id="txtRegisteredAddress" placeholder="Bldg. , Road , Area" />
                                    </div>

                                    <div className="btn-holder mt-5 text-center">
                                        <a href="/bank" className="btn btn-next btn-gradient">
                                            Save &amp; Next
                                        </a>
                                    </div>

                                    <div className="dv-bottom-bg mt-5">
                                        <div className="dv-help">
                                            Need help? <a href="/" className="lnk-contact">Contact Us</a>
                                        </div>
                                        <div className="first-bottom-bg"></div>
                                        <div className="second-bottom-bg"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </>
        );
    }
}

export default Address;
