import React from "react";
import {
    Modal
} from "reactstrap";
import "assets/css/safexpay-web.css"
import "./Registration.css";

class BusinessDetails extends React.Component {
    state = {};
    toggleModal = state => {
        this.setState({
            [state]: !this.state[state]
        });
    };

    render() {
        return (
            <>
                <section className="dv-Registration-content">
                    <div className="first-bg htg"></div>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 dv-instruction" data-aos="fade-right" data-aos-duration="1000">
                                <h1 className="section-heading">Quick &amp; Easy Activation Process</h1>
                                <div className="row">
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step completed">
                                            <div className="step">Step 1</div>
                                            <div className="text">
                                                Check email for Username &amp; OTP.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step completed">
                                            <div className="step">Step 2</div>
                                            <div className="text">
                                                Enter Username &amp; OTP. <br />
                                                Set a desirable password.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step">
                                            <div className="step">Step 3</div>
                                            <div className="text">
                                                Fill in all the details &amp; upload documents to start activation process.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul className="tick mt-5 mr-5">
                                    <li>Business Overview <span className="hr-white"></span> </li>
                                    {/* <li>Business Contact Details <span className="hr-white"></span></li> */}
                                    <li>Bank Account Details <span className="hr-white"></span></li>
                                    <li>Agreement &amp; KYC Documents</li>
                                </ul>
                            </div>
                            <div className="col-md-6 form-div" data-aos="fade-up" data-aos-duration="1000">
                                <div className="dv-login-form dv-form pt-5">
                                    <div className="form-group">
                                        <label htmlFor="txtBrandName">Brand Name<span class="red">*</span></label>
                                        <input type="text" className="form-control" id="txtBrandName" placeholder="Eg. Swiggy" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtBusinessRegisteredName">Business Registered Name<span class="red">*</span></label>
                                        <input type="text" className="form-control" id="txtBusinessRegisteredName" placeholder="As per Govt. Documents" />
                                    </div>
                                    {/* <div className="form-group">
                                        <label htmlFor="ddlBusinessType">Business Type</label>
                                        <select className="custom-select" id="ddlBusinessType">
                                            <option>Please select</option>
                                            <option value="Individual">Individual</option>
                                            <option value="Proprietorship">Proprietorship</option>
                                            <option value="Partnership/LLP">Partnership / LLP</option>
                                            <option value="PrivateLtd./PublicLtd.">Private Ltd. / Public Ltd.</option>
                                            <option value="Trust/Society">Trust/ Society</option>
                                        </select>
                                    </div> */}
                                    
                                    <div className="form-group sel-cust">
                                        <label htmlFor="ddlBusinessCategory">Business Category</label>
                                        <select className="custom-select" id="ddlBusinessCategory">
                                            <option>Please select</option>
                                            <option>Option1</option>
                                            <option>Option2</option>
                                            <option>Food Category</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <div className="label-upld">
                                            <label htmlFor="ddlBusinessSubCategory">FSSAI No.<span className="red">*</span></label>
                                            <span className="upld-txt">upload FSSAI certificate copy</span>
                                        </div>
                                        <button className="btn upld-btn" type="button" onClick={() => this.toggleModal("uploadModal")}>
                                             UPLOAD CERTIFICATE COPY
                                        </button>
                                        
                                        <input type="text" className="form-control" id="ddlBusinessSubCategory" placeholder="Please Enter" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="ddlBusinessSubCategory">Business Sub Category (Optional)</label>
                                        {/* <select className="custom-select" id="ddlBusinessSubCategory">
                                            <option>Please select</option>
                                        </select> */}
                                        <input type="text" className="form-control" id="ddlBusinessSubCategory" placeholder="Please Enter" />
                                    </div>
                                    <div className="form-group">
                                        <label className="d-block" htmlFor="rdWebsite">Do You Have A Website?<span className="red">*</span></label>
                                        <div className="custom-control custom-radio custom-control-inline my-3">
                                            <input type="radio" id="rdWebsite1" name="rdWebsite" className="custom-control-input" />
                                                <label className="custom-control-label" htmlFor="rdWebsite1">Yes</label>
</div>
                                            <div className="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="rdWebsite2" name="rdWebsite" className="custom-control-input" />
                                                    <label className="custom-control-label" htmlFor="rdWebsite2">No</label>
</div>
                                        <input type="text" className="form-control" id="txtWebsite" placeholder="Company.com" />
                                    
                                            </div>

                                            <div className="btn-holder mt-5 text-center">
                                                <a href="/thankyou" className="btn btn-next btn-gradient">
                                                    Save &amp; Next
                                        </a>
                                            </div>
                                            <div class="btn-holder mt-2 text-center">
                                                <a href="/thankyou" class="btn btn-next btn-green">Save &amp; Next</a>
                                                </div>

                                            <div className="dv-bottom-bg mt-5">
                                                <div className="dv-help">
                                                    Need help? <a href="/" className="lnk-contact">Contact Us</a>
                                                </div>
                                                <div className="first-bottom-bg"></div>
                                                <div className="second-bottom-bg"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                </section>
                <Modal
                    className="modal-dialog-centered modal-lg uploadModal"
                    isOpen={this.state.uploadModal}
                    toggle={() => this.toggleModal("uploadModal")}
                >
                    <div className="modal-body p-4">
                        <button
                            aria-label="Close"
                            className="close"
                            data-dismiss="modal"
                            type="button"
                            onClick={() => this.toggleModal("uploadModal")}
                        >
                            <span aria-hidden={true}>×</span>
                        </button>
                        <div className="dv-upload">
                            <div className="dv-upload-header">
                                <h5>Upload Image</h5>
                            </div>
                            <div className="dv-upload-img">
                                <p>Upload Image</p>
                            </div>
                            <div className="btn-holder mt-5 text-center">
                                <button  className="btn btn-gradient bt-wd">
                                   Sumbit
                                </button>
                            </div>
                            {/* <div className="dv-upload-progress">
                                <p>Uploaded image is getting veriﬁed , Please wait.</p>
                                <div className="progress">
                                    <div className="progress-bar progress-bar-striped bg-success" role="progressbar" style={{width: "80%"}} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div> */}
                        </div>
                    </div>
                </Modal>

            </>
        );
    }
}

export default BusinessDetails;
