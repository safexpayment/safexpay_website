import React from "react";
import "assets/css/safexpay-web.css"
import "./Registration.css";

class Activation extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Registration-content">
                    <div className="first-bg"></div>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6" data-aos="fade-right" data-aos-duration="1000">
                                <h1 className="section-heading">Quick &amp; Easy Activation Process</h1>
                                <div className="row">
                                    <div className="col-md-4">
                                        <div className="dv-step completed">
                                            <div className="step">Step 1</div>
                                            <div className="text">
                                                Check email for Username &amp; OTP.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="dv-step">
                                            <div className="step">Step 2</div>
                                            <div className="text">
                                                Enter Username &amp; OTP. <br />
                                                Set a desirable password.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="dv-step">
                                            <div className="step">Step 3</div>
                                            <div className="text">
                                                Fill in all the details &amp; upload documents to start activation process.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6" data-aos="fade-up" data-aos-duration="1000">
                                <div className="dv-login-form">
                                    <div class="form-group">
                                        <label for="ddlBusinessType">Business Type</label>
                                        <select class="form-control" id="ddlBusinessType">
                                            <option>Please Select</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="txtBrandName">Brand Name</label>
                                        <input type="text" class="form-control" id="txtBrandName" placeholder="Eg. Swiggy" />
                                    </div>
                                    <div class="form-group">
                                        <label for="txtBusinessRegisteredName">Business Registered Name</label>
                                        <input type="text" class="form-control" id="txtBusinessRegisteredName" placeholder="As per Govt. Documents" />
                                    </div>
                                    <div class="form-group">
                                        <label for="ddlBusinessCategory">Business Category</label>
                                        <select class="form-control" id="ddlBusinessCategory">
                                            <option>Please Select</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="ddlBusinessSubCategory">Business Sub Category</label>
                                        <select class="form-control" id="ddlBusinessSubCategory">
                                            <option>Please Select</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label className="d-block" for="website">Do You Have A Website?</label>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="website" id="websiteYes" value="Yes" />
                                            <label class="form-check-label" for="websiteYes">Yes</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="website" id="websiteNo" value="No" />
                                            <label class="form-check-label" for="websiteNo">No</label>
                                        </div>
                                        <input type="text" class="form-control" id="txtWebsite" placeholder="Company.com" />
                                    </div>
                                    
                                    <div className="btn-holder">
                                        <button className="btn btn-next">
                                           Save &amp; Next
                                        </button>
                                    </div>

                                    <div className="dv-help">
                                        Need help? <a href="/" className="lnk-contact">Contact Us</a>
                                    </div>

                                    <div className="dv-bottom-bg">
                                        <div className="first-bg"></div>
                                        <div className="second-bg"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </>
        );
    }
}

export default Activation;
