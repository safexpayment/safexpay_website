import React from "react";
import "assets/css/safexpay-web.css"
import "./Registration.css";

class BankDetails extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Registration-content">
                    <div className="first-bg htg"></div>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 dv-instruction" data-aos="fade-right" data-aos-duration="1000">
                                <h1 className="section-heading">Quick &amp; Easy Activation Process</h1>
                                <div className="row">
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step completed">
                                            <div className="step">Step 1</div>
                                            <div className="text">
                                                Check email for Username &amp; OTP.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step completed">
                                            <div className="step">Step 2</div>
                                            <div className="text">
                                                Enter Username &amp; OTP. <br />
                                                Set a desirable password.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step">
                                            <div className="step">Step 3</div>
                                            <div className="text">
                                                Fill in all the details &amp; upload documents to start activation process.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul className="tick mt-5 mr-5">
                                    <li className="completed">Business Overview <span className="hr-white"></span> </li>
                                    {/* <li className="completed">Business Contact Details <span className="hr-white"></span></li> */}
                                    <li>Bank Account Details <span className="hr-white"></span></li>
                                    <li>Agreement &amp; KYC Documents</li>
                                </ul>
                            </div>
                            <div className="col-md-6 form-div" data-aos="fade-up" data-aos-duration="1000">
                                <div className="dv-login-form dv-form pt-5">
                                    {/* <div className="form-group">
                                        <label htmlFor="ddlBankName">Bank Name</label>
                                        <select className="custom-select" id="ddlBankName">
                                            <option>Please Select</option>
                                        </select>
                                    </div> */}
                                    {/* <div className="form-group">
                                        <label htmlFor="ddlAccountType">Account Type</label>
                                        <select className="custom-select" id="ddlAccountType">
                                            <option>Please Select</option>
                                        </select>
                                    </div> */}
                                    <div className="form-group">
                                        <label htmlFor="txtAccountNumber">Account Number<span className="red">*</span></label>
                                        <input type="text" className="form-control" id="txtAccountNumber" placeholder="Eg.00000659 " />
                                    </div>
                                    {/* <div className="form-group">
                                        <label htmlFor="txtReenterAccountNumber">Reenter Account Number</label>
                                        <input type="text" className="form-control" id="txtReenterAccountNumber" placeholder="Eg.00000659" />
                                    </div> */}
                                    <div className="form-group">
                                        <label htmlFor="txtAccountHoldersName">Account Holder's Name<span className="red">*</span></label>
                                        <input type="text" className="form-control" id="txtAccountHoldersName" placeholder="Eg. Rahul Gupta" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtIFSCCode">IFSC Code<span className="red">*</span></label>
                                        <input type="text" className="form-control" id="txtIFSCCode" placeholder="BOB00000" />
                                    </div>
                                    {/* <div className="form-group">
                                        <label htmlFor="txtBankCity">Bank City</label>
                                        <input type="text" className="form-control" id="txtBankCity" placeholder="Eg. Mumbai" />
                                    </div> */}

                                    <div className="form-group mt-4 sm-btn">
                                        <button className="btn btn-next bl-btn-md">
                                            Verify Details
                                        </button>
                                    </div>
                                    <div className="btn-holder mt-5 text-center"><a href="/" className="btn btn-next btn-gradient">Save &amp; Next</a></div>
                                    {/* <div className="btn-holder mt-2 text-center">
                                        <a href="/success" className="btn btn-next btn-green">
                                            Confirmed &amp; Next
                                        </a>
                                    </div> */}

                                    <div className="dv-bottom-bg mt-5">
                                        <div className="dv-help">
                                            Need help? <a href="/" className="lnk-contact">Contact Us</a>
                                        </div>
                                        <div className="first-bottom-bg"></div>
                                        <div className="second-bottom-bg"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </>
        );
    }
}

export default BankDetails;
