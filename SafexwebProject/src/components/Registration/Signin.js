import React from "react";
import "assets/css/safexpay-web.css"
import "./Registration.css";

class Signin extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Registration-content">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 dv-instruction" data-aos="fade-right" data-aos-duration="1000">
                                <h1 className="section-heading">Simplify your payment &amp; grow your business.</h1>
                                <ul className="tick">
                                    <li>Easily accept payments from your customers.</li>
                                    <li>Streamline payments to your vendors or partners.</li>
                                    <li>Create your own payments or banking platform.</li>
                                </ul>
                            </div>
                            <div className="col-md-6 form-div" data-aos="fade-up" data-aos-duration="1000">
                                <div className="dv-login-form dv-form">
                                    <h1 className="section-heading border-heading">Login</h1>
                                    {/* <p className="text-center color-lightblue mb-3">Check email for Username &amp; OTP.</p> */}
                                    <div className="form-group mx-5">
                                        <label htmlFor="txtUsername">Username</label>
                                        <input type="text" className="form-control" id="txtUsername" placeholder="Enter Username" />
                                    </div>
                                    <div className="form-group mx-5 error-div">
                                        <label htmlFor="txtOtp">Password</label>
                                        <input type="text" className="form-control" id="txtOtp" placeholder="Enter Password" />
                                        <div className="err">*Invalid Password</div>
                                    </div>
                                    {/* <div className="dv-capcha text-center">
                                        CAPCHA
                                    </div>
                                    <p className="text-center">
                                        <a href="/" className="btn btn-resend-otp btn-link">Resend OTP</a>
                                    </p> */}
                                    <p className="text-center">
                                        <a href="/templogin" className="btn btn-forgtpass btn-link">Forgot password</a>
                                    </p>
                                    <div className="btn-holder text-center">
                                        <a href="/setpassword" className="btn btn-next btn-gradient">
                                            NEXT
                                        </a>
                                    </div>


                                    <div className="dv-bottom-bg mt-5">
                                    <div className="dv-help">
                                        Need help? <a href="/" className="lnk-contact">Contact Us</a>
                                    </div>
                                        <div className="first-bottom-bg"></div>
                                        <div className="second-bottom-bg"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </>
        );
    }
}

export default Signin;
