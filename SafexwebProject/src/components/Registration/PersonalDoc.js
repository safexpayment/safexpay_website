import React from "react";
import {
    Modal
} from "reactstrap";
import "assets/css/safexpay-web.css"
import "./Registration.css";

class PersonalDoc extends React.Component {
    state = {};
    toggleModal = state => {
        this.setState({
            [state]: !this.state[state]
        });
    };

    render() {
        return (
            <>
                <section className="dv-Registration-content doc-main-div">
                    <div className="first-bg htg"></div>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 dv-instruction" data-aos="fade-right" data-aos-duration="1000">
                                <h1 className="section-heading">Quick &amp; Easy Activation Process</h1>
                                <div className="row">
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step completed">
                                            <div className="step">Step 1</div>
                                            <div className="text">
                                                Check email for Username &amp; OTP.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step completed">
                                            <div className="step">Step 2</div>
                                            <div className="text">
                                                Enter Username &amp; OTP. <br />
                                                Set a desirable password.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step">
                                            <div className="step">Step 3</div>
                                            <div className="text">
                                                Fill in all the details &amp; upload documents to start activation process.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul className="tick mt-5 mr-5">
                                    <li className="completed">Business Overview <span className="hr-white"></span> </li>
                                    {/* <li className="completed">Business Contact Details <span className="hr-white"></span></li> */}
                                    <li className="completed">Bank Account Details <span className="hr-white"></span></li>
                                    <li>Agreement &amp; KYC Documents</li>
                                </ul>
                            </div>
                            <div className="col-md-6 form-div" data-aos="fade-up" data-aos-duration="1000">
                                <div className="dv-login-form dv-form">
                                    <h1 className="section-heading border-heading">{this.props.name}</h1>
                                    <div className="dv-indicator">
                                        <ul className="indicator">
                                            <li className="box completed"></li>
                                            <li className="line"></li>
                                            <li className="box"></li>
                                            <li className="line pid"></li>
                                            <li className="box pid"></li>
                                        </ul>
                                    </div>
                                    <h4 className="text-center doc-txt">Personal Documents</h4>

                                    {/* <div className="dv-doc dv-ind dv-prop">
                                        <div className="dv-particular">
                                            <label className="lbl">Identity Proof</label>
                                            <span className="note">Select your prefered Idenity  Proof</span>
                                        </div>
                                        <div className="dv-field">
                                            <select className="custom-select">
                                                <option>Select</option>
                                            </select>
                                            <button className="btn btn-green">VERIFIED</button>
                                        </div>
                                    </div> */}

                                    <div className="dv-doc dv-ind dv-prop">
                                        <div className="dv-particular">
                                            <label className="lbl">Address Proof<span className="red">*</span></label>
                                            <span className="note">Select your prefered Address Proof</span>
                                        </div>
                                        <div className="dv-field sel-doc">
                                            <select className="custom-select">
                                                <option>Select</option>
                                                <option>Driving License</option>
                                                <option>Voter ID</option>
                                                <option>Aadhaar Card</option>
                                                <option>Passport</option>
                                            </select>
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModal")}>UPLOAD</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-ind dv-prop error-div">
                                        <div className="dv-particular">
                                            <label className="lbl">PAN Card<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModalPan")}>UPLOAD</button>
                                        </div>
                                    </div>

                                    


                                    <div className="dv-doc dv-part">
                                        <div className="dv-particular">
                                            <label className="lbl">Director 1 :  Address Proof<span className="red">*</span></label>
                                            <span className="note">Select your prefered Address Proof</span>
                                        </div>
                                        <div className="dv-field sel-doc">
                                            <select className="custom-select">
                                                <option>Select</option>
                                                <option>Driving License</option>
                                                <option>Voter ID</option>
                                                <option>Aadhaar Card</option>
                                                <option>Passport</option>
                                            </select>
                                            <button className="btn btn-green">VERIFIED</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-part">
                                        <div className="dv-particular">
                                            <label className="lbl">Director 2 : Address Proof<span className="red">*</span></label>
                                            <span className="note">Select your prefered Address Proof</span>
                                        </div>
                                        <div className="dv-field sel-doc">
                                            <select className="custom-select">
                                                <option>Select</option>
                                                <option>Driving License</option>
                                                <option>Voter ID</option>
                                                <option>Aadhaar Card</option>
                                                <option>Passport</option>
                                            </select>
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModal")}>UPLOAD</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-part">
                                        <div className="dv-particular">
                                            <label className="lbl">Director 1 :  PAN Card<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModalPan")}>UPLOAD</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-part">
                                        <div className="dv-particular">
                                            <label className="lbl">Director 2 :  PAN Card<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModalPan")}>UPLOAD</button>
                                        </div>
                                    </div>
                                    {/* <div className="dv-doc dv-part">
                                        <div className="dv-particular">
                                            <label className="lbl">Address Proof</label>
                                            <span className="note">Select your prefered Address Proof</span>
                                        </div>
                                        <div className="dv-field">
                                            <select className="custom-select">
                                                <option>Select</option>
                                            </select>
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModal")}>UPLOAD</button>
                                        </div>
                                    </div> */}


                                    <div className="dv-doc dv-pub">
                                        <div className="dv-particular">
                                            <label className="lbl">Director 1 :  Address Proof<span className="red">*</span></label>
                                            <span className="note">Select your prefered Address Proof</span>
                                        </div>
                                        <div className="dv-field sel-doc">
                                            <select className="custom-select">
                                                <option>Select</option>
                                                <option>Driving License</option>
                                                <option>Voter ID</option>
                                                <option>Aadhaar Card</option>
                                                <option>Passport</option>
                                            </select>
                                            <button className="btn btn-green">VERIFIED</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-pub">
                                        <div className="dv-particular">
                                            <label className="lbl">Director 2 :  Address Proof<span className="red">*</span></label>
                                            <span className="note">Select your prefered Address Proof</span>
                                        </div>
                                        <div className="dv-field sel-doc">
                                            <select className="custom-select">
                                                <option>Select</option>
                                                <option>Driving License</option>
                                                <option>Voter ID</option>
                                                <option>Aadhaar Card</option>
                                                <option>Passport</option>
                                            </select>
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModal")}>UPLOAD</button>
                                        </div>
                                    </div>
                                    {/* <div className="dv-doc dv-pub">
                                        <div className="dv-particular">
                                            <label className="lbl">Director 2 : Identity Proof</label>
                                            <span className="note">Select your prefered Idenity  Proof</span>
                                        </div>
                                        <div className="dv-field">
                                            <select className="custom-select">
                                                <option>Select</option>
                                            </select>
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModal")}>UPLOAD</button>
                                        </div>
                                    </div> */}
                                    <div className="dv-doc dv-pub">
                                        <div className="dv-particular">
                                            <label className="lbl">Director 1 : PAN Card<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModalPan")}>UPLOAD</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-pub">
                                        <div className="dv-particular">
                                            <label className="lbl">Director 2 : PAN Card<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModalPan")}>UPLOAD</button>
                                        </div>
                                    </div>
                                    


                                    <div className="dv-doc dv-soc">
                                        <div className="dv-particular">
                                            <label className="lbl">Director 1 :  Address Proof<span className="red">*</span></label>
                                            <span className="note">Select your prefered Address Proof</span>
                                        </div>
                                        <div className="dv-field sel-doc">
                                            <select className="custom-select">
                                                <option>Select</option>
                                                <option>Driving License</option>
                                                <option>Voter ID</option>
                                                <option>Aadhaar Card</option>
                                                <option>Passport</option>
                                            </select>
                                            <button className="btn btn-green">VERIFIED</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-soc">
                                        <div className="dv-particular">
                                            <label className="lbl">Director 2 :  Address Proof<span className="red">*</span></label>
                                            <span className="note">Select your prefered Address Proof</span>
                                        </div>
                                        <div className="dv-field sel-doc">
                                            <select className="custom-select">
                                                <option>Select</option>
                                                <option>Driving License</option>
                                                <option>Voter ID</option>
                                                <option>Aadhaar Card</option>
                                                <option>Passport</option>
                                            </select>
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModal")}>UPLOAD</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-soc">
                                        <div className="dv-particular">
                                            <label className="lbl">Director 1 : PAN Card<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModalPan")}>UPLOAD</button>
                                        </div>
                                    </div>
                                    <div className="dv-doc dv-soc">
                                        <div className="dv-particular">
                                            <label className="lbl">Director 2 : PAN Card<span className="red">*</span></label>
                                        </div>
                                        <div className="dv-field">
                                            <button className="btn btn-lightblue" onClick={() => this.toggleModal("uploadModalPan")}>UPLOAD</button>
                                        </div>
                                    </div>
                                    

                                    {/* <div className="my-5 py-5"></div> */}

                                    <div className="div-btn-nxt dv-ind-btn">
                                        <div className="btn-holder mt-5 text-center">
                                            <a href="/agreement" className="btn btn-next btn-gradient">
                                                Next
                                            </a>
                                        </div>
                                    </div>

                                    <div className="div-btn-nxt dv-prop-btn">
                                        <div className="btn-holder mt-5 text-center">
                                            <a href="/businessprop" className="btn btn-next btn-gradient">
                                                Next
                                            </a>
                                        </div>
                                    </div>

                                    <div className="div-btn-nxt dv-part-btn">
                                        <div className="btn-holder mt-5 text-center">
                                            <a href="/businesspart" className="btn btn-next btn-gradient">
                                                Next
                                            </a>
                                        </div>
                                    </div>

                                    <div className="div-btn-nxt dv-pub-btn">
                                        <div className="btn-holder mt-5 text-center">
                                            <a href="/businesspub" className="btn btn-next btn-gradient">
                                                Next
                                            </a>
                                        </div>
                                    </div>

                                    <div className="div-btn-nxt dv-soc-btn">
                                        <div className="btn-holder mt-5 text-center">
                                            <a href="/businesssoc" className="btn btn-next btn-gradient">
                                                Next
                                            </a>
                                        </div>
                                    </div>

                                    <div className="dv-bottom-bg mt-5">
                                        <div className="dv-help">
                                            Need help? <a href="/" className="lnk-contact">Contact Us</a>
                                        </div>
                                        <div className="first-bottom-bg"></div>
                                        <div className="second-bottom-bg"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <Modal
                    className="modal-dialog-centered modal-lg uploadModal"
                    isOpen={this.state.uploadModal}
                    toggle={() => this.toggleModal("uploadModal")}
                >
                    <div className="modal-body p-4">
                        <button
                            aria-label="Close"
                            className="close"
                            data-dismiss="modal"
                            type="button"
                            onClick={() => this.toggleModal("uploadModal")}
                        >
                            <span aria-hidden={true}>×</span>
                        </button>
                        <div className="dv-upload">
                            <div className="dv-upload-header">
                                <h5>Upload Image</h5>
                            </div>
                            <div className="dv-upload-img">
                                <p>Upload Image</p>
                            </div>
                            <div className="btn-holder mt-3 text-center">
                                <button  className="btn btn-gradient bt-wd">
                                   Sumbit
                                </button>
                            </div>
                            {/* <div className="dv-upload-progress">
                                <p>Uploaded image is getting veriﬁed , Please wait.</p>
                                <div className="progress">
                                    <div className="progress-bar progress-bar-striped bg-success" role="progressbar" style={{width: "80%"}} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div> */}
                        </div>
                    </div>
                </Modal>
                <Modal
                    className="modal-dialog-centered modal-lg uploadModalPan"
                    isOpen={this.state.uploadModalPan}
                    toggle={() => this.toggleModal("uploadModalPan")}
                >
                    <div className="modal-body p-4">
                        <button
                            aria-label="Close"
                            className="close"
                            data-dismiss="modal"
                            type="button"
                            onClick={() => this.toggleModal("uploadModalPan")}
                        >
                            <span aria-hidden={true}>×</span>
                        </button>
                        <div className="dv-upload">
                            <div className="dv-upload-header">
                                <h5>Upload Your PAN Card Copy</h5>
                                <p>Please upload front side</p>
                            </div>
                            <div className="dv-upload-img">
                                    <p>Upload Image</p>
                            </div>
                            <div className="filledDet">
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="txtname">Name</label>
                                            <input type="text" className="form-control" id="txtname" placeholder="" disabled />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="ftname">Father Name</label>
                                            <input type="text" className="form-control" id="ftname" placeholder="" disabled />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="txtno">PAN No.</label>
                                            <input type="text" className="form-control" id="txtno" placeholder="" disabled />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="txtdb">Date of Birth</label>
                                            <input type="text" className="form-control" id="txtdb" placeholder="" disabled />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="btn-holder mt-3 text-center">
                                <button  className="btn btn-gradient bt-wd">
                                   Sumbit
                                </button>
                            </div>
                            {/* <div className="dv-upload-progress">
                                <p>Uploaded image is getting veriﬁed , Please wait.</p>
                                <div className="progress">
                                    <div className="progress-bar progress-bar-striped bg-success" role="progressbar" style={{width: "80%"}} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div> */}
                        </div>
                    </div>
                </Modal>

                <Modal
                    className="modal-dialog-centered modal-lg uploadModalAadhaar"
                    isOpen={this.state.uploadModalAadhaar}
                    toggle={() => this.toggleModal("uploadModalAadhaar")}
                >
                    <div className="modal-body p-4">
                        <button
                            aria-label="Close"
                            className="close"
                            data-dismiss="modal"
                            type="button"
                            onClick={() => this.toggleModal("uploadModalAadhaar")}
                        >
                            <span aria-hidden={true}>×</span>
                        </button>
                        <div className="dv-upload">
                            <div className="dv-upload-header">
                                <h5>Upload Your AADHAAR Card Copy</h5>
                                <p>Please upload front &amp; back side</p>
                            </div>
                            <div className="row ad-upld">
                                <div className="col-md-6">
                                    <div className="dv-upload-img">
                                        <p>Upload Image</p>
                                    </div>
                                    <p className="text-center mod-txt">Front Side</p>
                                </div>
                                <div className="col-md-6">
                                    <div className="dv-upload-img">
                                        <p>Upload Image</p>
                                    </div>
                                    <p className="text-center mod-txt">Back Side</p>
                                </div>
                            </div>
                            <div className="filledDet">
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="txtname">Name</label>
                                            <input type="text" className="form-control" id="txtname" placeholder="" disabled />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="txtno">Aadhaar Card No.</label>
                                            <input type="text" className="form-control" id="txtno" placeholder="" disabled />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="ftname">Address</label>
                                            <textarea className="form-control addtxtarea" name="" disabled></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="btn-holder mt-3 text-center">
                                <button  className="btn btn-gradient bt-wd">
                                   Sumbit
                                </button>
                            </div>
                            {/* <div className="dv-upload-progress">
                                <p>Uploaded image is getting veriﬁed , Please wait.</p>
                                <div className="progress">
                                    <div className="progress-bar progress-bar-striped bg-success" role="progressbar" style={{width: "80%"}} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div> */}
                        </div>
                    </div>
                </Modal>
            </>
        );
    }
}

export default PersonalDoc;
