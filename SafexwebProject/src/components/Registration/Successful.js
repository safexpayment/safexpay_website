import React from "react";
import "assets/css/safexpay-web.css"
import "./Registration.css";

class Successful extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Registration-content doc-main-div">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 dv-instruction" data-aos="fade-right" data-aos-duration="1000">
                                <h1 className="section-heading">Quick &amp; Easy Activation Process</h1>
                                <div className="row">
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step completed">
                                            <div className="step">Step 1</div>
                                            <div className="text">
                                                Check email for Username &amp; OTP.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step completed">
                                            <div className="step">Step 2</div>
                                            <div className="text">
                                                Enter Username &amp; OTP. <br />
                                                Set a desirable password.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step completed">
                                            <div className="step">Step 3</div>
                                            <div className="text">
                                                Fill in all the details &amp; upload documents to start activation process.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul className="tick mt-5 mr-5">
                                    <li className="completed">Business Overview <span className="hr-white"></span> </li>
                                    {/* <li className="completed">Business Contact Details <span className="hr-white"></span></li> */}
                                    <li className="completed">Bank Account Details <span className="hr-white"></span></li>
                                    <li className="completed">Agreement &amp; KYC Documents</li>
                                </ul>
                            </div>
                            <div className="col-md-6 form-div" data-aos="fade-up" data-aos-duration="1000">
                                <div className="dv-login-form dv-form pt-5">
                                    <h1 className="section-heading border-heading">Upload Successful</h1>
                                    <div className="successMsg text-center">
                                        <img src={require("assets/img/icons/success.png")} className="img-success my-3" alt="" />
                                        <h4 className="my-2 px-3 mxx-note">
                                        Your details and documents are being veriﬁed and we will get back to you within 3 hours with the activation kit
                                    </h4>
                                    </div>
                                    

                                    <div className="dv-bottom-bg mt-5">
                                        <div className="dv-help">
                                            Need help? <a href="/" className="lnk-contact">Contact Us</a>
                                        </div>
                                        <div className="first-bottom-bg"></div>
                                        <div className="second-bottom-bg"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </>
        );
    }
}

export default Successful;
