import React from "react";
import "assets/css/safexpay-web.css"
import "./Registration.css";

class ThankYou extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Registration-content">
                    <div className="first-bg"></div>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 dv-instruction" data-aos="fade-right" data-aos-duration="1000">
                            <h1 className="section-heading">Simplify your payment &amp; grow your business.</h1>
                                <ul className="tick">
                                    <li>Easily accept payments from your customers.</li>
                                    <li>Streamline payments to your vendors or partners.</li>
                                    <li>Create your own payments or banking platform.</li>
                                </ul>
                            </div>
                            <div className="col-md-6 form-div" data-aos="fade-up" data-aos-duration="1000">
                                <div className="dv-login-form dv-form">
                                    <h1 className="section-heading border-heading">Thank You</h1>
                                    <div className="successMsg text-center">
                                        <img src={require("assets/img/icons/thankyou.png")} className="img-thankyou" alt="" />
                                        <h4 className="my-4 px-3">
                                        We have received your request &amp; we will get back soon.
                                    </h4>
                                    </div>
                                    

                                    <div className="dv-bottom-bg mt-4">
                                        <div className="dv-help">
                                            Need help? <a href="/" className="lnk-contact">Contact Us</a>
                                        </div>
                                        <div className="first-bottom-bg"></div>
                                        <div className="second-bottom-bg"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </>
        );
    }
}

export default ThankYou;
