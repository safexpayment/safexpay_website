import React from "react";
import "assets/css/safexpay-web.css"
import "./Registration.css";

class SetPassword extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Registration-content">
                    <div className="first-bg"></div>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 dv-instruction" data-aos="fade-right" data-aos-duration="1000">
                                <h1 className="section-heading">Quick &amp; Easy Activation Process</h1>
                                <div className="row">
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step completed">
                                            <div className="step">Step 1</div>
                                            <div className="text">
                                                Check email for Username &amp; OTP.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step">
                                            <div className="step">Step 2</div>
                                            <div className="text">
                                                Enter Username &amp; OTP. <br />
                                                Set a desirable password.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4 pr-0">
                                        <div className="dv-step">
                                            <div className="step">Step 3</div>
                                            <div className="text">
                                                Fill in all the details &amp; upload documents to start activation process.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 form-div" data-aos="fade-up" data-aos-duration="1000">
                                <div className="dv-login-form dv-form">
                                    <h1 className="section-heading border-heading">Set Password</h1>

                                    <div className="form-group mx-5">
                                        <label htmlFor="txtPassword">Password</label>
                                        <input type="text" className="form-control" id="txtPassword" placeholder="Enter new password" />
                                    </div>
                                    <div className="form-group mx-5">
                                        <label htmlFor="txtConfirmPassword">Confirm Password</label>
                                        <input type="Password" className="form-control" id="txtConfirmPassword" placeholder="****" />
                                    </div>
                                    <div className="progress-bar_wrap">
                                        <div className="progress-bar_item progress-bar_item-1 active"><p className="text-prog">Weak</p></div>
                                        <div className="progress-bar_item progress-bar_item-2 active"><p className="text-prog">Medium</p></div>
                                        <div className="progress-bar_item progress-bar_item-3 active"><p className="text-prog">Strong</p></div>
                                        <div className="progress-bar_item progress-bar_item-4 active"><p className="text-prog">Very Strong</p></div>
                                    </div>
                                    <div className="form-group mt-4 mx-5">
                                        <ul className="lipass">
                                            <li>Minimum 8 characters</li>
                                            <li>1 Upper Case</li>
                                            <li>1 Lower Case</li>
                                            <li>1 Numeric Letters.</li>
                                        </ul>
                                        <div className="clearfix"></div>
                                    </div>
                                    <div className="form-group mx-5">
                                        <ul className="egPass">
                                            <li>Password Tip : Use a PASSPHRASE.</li>
                                            <li>A passphrase is a phrase or set of words.</li>
                                            <li>eg: c@pricornRgr8</li>
                                        </ul>
                                        <div className="clearfix"></div>
                                    </div>
                                    <div className="btn-holder mt-4 text-center">
                                        <a href="/business" className="btn btn-next btn-gradient">
                                            Save &amp; Next
                                        </a>
                                    </div>

                                    {/* <div className="my-5 py-3"></div> */}

                                    <div className="dv-bottom-bg mt-5">
                                    <div className="dv-help">
                                        Need help? <a href="/" className="lnk-contact">Contact Us</a>
                                    </div>
                                        <div className="first-bottom-bg"></div>
                                        <div className="second-bottom-bg"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </>
        );
    }
}

export default SetPassword;
