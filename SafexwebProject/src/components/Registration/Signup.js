import React from "react";
import "assets/css/safexpay-web.css"
import "./Registration.css";

class Signup extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Registration-content">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 dv-instruction" data-aos="fade-right" data-aos-duration="1000">
                                <h1 className="section-heading">Sign Up, Lets Get Started</h1>
                                <ul className="tick">
                                    <li>Accepting payments from anyone</li>
                                    <li>Sending money in milliseconds</li>
                                    <li>Becoming your own payments or banking platform</li>
                                </ul>
                            </div>
                            <div className="col-md-6" data-aos="fade-up" data-aos-duration="1000">
                                <div className="dv-registration-form dv-form pt-5">
                                    <div className="form-group mt-4">
                                        <label htmlFor="txtCompanyName">Company Name <span className="req">*</span></label>
                                        <input type="text" className="form-control" id="txtCompanyName" placeholder="Eg. Safexpay" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtFirstName">First Name <span className="req">*</span></label>
                                        <input type="text" className="form-control" id="txtFirstName" placeholder="Eg. Rahul" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtLastName">Last Name <span className="req">*</span></label>
                                        <input type="text" className="form-control" id="txtLastName" placeholder="Eg. Gupta" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtEmail">Email Address <span className="req">*</span></label>
                                        <input type="text" className="form-control" id="txtEmail" placeholder="Eg. rahul@safexpay.com" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtMobile">Mobile Number <span className="req">*</span></label>
                                        <input type="text" className="form-control" id="txtMobile" placeholder="+91 ******0101" />
                                    </div>
                                    <div className="form-group form-check text-center my-4">
                                        <input type="checkbox" className="form-check-input" id="chkAgreeTC" />
                                        <label className="form-check-label" htmlFor="chkAgreeTC">I agree to <a href="/terms-conditions">Terms and Conditions*</a></label>
                                    </div>
                                    <div className="btn-holder text-center">
                                        <a href="/login"  className="btn btn-create-account btn-gradient">
                                            Create Account
                                        </a>
                                    </div>
                                    <p className="text-center my-4">
                                        <a href="/login" className="btn btn-link">Already a registered user?</a>
                                    </p>
                                    <div className="dv-bottom-bg">
                                    <div className="first-bottom-bg"></div>
                                        <div className="second-bottom-bg"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </>
        );
    }
}

export default Signup;
