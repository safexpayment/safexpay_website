import React from "react";
import "assets/css/safexpay-web.css"
import "./Profile.css";

class Profile extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Profile-content">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-3 dv-profile-links">
                                <p className="my-0">My Profile</p>
                                <h2 className="mt-0 mb-4 py-0">Brand Name</h2>

                                <ul className="profileLinks mb-5 pb-5">
                                    <li><a href="/">Business Overview</a></li>
                                    <li><a href="/">Business Contact Details</a></li>
                                    <li><a href="/">Bank Account Details</a></li>
                                    <li><a href="/">KYC Personal Documents</a></li>
                                    <li><a href="/">KYC Business Documents</a></li>
                                    <li><a href="/">Agreement</a></li>
                                </ul>



                                <ul className="profileLinks mt-5 pt-5">
                                    <li><a href="/">Reset Password</a></li>
                                    <li><a href="/">Terms &amp; Conditions</a></li>
                                    <li><a href="/">Policy</a></li>
                                </ul>
                            </div>
                            <div className="col-md-9">
                            <div className="dv-Profile-content-holder pl-3">
                                <div className="row">
                                    <div className="col-md-9">
                                        <h2 className="section-heading">Business Overview</h2>
                                    </div>
                                    <div className="col-md-3">
                                        <button className="btn btn-edit">Edit Profile</button>
                                    </div>
                                </div>
                                <div className="row border-bottom pb-3">
                                    <div className="col-md-3">
                                        <div className="dv-image-upload">
                                        Upload<br />
Image<br />
100px X 100px
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="form-content">
                                            <label className="lbl">User Name</label>
                                            <span className="lblValue">Brand 123</span>
                                        </div>
                                        <div className="form-content">
                                            <label className="lbl">Company Name</label>
                                            <span className="lblValue">Safexpay</span>
                                        </div>
                                        <div className="form-content">
                                            <label className="lbl">Name</label>
                                            <span className="lblValue">Rahul Gupta</span>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="form-content">
                                            <label className="lbl">Email Id</label>
                                            <span className="lblValue">rahulgupta@safexpay.com</span>
                                        </div>
                                        <div className="form-content">
                                            <label className="lbl">Mobile</label>
                                            <span className="lblValue">+91 9008888888</span>
                                        </div>                                        
                                    </div>
                                    <div className="col-md-3">
                                        
                                    </div>
                                </div>

                                
                                <div className="row mt-3">
                                    <div className="col-md-12 text-right">                                        
                                        <button className="btn btn-edit">Edit Profile</button>
                                    </div>
                                    <div className="col-md-3">
                                        <div className="form-content">
                                            <label className="lbl">Brand Name</label>
                                            <span className="lblValue">XYZ</span>
                                        </div>
                                        <div className="form-content">
                                            <label className="lbl">Brand Name</label>
                                            <span className="lblValue">Private Ltd</span>
                                        </div>
                                        <div className="form-content">
                                            <label className="lbl">Business Type</label>
                                            <span className="lblValue">Private Ltd</span>
                                        </div>
                                        <div className="form-content">
                                            <label className="lbl">Business Category</label>
                                            <span className="lblValue">Private Ltd</span>
                                        </div>
                                        <div className="form-content">
                                            <label className="lbl">Business Sub Category</label>
                                            <span className="lblValue">Private Ltd</span>
                                        </div>
                                    </div>
                                    <div className="col-md-3">
                                        
                                    </div>
                                    <div className="col-md-3">
                                        <div className="form-content">
                                            <label className="lbl">Website</label>
                                            <span className="lblValue">safexpay.com</span>
                                        </div>                                     
                                    </div>
                                    <div className="col-md-3">
                                        
                                    </div>
                                </div>
</div>
                            </div>
                        </div>
                    </div>
                </section>

            </>
        );
    }
}

export default Profile;
