import React from "react";
import "assets/css/safexpay-web.css"
import "./Quality.css";

class Quality extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Quality">
                    <div className="container">

                        <h2 className="section-heading pb-0" data-aos="fade-up" data-aos-duration="1000">It's In Our Name To Be Safe!</h2>
                        <p className="text-center pb-3" data-aos="fade-up" data-aos-duration="1000">Safexpay means Robust Security</p>

                        <div className="row">
                            <div className="col-md-6 col-12 pr-md-4 my-4" data-aos="fade-up" data-aos-duration="1000">
                                <div className="row no-gutters">
                                    <div className="col-md-3 text-md-left text-center">
                                        <img src={require("assets/img/icons/pci.png")} className="card-img" alt="PCI DSS 3.2.1"  />

                                    </div>
                                    <div className="col-md-9 text-md-left text-center">
                                        <div className="card-body pt-0">
                                            <h5 className="card-title">PCI DSS 3.2.1</h5>
                                            <p className="card-text">We follow the global standard for card entry and transmission of this data to your bak. We undergo mandatory annual audits for this certification. We perform regular VAPT checks. You have nothing to worry about how your payments data is processed.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-12 pl-md-4 my-4" data-aos="fade-up" data-aos-duration="1000">
                                <div className="row no-gutters">
                                    <div className="col-md-3 text-md-left text-center">
                                        <img src={require("assets/img/icons/encrypt.png")} className="card-img" alt="AES &amp; TLS Encryption" />

                                    </div>
                                    <div className="col-md-9 text-md-left text-center">
                                        <div className="card-body pt-0">
                                            <h5 className="card-title">AES &amp; TLS Encryption</h5>
                                            <p className="card-text">All the data received or sent over our network is AES 256 bit encryption, which is one of the highest level of encryption in the industry. WE support TLS 1.2 which is the lastest and secure version for http channel encryption. Additionally we employ server to server SSL pinning and domain level whitelisting to filter out traffic</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-12 pr-md-4 my-4" data-aos="fade-up" data-aos-duration="1000">
                                <div className="row no-gutters">
                                    <div className="col-md-3 text-md-left text-center">
                                        <img src={require("assets/img/icons/iso.png")} className="card-img" alt="ISO 27001-2013 certified" />

                                    </div>
                                    <div className="col-md-9 text-md-left text-center">
                                        <div className="card-body pt-0">
                                            <h5 className="card-title">ISO 27001-2013 certified</h5>
                                            <p className="card-text">We are an ISO 27001-2013 certified organization. This standard provides guidelines on effective Information security Management System (ISMS). We ensure that our ISMS policy is regularly updated and implemented. Our data is safeguarded by maintaining access control to all data and assets. Secure connections using VPN/Firewall are used to connect over the internet to protect against any cyber attacks.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-12 pl-md-4 my-4" data-aos="fade-up" data-aos-duration="1000">
                                <div className="row no-gutters">
                                    <div className="col-md-3 text-md-left text-center">
                                        <img src={require("assets/img/icons/token.png")} className="card-img" alt="Card Tokenization" />

                                    </div>
                                    <div className="col-md-9 text-md-left text-center">
                                        <div className="card-body pt-0">
                                            <h5 className="card-title">Card Tokenization</h5>
                                            <p className="card-text">Your card data is secure with us, card data storage is purely optin and can be used for faster checkouts. Card details are stored using the highest level of encryption (AES 256) and in isolated device. We do not store any authentication data so that the details cannot be misused.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>

            </>
        );
    }
}

export default Quality;
