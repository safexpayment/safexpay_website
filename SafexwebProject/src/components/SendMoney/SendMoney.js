import React from "react";
import "assets/css/safexpay-web.css";
import "./SendMoney.css";
class SendMoney extends React.Component {

    render() {
        return (
            <>
                <section className="dv-SendMoney">
                <div className="container">
                        <h2 className="section-heading" data-aos="fade-up" data-aos-duration="1000">No More Hassle In Sending Money To Anyone, Anywhere.</h2>
                        <div className="row" data-aos="fade-up" data-aos-duration="1000">
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/balance.png")} className="card-img-top" alt="Transfer Money" />
                                    <div className="card-body mh78">
                                        <h5 className="card-title">Balance Threshold <br /> Alerts</h5>
                                        <p className="card-text">User can Setup the alerts incase balance below that limit , User can
                                            top up the payout account using payment gateway or NEFT/RTGS.<br /><br /><br /> </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/insurance.png")} className="card-img-top" alt="Insurance" />
                                    <div className="card-body mh78">
                                        <h5 className="card-title">Payout Links <br /><br /> </h5>
                                        <p className="card-text">Business can send payout links over
emails and SMS to send payments
instantly without the hassle of
fetching and validating bank details.
                                        <br /><br/><br/>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/beneficiary.png")} className="card-img-top" alt="Beneficiary Management" />
                                    <div className="card-body">
                                        <h5 className="card-title">Beneﬁciary Management
&amp; Account Validation</h5>
                                        <p className="card-text">Hassle Free! Beneﬁciary Management
system which enables you to add,
manage and save beneﬁciaries for
future payouts. Validate the user
account with account validation API. <br /></p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/reporting.png")} className="card-img-top" alt="Report" />
                                    <div className="card-body">
                                        <h5 className="card-title">Automated Reconciliation
&amp; Status check</h5>
                                        <p className="card-text">Transactions will be auto-reconciled
along with ability to check status for 
any transactions using API.<br /><br /><br /></p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/payout-solutions.png")} className="card-img-top" alt="Payout Solutions" />
                                    <div className="card-body">
                                        <h5 className="card-title">Payment Without
Adding beneﬁciary</h5>
                                        <p className="card-text">Quick payout without adding the
beneﬁciary using various options like 
NEFT, RTGS, IMPS etc      
                                        <br/><br /><br />
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/dashboard-2.png")} className="card-img-top" alt="Analytics" />
                                    <div className="card-body">
                                        <h5 className="card-title">Powerful Analytics <br/><br /></h5>
                                        <p className="card-text">Powerful Analytics helps to
understand where all payouts are
made, User Added, mode payments 
used, failure reasons.<br /><br /> </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}

export default SendMoney;
