import React from "react";
import "assets/css/safexpay-web.css";
import "./StartupPricing.css";
class StartupPricing extends React.Component {

    render() {
        return (
            <>
                <section className="dv-StartupPricing">
                    <div className="container">
                        <h2 className="section-heading" data-aos="fade-up" data-aos-duration="1000">Start-up pricing for Payment Gateway</h2>
                        <div className="dv-StartupPricing-content px-5" data-aos="fade-up" data-aos-duration="1000">
                            <div className="dv-btnHolder row mx-0">
                                <div className="col-6 text-center">
                                    <button className="btn active" id="btn-bestSeller">Lowest Ever pricing</button>
                                </div>
                                <div className="col-6 text-center">
                                    <button className="btn" id="btn-allPaymentMethods">All Payment Methods</button>
                                </div>

                                <div className="dv-bestSeller row mx-0" data-aos="fade-down" data-aos-duration="1000" id="dv-bestSeller">
                                    <div className="col-lg-5">
                                        <div className="dv-percentage">
                                            <label>1.65%<sup>*</sup></label>
                                        </div>
                                    </div>
                                    <div className="col-lg-7">
                                        <h2 className="section-heading">Domestic Payments</h2>
                                        <ul className="tick">
                                            <li>
                                                <strong>Net Banking</strong>
                                                <span>SBI<sup>+</sup>, HDFC<sup>+</sup>, ICICI<sup>+</sup>, Axis<sup>+</sup>, Kotak &amp; 50 more.</span>
                                            </li>
                                            <li><strong>Indian Debit &amp; Credit Card</strong></li>
                                            <li>
                                                <strong>UPI</strong>
                                                <span>Google Pay, PhonePe, Paytm, BHIM &amp; other UPI apps</span>
                                            </li>
                                            <li>
                                                <strong>Wallets</strong>
                                                <span>Amazon Pay<sup>+</sup>, Paytm, Freecharge, Mobikwik, etc</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-12 dv-note text-right">
                                        (*) Pricing per transaction &amp; GST additional. | (+) Subject to approval from banking partners.
                                    </div>
                                    <div className="col-12 dv-facility rounded-border-box pl-md-5">
                                        <ul className="ul-facility row mx-0">
                                            <li className="col-4 py-3">No Setup fees</li>
                                            <li className="col-4 py-3">No Annual Maintainance fees</li>
                                            <li className="col-4 py-3">No fees for transfer</li>                                    
                                            <li className="col-4 py-3">No fees for failed payments</li>
                                            <li className="col-4 py-3">No fees for refunds</li>
                                            <li className="col-4 py-3">No fees for support</li>
                                        </ul>
                                    </div>
                                </div>
                            
                                <div className="dv-allPaymentMethods row mx-0 d-none" data-aos="fade-down" data-aos-duration="1000" id="dv-allPaymentMethods">
                                    <div className="col-lg-6">
                                        <div className="dv-percentage">
                                            <label>1.65%<sup>*</sup></label>
                                        </div>
                                        <h2 className="section-heading">Domestic Payments</h2>
                                        <ul className="tick">
                                            <li>
                                                <strong>Net Banking</strong>
                                                <span>SBI<sup>+</sup>, HDFC<sup>+</sup>, ICICI<sup>+</sup>, Axis<sup>+</sup>, Kotak &amp; 50 more.</span>
                                            </li>
                                            <li><strong>Indian Debit &amp; Credit Card</strong></li>
                                            <li>
                                                <strong>UPI</strong>
                                                <span>Google Pay, PhonePe, Paytm, BHIM &amp; other UPI apps</span>
                                            </li>
                                            <li>
                                                <strong>Wallets</strong>
                                                <span>Amazon Pay+, Paytm, Freecharge, Mobikwik, etc</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-lg-6">
                                    <div className="dv-percentage">
                                            <label>2.75% + <span className="rupee">5</span><sup>*</sup></label>
                                        </div>
                                        <h2 className="section-heading">Other Payment Methods</h2>
                                        <ul className="tick">
                                            <li><strong>Amex Card+</strong></li>
                                            <li>
                                                <strong>EMI+</strong>
                                                <span>SBI+, HDFC+, ICICI+, Axis+, Kotak</span>
                                            </li>
                                            <li>
                                                <strong>Pay Later+</strong>
                                                <span>(LazyPay, Simpl, ePayLater, Ola Money)</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-12 dv-facility rounded-border-box pl-md-5">
                                        <ul className="ul-facility row mx-0">
                                            <li className="col-4 py-3">No Setup fees</li>
                                            <li className="col-4 py-3">No Annual Maintainance fees</li>
                                            <li className="col-4 py-3">No fees for support</li>
                                        </ul>
                                    </div>
                                    <div className="col-12 dv-note text-right">
                                        (*) Pricing per transaction &amp; GST additional. | (+) Subject to approval from banking partners.
                                    </div>
                                </div>
                            
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}

export default StartupPricing;
