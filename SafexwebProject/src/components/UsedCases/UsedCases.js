import React from "react";
import {
    Modal
} from "reactstrap";

import "assets/css/safexpay-web.css";
import "./UsedCases.css";

//Owl Carousel Libraries and Module
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
//Owl Carousel Settings
const usedCasesBoxOptions = {
    margin: 10,
    responsiveClass: true,
    nav: true,
    autoplay: true,
    rewind: true,
    navText: ["&#8249;", "&#8250;"],
    smartSpeed: 1000,
    dots: true,
      dotsEach: true,
      dotData: true,
    loop: true,
    responsive: {
        0: {
            items: 1,
        },
        400: {
            items: 1,
        },
        600: {
            items: 1,
        },
        767: {
            items: 1,
        },
        768: {
            items: 3,
        }
    },
};

const usedCasesModalSlideOptions = {
    margin: 10,
    responsiveClass: true,
    nav: true,
    autoplay: false,
    dots: true,
    rewind: true,
    navText: ["&#8249;", "&#8250;"],
    smartSpeed: 1000,
    loop: true,
    responsive: {
        0: {
            items: 1,
        },
        400: {
            items: 1,
        },
        600: {
            items: 1,
        },
        700: {
            items: 1,
        },
        1000: {
            items: 1,
        }
    },
};

class UsedCases extends React.Component {
    state = {};
    toggleModal = state => {
        this.setState({
            [state]: !this.state[state]
        });
    };

    render() {

        return (
            <>
                <section className="dv-UsedCases">

                    <div className="container">
                        <h2 className="section-heading pt-5 pb-5 mt-5" data-aos="fade-up" data-aos-duration="1000">We Understand Your Business Needs</h2>
                        <div className="usedCasesBoxSlider" data-aos="fade-up" data-aos-duration="1000">
                            <OwlCarousel className="slider-items  owl-carousel" {...usedCasesBoxOptions}>
                                <div className="item">
                                    <div className="card" onClick={() => this.toggleModal("usedCasesModal")}>
                                        <img src={require("assets/img/icons/digital-gaming.png")} className="card-img-top" alt="Digital Gaming" />
                                        <div className="card-body">
                                            <h5 className="card-title">Digital Gaming</h5>
                                            <p className="card-text">Make instant &amp; easy distribution of prizes and reward payments, thereby increasing user loyalty and retention.
                                    <br />
                                            </p>
                                        </div>
                                        {/* <div className="card-footer">
                                            <a className="more" href="safexpay.com">Learn more</a>
                                        </div> */}
                                    </div>
                                </div>
                                <div className="item">
                                    <div className="card" onClick={() => this.toggleModal("usedCasesModal")}>
                                        <img src={require("assets/img/icons/payroll.png")} className="card-img-top" alt="Payroll" />
                                        <div className="card-body">
                                            <h5 className="card-title">Payroll disbursement</h5>
                                            <p className="card-text">No delays due to bank unavailability. Now just upload a ﬁle at your convenience and disburse employee payroll at a click.</p>
                                        </div>
                                        {/* <div className="card-footer">
                                            <a className="more" href="safexpay.com">Learn more</a>
                                        </div> */}
                                    </div>
                                </div>
                                <div className="item">
                                    <div className="card" onClick={() => this.toggleModal("usedCasesModal")}>
                                        <img src={require("assets/img/icons/vendor-payout.png")} className="card-img-top" alt="Vendor Payout" />
                                        <div className="card-body">
                                            <h5 className="card-title">Vendor Payout</h5>
                                            <p className="card-text">Goodbye to troubles managing Vendor Payouts. We have made it super easy, simply upload a ﬁle using bulk/scheduled payouts. </p>
                                        </div>
                                        {/* <div className="card-footer">
                                            <a className="more" href="safexpay.com">Learn more</a>
                                        </div> */}
                                    </div>
                                </div>
                                <div className="item">
                                    <div className="card" onClick={() => this.toggleModal("usedCasesModal")}>
                                        <img src={require("assets/img/icons/loan.png")} className="card-img-top" alt="Loan Disbursal" />
                                        <div className="card-body">
                                            <h5 className="card-title">Loan Disbursal</h5>
                                            <p className="card-text">Instant loan disbursal with zero wait time for ﬁnancial organizations to disburse loans to consumers and businesses  24*7 .</p>
                                        </div>
                                        {/* <div className="card-footer">
                                            <a className="more" href="safexpay.com">Learn more</a>
                                        </div> */}
                                    </div>
                                </div>
                                <div className="item">
                                    <div className="card" onClick={() => this.toggleModal("usedCasesModal")}>
                                        <img src={require("assets/img/icons/insurance-claims.png")} className="card-img-top" alt="Insurance claims" />
                                        <div className="card-body">
                                            <h5 className="card-title">Insurance claims</h5>
                                            <p className="card-text">Reduce wait for your customers to get their insurance payouts. You approve the insurance claim &amp; we help you settle it instantly. </p>
                                        </div>
                                        {/* <div className="card-footer">
                                            <a className="more" href="safexpay.com">Learn more</a>
                                        </div> */}
                                    </div>
                                </div>
                            </OwlCarousel>
                        </div>
                    </div>
                </section>

                <Modal
                    className="modal-dialog-centered modal-xl"
                    isOpen={this.state.usedCasesModal}
                    toggle={() => this.toggleModal("usedCasesModal")}
                >
                    <div className="modal-body">

                        <OwlCarousel className="slider-items usedCasesModalSlider owl-carousel" {...usedCasesModalSlideOptions}>

                            <div className="dv-UsedCasesPop item dv-gaming">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-4 col-12">
                                            <div className="dv-challenge">
                                                <img src={require("assets/img/icons/digital-gaming.png")} className="card-img-top" alt="Digital Gaming" />
                                                <h3 className="heading">Digital Gaming</h3>
                                                <h4 className="sub-heading">Challenge</h4>
                                                <div className="dv-content">
                                                    Gamers don't trust the
                                                    gaming platform if
                                                    there are delays in
                                                    getting their winnings in
                                                    their bank account.
                                    </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 col-12">
                                            <div className="dv-solution">

                                                <button
                                                    aria-label="Close"
                                                    className="close"
                                                    data-dismiss="modal"
                                                    type="button"
                                                    onClick={() => this.toggleModal("usedCasesModal")}
                                                >
                                                    <span aria-hidden={true}>×</span>
                                                </button>
                                                <h3 className="heading">
                                                    <i>Turbo-Fast</i>
                                                    <br />WINNINGS</h3>
                                                <h4 className="sub-heading">Solution</h4>
                                                <div className="dv-content row">
                                                    <div className="col-md-5 col-12 d-flex">
                                                        <div className="align-self-center">

                                                            <img src={require("assets/img/icons/alert.png")} className="card-img-top" alt="Smart Alert" />
                                                            <p>Smart alerting
                                                            system tracks your
                                                            average payouts,
                                                            bank holidays &amp;
                                                            weekends so that
                                                            your ﬁnance team
                                                            can move funds to
                                                            cover your payouts
                                                            for the next few
days.</p>

                                                            <img src={require("assets/img/icons/transfer.png")} className="card-img-top" alt="Transfer" />
                                                            <p>Transfer game
                                                            winnings instantly
to the players.</p>
                                                        </div></div>
                                                    <div className="col-md-2 col-12 px-md-0 d-flex">
                                                        <div className="align-self-center">
                                                            <img src={require("assets/img/icons/one.png")} className="card-img-top" alt="SafexPay" />
                                                            <img src={require("assets/img/icons/connect1.png")} className="card-img-top" alt="SafexPay" />
                                                            <img src={require("assets/img/icons/two.png")} className="card-img-top" alt="SafexPay" />
                                                            <img src={require("assets/img/icons/connect.png")} className="card-img-top" alt="SafexPay" />
                                                            <img src={require("assets/img/icons/three.png")} className="card-img-top" alt="SafexPay" />
                                                        </div></div>
                                                    <div className="col-md-5 col-12 d-flex">
                                                        <div className="align-self-center">
                                                            <img src={require("assets/img/icons/players.png")} className="card-img-top" alt="Payment Gateway" />
                                                            <p>Players raise
                                                            requests for
game-winning.</p></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 col-12">
                                            <div className="dv-result">
                                                <h4 className="sub-heading">Result</h4>
                                                <div className="dv-content">
                                                    Increase in retention
                                                    and time spent on apps
                                                    by gamers. Drop in customer
                                                    queries regarding
                                                    funds transfers &amp;
                                                    account credits.
                                        <button className="btn btn-danger">GET STARTED</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div className="dv-UsedCasesPop item dv-payroll">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-4 col-12">
                                            <div className="dv-challenge">
                                                <img src={require("assets/img/icons/digital-gaming.png")} className="card-img-top" alt="Digital Gaming" />
                                                <h3 className="heading">Payroll
Disbursement</h3>
                                                <h4 className="sub-heading">Challenge</h4>
                                                <div className="dv-content">
                                                    Due to bank issues such
                                                    as bank holidays, cut oﬀ
                                                    timings and tedious ﬁle
                                                    format there are delays
                                                    in payroll disbursement.
                                    </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 col-12">
                                            <div className="dv-solution">

                                                <button
                                                    aria-label="Close"
                                                    className="close"
                                                    data-dismiss="modal"
                                                    type="button"
                                                    onClick={() => this.toggleModal("usedCasesModal")}
                                                >
                                                    <span aria-hidden={true}>×</span>
                                                </button>
                                                <h3 className="heading">
                                                    <i>Turbo-Fast</i>
                                                    <br />SALARY</h3>
                                                <h4 className="sub-heading">Solution</h4>
                                                <div className="dv-content row">
                                                    <div className="col-md-5 col-12 d-flex">
                                                        <div className="align-self-center">
                                                            <img src={require("assets/img/icons/schedule.png")} className="card-img-top" alt="Schedule" />
                                                            <p>An employer can
                                                            upload a ﬁle on the
                                                            dashboard by using
                                                            the Schedule / Bulk
upload option.</p>

                                                            <img src={require("assets/img/icons/bank.png")} className="card-img-top" alt="Bank" />
                                                            <p>Disbursement will
                                                            be done at the time
                                                            you schedule in the
                                                            employees account
                                                            without bank
dependency.</p>
                                                        </div></div>
                                                    <div className="col-md-2 col-12 px-md-0 d-flex">
                                                        <div className="align-self-center">
                                                            <img src={require("assets/img/icons/one.png")} className="card-img-top" alt="" />
                                                            <img src={require("assets/img/icons/connect1.png")} className="card-img-top" alt="" />
                                                            <img src={require("assets/img/icons/two.png")} className="card-img-top" alt="" />
                                                            <img src={require("assets/img/icons/connect.png")} className="card-img-top" alt="" />
                                                            <img src={require("assets/img/icons/three.png")} className="card-img-top" alt="" />
                                                        </div></div>
                                                    <div className="col-md-5 col-12 d-flex">
                                                        <div className="align-self-center">
                                                            <img src={require("assets/img/icons/file.png")} className="card-img-top" alt="File" />
                                                            <p>Employers can also
                                                            process non-salary
                                                            components like a
                                                            bill reimbursement
                                                            using the same
                                                            Single / Bulk upload
option.</p>
                                                        </div></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 col-12">
                                            <div className="dv-result">
                                                <h4 className="sub-heading">Result</h4>
                                                <div className="dv-content">
                                                    Happy employees with
                                                    quick salary &amp; expenses
                                                    disbursements, no
                                                    delays due to bank
                                                    unavailability or
                                                    dependency.
                                        <button className="btn btn-danger">GET STARTED</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div className="dv-UsedCasesPop item dv-payments">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-4 col-12">
                                            <div className="dv-challenge">
                                                <img src={require("assets/img/icons/digital-gaming.png")} className="card-img-top" alt="Digital Gaming" />
                                                <h3 className="heading">Vendor Payouts</h3>
                                                <h4 className="sub-heading">Challenge</h4>
                                                <div className="dv-content">
                                                    Businesses are
                                                    struggling for a solution
                                                    using which they can do
                                                    instant payments and
                                                    manage all their vendor
                                                    details under one roof.
                                    </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 col-12">
                                            <div className="dv-solution">

                                                <button
                                                    aria-label="Close"
                                                    className="close"
                                                    data-dismiss="modal"
                                                    type="button"
                                                    onClick={() => this.toggleModal("usedCasesModal")}
                                                >
                                                    <span aria-hidden={true}>×</span>
                                                </button>
                                                <h3 className="heading">
                                                    <i>Turbo-Fast</i>
                                                    <br />PAYMENTS</h3>
                                                <h4 className="sub-heading">Solution</h4>
                                                <div className="dv-content row">
                                                    <div className="col-md-5 col-12 d-flex">
                                                        <div className="align-self-center">
                                                            <img src={require("assets/img/icons/save-details.png")} className="card-img-top" alt="Save Details" />
                                                            <p>Add vendor’s bank
                                                            account details on
the dashboard.</p>

                                                            <img src={require("assets/img/icons/bank.png")} className="card-img-top" alt="Bank" />
                                                            <p> Instant credit to
                                                            vendor’s bank
account.</p>
                                                        </div></div>
                                                    <div className="col-md-2 col-12 px-md-0 d-flex">
                                                        <div className="align-self-center">
                                                            <img src={require("assets/img/icons/one.png")} className="card-img-top" alt="" />
                                                            <img src={require("assets/img/icons/connect1.png")} className="card-img-top" alt="" />
                                                            <img src={require("assets/img/icons/two.png")} className="card-img-top" alt="" />
                                                            <img src={require("assets/img/icons/connect.png")} className="card-img-top" alt="" />
                                                            <img src={require("assets/img/icons/three.png")} className="card-img-top" alt="" />
                                                        </div></div>
                                                    <div className="col-md-5 col-12 d-flex">
                                                        <div className="align-self-center">
                                                            <img src={require("assets/img/icons/manage.png")} className="card-img-top" alt="Manage Vendors" />
                                                            <p>Manage all your
                                                            vendors under one
                                                            roof. Generate
                                                            invoices, get
                                                            MIS reports with
                                                            account &amp; ﬁnance
                                                            software within
                                                            the merchant
dashboard.  </p>
                                                        </div></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 col-12">
                                            <div className="dv-result">
                                                <h4 className="sub-heading">Result</h4>
                                                <div className="dv-content">
                                                    You gets a turnkey
                                                    solution for all your
                                                    needs - from managing
                                                    your vendor details,
                                                    generating invoices via
                                                    account &amp; ﬁnance
                                                    software to doing
                                                    instant payouts.
                                        <button className="btn btn-danger">GET STARTED</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div className="dv-UsedCasesPop item dv-loan">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-4 col-12">
                                            <div className="dv-challenge">
                                                <img src={require("assets/img/icons/digital-gaming.png")} className="card-img-top" alt="Digital Gaming" />
                                                <h3 className="heading">Loan Disbursal</h3>
                                                <h4 className="sub-heading">Challenge</h4>
                                                <div className="dv-content">
                                                    Borrowers are attracted
                                                    to lenders who meet
                                                    their ﬁnancing needs
                                                    quickly.  They want the
                                                    process to be easy and
                                                    the amount to be
                                                    credited instantly.
                                    </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 col-12">
                                            <div className="dv-solution">

                                                <button
                                                    aria-label="Close"
                                                    className="close"
                                                    data-dismiss="modal"
                                                    type="button"
                                                    onClick={() => this.toggleModal("usedCasesModal")}
                                                >
                                                    <span aria-hidden={true}>×</span>
                                                </button>
                                                <h3 className="heading">
                                                    <i>Turbo-Fast</i>
                                                    <br />LOANS</h3>
                                                <h4 className="sub-heading">Solution</h4>
                                                <div className="dv-content row">
                                                    <div className="col-md-5 col-12 d-flex">
                                                        <div className="align-self-center">
                                                            <img src={require("assets/img/icons/request.png")} className="card-img-top" alt="Request" />
                                                            <p>The borrower will
                                                            raise a request for
Loan Disbursal. </p>

                                                            <img src={require("assets/img/icons/bank.png")} className="card-img-top" alt="Bank" />
                                                            <p>Instant credit to
                                                            borrower’s bank
account.</p>
                                                        </div></div>
                                                    <div className="col-md-2 col-12 px-md-0 d-flex">
                                                        <div className="align-self-center">
                                                            <img src={require("assets/img/icons/one.png")} className="card-img-top" alt="" />
                                                            <img src={require("assets/img/icons/connect1.png")} className="card-img-top" alt="" />
                                                            <img src={require("assets/img/icons/two.png")} className="card-img-top" alt="" />
                                                            <img src={require("assets/img/icons/connect.png")} className="card-img-top" alt="" />
                                                            <img src={require("assets/img/icons/three.png")} className="card-img-top" alt="" />
                                                        </div></div>
                                                    <div className="col-md-5 col-12 d-flex">
                                                        <div className="align-self-center">
                                                            <img src={require("assets/img/icons/verify.png")} className="card-img-top" alt="Verify" />
                                                            <p>The borrower's bank
                                                            account details are
                                                            veriﬁed to prevent
loan fraud.</p>
                                                        </div></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 col-12">
                                            <div className="dv-result">
                                                <h4 className="sub-heading">Result</h4>
                                                <div className="dv-content">
                                                    Zero wait time  for
                                                    customers with
                                                    instant beneﬁciary
                                                    veriﬁcation of accounts
                                                    for ﬁnancial
                                                    organizations to
                                                    disburse loans 24*7.
                                        <button className="btn btn-danger">GET STARTED</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div className="dv-UsedCasesPop item dv-insurance">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-md-4 col-12">
                                            <div className="dv-challenge">
                                                <img src={require("assets/img/icons/digital-gaming.png")} className="card-img-top" alt="Digital Gaming" />
                                                <h3 className="heading">Insurance Claims
</h3>
                                                <h4 className="sub-heading">Challenge</h4>
                                                <div className="dv-content">
                                                    Insurance companies
                                                    use cheque or other
                                                    inconvenient modes as
                                                    a traditional methods
                                                    for claim settlements.
                                    </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 col-12">
                                            <div className="dv-solution">

                                                <button
                                                    aria-label="Close"
                                                    className="close"
                                                    data-dismiss="modal"
                                                    type="button"
                                                    onClick={() => this.toggleModal("usedCasesModal")}
                                                >
                                                    <span aria-hidden={true}>×</span>
                                                </button>
                                                <h3 className="heading">
                                                    <i>Turbo-Fast</i>
                                                    <br />CLAIMS</h3>
                                                <h4 className="sub-heading">Solution</h4>
                                                <div className="dv-content row">
                                                    <div className="col-md-5 col-12 d-flex">
                                                        <div className="align-self-center">
                                                            <img src={require("assets/img/icons/save-details.png")} className="card-img-top" alt="Save Details" />
                                                            <p>Enter the insured
                                                            person’s bank
                                                            account details on
the dashboard.</p>

                                                            <img src={require("assets/img/icons/bank.png")} className="card-img-top" alt="Bank" />
                                                            <p>At one click the
                                                            claim amount gets
                                                            credited to the
                                                            insured person’s
bank account.</p>
                                                        </div></div>
                                                    <div className="col-md-2 col-12 px-md-0 d-flex">
                                                        <div className="align-self-center">
                                                            <img src={require("assets/img/icons/one.png")} className="card-img-top" alt="" />
                                                            <img src={require("assets/img/icons/connect1.png")} className="card-img-top" alt="" />
                                                            <img src={require("assets/img/icons/two.png")} className="card-img-top" alt="" />
                                                            <img src={require("assets/img/icons/connect.png")} className="card-img-top" alt="" />
                                                            <img src={require("assets/img/icons/three.png")} className="card-img-top" alt="" />
                                                        </div></div>
                                                    <div className="col-md-5 col-12 d-flex">
                                                        <div className="align-self-center">
                                                            <img src={require("assets/img/icons/file.png")} className="card-img-top" alt="" />
                                                            <p>Upload a ﬁle on the
                                                            dashboard by using
                                                            the Single / Bulk
                                                            upload option. The
                                                            beneﬁciary’s
                                                            account details are
                                                            veriﬁed before the
                                                            amount gets
transferred.</p>
                                                        </div></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 col-12">
                                            <div className="dv-result">
                                                <h4 className="sub-heading">Result</h4>
                                                <div className="dv-content">
                                                    Improves customer
                                                    satisfaction by
                                                    delivering money
                                                    instantly in time of their
                                                    need with a fully digital
                                                    claim settlement
                                                    process.
                                        <button className="btn btn-danger">GET STARTED</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </OwlCarousel>
                    </div>
                </Modal>
            </>
        );
    }
}

export default UsedCases;
