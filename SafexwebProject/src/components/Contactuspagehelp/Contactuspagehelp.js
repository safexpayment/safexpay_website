import React from "react";
import ReactDOM from "react-dom";
import "./Contactuspagehelp.css";
import axios from 'axios';

class Contactuspage extends React.Component{

    state={
        name:'',
        mobilenumber:'',
        emailid:'',
        industrytype:'',
        i_agree:'false'
    }

    checkflag = false;
    flag=0;
    datasend = false;

    postdatahandler=()=>{

        var emailid = this.state.emailid;
        var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/; 
        console.log('====>>>>>>>', pattern.test(emailid));
        var validemail = pattern.test(emailid);


        const data ={
            "full_name": this.state.name,
            "email_id": this.state.emailid,
            "mobile_no": this.state.mobilenumber,
            "industry_type": this.state.industrytype,
            "form_type":"ContactUs",
            "i_agree":"true"
        };


        console.log(data);
        console.log("checkflag",this.checkflag);
        console.log("state-i_agree",this.state.i_agree);

        if(this.state.name == "" || this.state.mobilenumber == "" || this.state.emailid == "")
        {
            this.datasend = false;
           
        }
        else{
            this.datasend = true
        }

        if(this.datasend)
        {
        if(validemail)
        {
        if(this.state.i_agree==true)
        {
            console.log("data send");
            this.checkflag = true;

            document.getElementById("submitbtn6").style.display = "none";
            //document.getElementById("spinner6").style.visibility = "visible";
            document.getElementById("spinner6").style.display="block";

           
            
            axios.post('https://www.avantgardepayments.com/agadmin/api/emailSender/post',data).then(response=>{
                     console.log(response);
                     this.datasend = false;

                     if(response.data.message = "Success")
                     {
                        // document.getElementById("spinner6").style.visibility = "hidden";
                         document.getElementById("spinner6").style.display= "none";
                         document.getElementById("submitbtn6").style.display = "inline";
                         document.getElementById("submitbtn6").style.backgroundColor = 'var(--darkgreen)';
                         document.getElementById("submitbtn6").innerHTML = "Send";
                         //document.getElementById("confmsg6").style.visibility = "visible";
                         document.getElementById("confmsg6").style.display = "block" 
                     }

            if(response.data.message = "Success")
            {
           setTimeout(function(){
            document.getElementById("submitbtn6").style.backgroundColor = 'var(--lightblue)';
            document.getElementById("submitbtn6").innerHTML = "Submit";
            //document.getElementById("confmsg6").style.visibility = "hidden";
            document.getElementById("confmsg6").style.display = "none";
             },5000)
             }

             if(response.data.message = "Success")
        {
            if(this.checkflag){
                document.getElementById("txtFirstName6").value = '';
                document.getElementById("txtEmail6").value = '';
                document.getElementById("txtMobile6").value = '';
                document.getElementById("chkAgreeTC6").checked = false;
                document.getElementById("txtIndustryType6").selectedIndex = 0;
                this.flag=this.flag+1;
                if(this.flag%2 == 0)
                {
                    this.setState({
                        i_agree:false
                    })
                }
                else{
                    this.setState({
                        i_agree:true
                    })
                }
                this.checkflag = false;
                
            }
    }

    if(response.data.message = "Success")
    {
     setTimeout(function(){
            document.getElementById("fullform6").classList.add("close6");
            document.getElementById("backdrop6").classList.add("close6");

         },4000)
    }
                 });
/*
           setTimeout(function(){
                    document.getElementById("fullform6").classList.add("close6");
                    document.getElementById("backdrop6").classList.add("close6");
        
             },1000)*/


          
        
        }
        else{
            alert("please click in checkbox before click on submit button");
        }

    }
    else{
            alert("Please Enter Valid Email Id");
    }
}
else{
    alert("please provide data then enter submit button");
}
            

        



       /* setTimeout(function(){ document.getElementById("submitbtn4").style.backgroundColor = 'var(--lightblue)';
                               document.getElementById("submitbtn4").innerHTML = "Submit"; }, 5000);*/
        
       // axios.post('www.avantgardepayments.com/agadmin/api/emailSender/post',data).then(response=>{
       //     console.log(response);
       // })
    }

    clickhandler=()=>{
        this.flag=this.flag+1;
        if(this.flag%2 == 0)
        {
            this.setState({
                i_agree:false
            })
        }
        else{
            this.setState({
                i_agree:true
            })
        }
        
    }


   
    render(){
        var attchedClasses1 =["dv-Registration-content", "close6"];
        var backdropcl = ["Backdrop1","close2"];
        if(this.props.open)
        {
        attchedClasses1 = ["dv-Registration-content","Open2"];
        backdropcl = ["Backdrop1"];
        }

        return (
            <>
             <div className={backdropcl.join(' ')} id="backdrop6" onClick={this.props.click}></div>
             
                <section className={attchedClasses1.join(' ')} id="fullform6">            
                                <div className="dv-registration-form dv-form pt-5">
                                <button className="close" onClick={this.props.click}>
                                    <span aria-hidden={true}>×</span>
                                </button>
                                    <div className="form-group">
                                    <div className="contus">
                                        {/*<label className="section-heading">Please provide us a few details so our sales team can reach out to you</label>*/}
                                        <h1 className="section-heading border-heading">Contact Us</h1>
                                    </div>
                                        <label htmlFor="txtFirstName">Full Name <span className="req">*</span></label>
                                        <input type="text" className="form-control form-option1" id="txtFirstName6" placeholder="Enter Name" onChange={(event)=>this.setState({name:event.target.value})}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtEmail">Email Address <span className="req">*</span></label>
                                        <input type="email" className="form-control form-option1" id="txtEmail6" placeholder="Enter Email Address" onChange={(event) => this.setState({emailid:event.target.value})}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="txtMobile">Mobile Number <span className="req">*</span></label>
                                        <input type="number" className="form-control form-option1" id="txtMobile6" placeholder="Enter Mobile Number" onChange={(event) => this.setState({mobilenumber:event.target.value})}/>
                                    </div>
                                    <div className="form-group mt-4">
                                        <label htmlFor="txtCompanyName">Industry Type </label>
                                        <select type="text" className="form-control form-option1" id="txtIndustryType6" placeholder="Eg. Safexpay" onChange={(event) => this.setState({industrytype:event.target.value})}>
                                            <option value="select">Select</option>
                                            <option value="airlines">Airlines</option>
                                            <option value="b2b">B2B</option>
                                            <option value="donation">Donation</option>
                                            <option value="ecommerce">Ecommerce</option>
                                            <option value="education">Education</option>
                                            <option value="financialservices">Financial Services</option>
                                            <option value="hospitality">Hospitality</option>
                                            <option value="travel">Travel</option>
                                            <option value="utilities">Utilities</option>
                                            <option value="other">other</option>                   
                                        </select>
                                    </div>
                                    <div className="form-group form-check text-center my-4">
                                        <input type="checkbox" className="form-check-input" id="chkAgreeTC6" onClick={this.clickhandler}/>
                                        <label className="form-check-label" htmlFor="chkAgreeTC">I agree to be contacted on the mobile number and email id provided </label>
                                    </div>
                                    <h6 className="confmsg1" id="confmsg6">Thankyou,Your message has been successfully sent.</h6>
                                    <div className="text-center">
                                        <button  className="submitbtn1" id="submitbtn6" onClick={this.postdatahandler}>
                                        Submit
                                        </button>
                                    </div>
                                    <div className="text-center myspin4" id="spinner6">
                                        <button  className="submitbtn1">
                                        Loading <span class="fa fa-refresh fa-spin"></span>
                                        </button>
                                    </div>
                                    
                                </div>
                </section>
            </>
        );
    }
}

export default Contactuspage;
