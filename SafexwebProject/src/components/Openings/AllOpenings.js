import React from "react";
import { Tabs, Tab } from 'react-bootstrap';
import Technology from "components/Openings/Technology.js";
import Commercial from "components/Openings/Commercial.js";
import "assets/css/safexpay-web.css"
import "./Career.css";

class AllOpenings extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Career">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 pl-md-5 pr-md-5">

                                <h2 className="section-heading text-center py-2 my-0" data-aos="fade-up" data-aos-duration="1000">
                                    Our Aspirations
                        </h2>
                                <h5 className="pl-md-5 pr-md-5 ml-md-5 mr-md-5 text-center">To build a global,  digital platform infrastructure and
                                product company focused for every business, with
                                an extraordinary culture that attracts innovative, collaborative people.
                        </h5>

                            </div>
                        </div>
                        <h2 className="section-heading text-center py-2 mt-5" data-aos="fade-up" data-aos-duration="1000">
                            Open Position
                        </h2>
<div data-aos="fade-up" data-aos-duration="1000">
                        <Tabs id="career-tab"
                            defaultActiveKey="technology">
                            <Tab eventKey="technology" title="Technology">
                                <Technology />
                            </Tab>
                            <Tab eventKey="commercial" title="Commercial">
                                <Commercial />
                            </Tab>
                            <Tab eventKey="allpositions" title="All Positions">
                                <Technology />
                                <Commercial />
                            </Tab>
                        </Tabs>
                 </div>   </div>
                </section>

            </>
        );
    }
}

export default AllOpenings;
