import React from "react";
import "assets/css/safexpay-web.css"
import "./Career.css";

class Technology extends React.Component {

    render() {
        return (
            <>
                <h4 class="career-txt-heading">Technology</h4>
                <ul className="container ul-position">
                    <li className="row li-position">
                        <div className="col-md-8">
                            <div className="lblPosition">Java Developer / Sr. Java Developer</div>                            
                            <strong className="lblLocation">Mumbai </strong>
                            <span className="lblTime"> : Full Time</span>
                            <span className="lblExp"> |  2 years Above</span>
                        </div>
                        <div className="col-md-4 rep-text-center text-right">
                            <a href="/career/java-developer" className="btn btn-Apply">Apply Now</a>    
                        </div>
                    </li>
                    
                  {/*  <li className="row li-position">
                        <div className="col-md-8">
                            <div className="lblPosition">Product Manager</div>                             
                            <strong className="lblLocation">Mumbai </strong>
                            <span className="lblTime"> : Full Time</span>
                            <span className="lblExp"> |  10 years Above</span>
                        </div>
                        <div className="col-md-4 rep-text-center text-right">
                        <a href="/career/product-manager" className="btn btn-Apply">Apply Now</a>    
                        </div>
        </li>*/}
                    <li className="row li-position">
                        <div className="col-md-8">
                            <div className="lblPosition">Team Lead</div>                             
                            <strong className="lblLocation">Mumbai </strong>
                            <span className="lblTime"> : Full Time</span>
                            <span className="lblExp"> |  6 years Above</span>
                        </div>
                        <div className="col-md-4 rep-text-center text-right">
                        <a href="/career/team-lead" className="btn btn-Apply">Apply Now</a>    
                        </div>
                    </li>
                    <li className="row li-position">
                        <div className="col-md-8">
                            <div className="lblPosition">App support Engineer</div>                             
                            <strong className="lblLocation">Mumbai </strong>
                            <span className="lblTime"> : Full Time</span>
                            <span className="lblExp"> |  2 years Above</span>
                        </div>
                        <div className="col-md-4 rep-text-center text-right">
                        <a href="/career/app-support-engineer" className="btn btn-Apply">Apply Now</a>    
                        </div>
                    </li>
                    <li className="row li-position">
                        <div className="col-md-8">
                            <div className="lblPosition">VP Engineering</div>                             
                            <strong className="lblLocation">Mumbai </strong>
                            <span className="lblTime"> : Full Time</span>
                            <span className="lblExp"> |  10 years Above</span>
                        </div>
                        <div className="col-md-4 rep-text-center text-right">
                        <a href="/career/vp-engineer" className="btn btn-Apply">Apply Now</a>    
                        </div>
                    </li>
                    <li className="row li-position">
                        <div className="col-md-8">
                            <div className="lblPosition">IOS Developer</div>                             
                            <strong className="lblLocation">Mumbai </strong>
                            <span className="lblTime"> : Full Time</span>
                            <span className="lblExp"> |  2 years Above</span>
                        </div>
                        <div className="col-md-4 rep-text-center text-right">
                        <a href="/career/ios-developer" className="btn btn-Apply">Apply Now</a>    
                        </div>
                    </li>
                </ul>
            </>
        );
    }
}

export default Technology;
