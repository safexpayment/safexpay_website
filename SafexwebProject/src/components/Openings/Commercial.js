import React from "react";
import "assets/css/safexpay-web.css"
import "./Career.css";

class Commercial extends React.Component {

    render() {
        return (
            <>
               <h4 class="career-txt-heading">Commercial</h4>
                <ul className="container ul-position">
                    <li className="row li-position">
                        <div className="col-md-8">
                            <div className="lblPosition">Business Development</div>                             
                            <strong className="lblLocation">Mumbai </strong>
                            <span className="lblTime"> : Full Time</span>
                            <span className="lblExp"> |  0 years (fresher)</span>
                        </div>
                        <div className="col-md-4 rep-text-center text-right">
                        <a href="/career/business-development" className="btn btn-Apply">Apply Now</a>    
                        </div>
                    </li>
                    
                   
                   
                   
                </ul>
            </>
        );
    }
}

export default Commercial;
