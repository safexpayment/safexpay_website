import React from "react";
import "assets/css/safexpay-web.css"
import "./Location.css";

class Location extends React.Component {
    render() {
        return (
            <>
                <section className="dv-Location pt-5 pb-5 mb-5">
                    <div className="container">
                        <h2 className="section-heading text-center" data-aos="fade-up" data-aos-duration="1000">
                            Our Location
                        </h2>
                        <div className="container" data-aos="fade-up" data-aos-duration="1000">                            
                            <div className="row">
                                <div className="col-md-4 dv-address offset-md-1">                                    
                                    <h2 className="section-heading text-left mb-0 pb-0">
                                        Mumbai
                                    </h2>
                                    <p>627, Lodha Supremous -2, Wagle Estate Thane -West</p>
                                </div>
                                <div className="col-md-4 dv-address">                                 
                                    <h2 className="section-heading text-left mb-0 pb-0">
                                        Delhi
                                    </h2>
                                    <p>879, Udyog Vihar ,Phase V, Gurgoan -122016</p>
                                </div>
                                <div className="col-md-3 dv-address">
                                    <p>Bangalore</p>
                                    <p>UAE</p>
                                    <p>Singapore</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}
export default Location;
