import React from "react";
import "assets/css/safexpay-web.css";
import "./MakePayouts.css";
import Contactuspage from "components/Contactuspage/Contactuspage";

class HeroBanner extends React.Component {
    state={
        open:false
    }

    showcontactuspage =(props)=>{
        console.log("button click.....");
        this.setState({
            open:!this.state.open
        })

    }

    closepage=()=>{
        this.setState({
            open:!this.state.open
        })
    }

    render() {
        return (
            <>
                <Contactuspage open={this.state.open} click={this.closepage}/>            
                <section className="dv-HeroBanner dv-MakePayoutsHeroBanner">
                    
                <div className="first-bg"></div>
                <div className="second-bg"></div>
                    <div className="container pt-5">
                        <div className="row">
                            <div className="col-md-6 col-12 text-md-left text-center pr-0 align-self-center">
                                <h1 className="herobanner-heading">
                                   <strong>Transfer Money 24*7 with <br />
Safexpay Payout instantly anywhere...</strong> 
                                            </h1>
                                <p className="herobanner-content">
                                Now experience a platform that understands your business better and helps you
transfer money eﬃciently with speed and convenience.
                                            </p>
                                <div className="btn-wrapper">
                                    {/* as */}
                                    {/* <button
                                        className="btn btn-white-rounded mr-4">
                                        Get Started
                                        </button> */}
                                    <button
                                        className="btn btn-white-rounded"
                                        onClick={this.showcontactuspage}>
                                         TALK TO US
                                        </button>
                                </div>
                            </div>
                            <div className="col-md-6 col-12 text-center">
                                <img
                                    alt=""
                                    className="img-fluid"
                                    src={require("assets/img/banners/move-money.png")}
                                />
                            </div>
                        </div>
                    </div>
                
                </section>
            </>
        );
    }
}

export default HeroBanner;
