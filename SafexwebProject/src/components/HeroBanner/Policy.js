import React from "react";
import "assets/css/safexpay-web.css";
import "./Policy.css";
class HeroBanner extends React.Component {

    render() {
        return (
            <>
                <section className="dv-HeroBanner dv-Policy" data-aos="fade-up" data-aos-duration="1000">
                    <h1 className="herobanner-heading text-center pt-5 font-w6">
                    Privacy Policy
                                            </h1>
                </section>
            </>
        );
    }
}

export default HeroBanner;
