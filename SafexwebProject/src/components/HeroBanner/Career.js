import React from "react";
import "assets/css/safexpay-web.css";
import "./Career.css";
class HeroBanner extends React.Component {

    render() {
        return (
            <>
                <section className="dv-HeroBanner dv-Policy" data-aos="fade-up" data-aos-duration="1000">
                    <h1 className="herobanner-heading text-center pt-5 font-w6">
                    Work at the most dynamics ﬁntech company.
                    </h1>
                    <p className="text-center">Work @<img 
                                        className="inline-logo"
                                        alt="Safexpay"
                                        src={require("assets/img/logos/logo_wh.png")}
                                    /> </p>
                </section>
            </>
        );
    }
}

export default HeroBanner;
