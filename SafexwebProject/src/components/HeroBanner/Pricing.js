import React from "react";
import "assets/css/safexpay-web.css";
import "./Pricing.css";
class HeroBanner extends React.Component {

    render() {
        return (
            <>
                <section className="dv-HeroBanner dv-PricingHeroBanner">
                    <div className="container pt-5">
                        <div className="row">
                            <div className="col-12 text-center align-self-center" >
                                <h1 className="herobanner-heading">
                                Lowest and Transparent Pricing
                                            </h1>
                                <p className="herobanner-content">
                                with no setup charges
                                            </p>
                            </div>
                        </div>
                    </div>
                
                </section>
            </>
        );
    }
}

export default HeroBanner;
