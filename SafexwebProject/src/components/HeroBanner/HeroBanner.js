import React from "react";
import "assets/css/safexpay-web.css";
import "./HeroBanner.css";
import Contactuspage from "components/Contactuspage/Contactuspage";

class HeroBanner extends React.Component {
    state={
        open:false
    }

    showcontactuspage =(props)=>{
        console.log("button click.....");
        this.setState({
            open:!this.state.open
        })

    }

    closepage=()=>{
        this.setState({
            open:!this.state.open
        })
    }

    render() {
        return (
            <>
                <Contactuspage open={this.state.open} click={this.closepage}/>
                <section className="dv-HeroBanner dv-HomeHeroBanner">                    
                    <div className="container">
                        <div className="row">
                            <div className="col-md-5 col-12 text-md-left text-center pr-0 align-self-center">
                                <h1 className="herobanner-heading">
                                    <strong>
                                    Simplify your payments,<br />
banking needs &amp;<br />
grow your business</strong>
                                            </h1>
                                <p className="herobanner-content  animate__animated animate__fadeIn">
                                    Easily accept payments from your customers.
                                                <br />Streamline payments to your vendors or partners.
                                                <br />Be the bank for your customers or employees.
                                            </p>
                                 <div className="btn-wrapper">
                                    {/* <button
                                        className="btn btn-white-rounded mr-4 animate__animated animate__fadeIn">
                                        Get Started
                                        </button> //till here only*/}
                                    <button
                                        className="btn btn-white-rounded animate__animated animate__fadeIn"
                                        onClick={this.showcontactuspage}>
                                        Talk To Us
                                    </button>
                                </div>
                            </div>
                            <div className="col-md-7 col-12 pl-0">
                                <img
                                    alt="Safex Payment Gateway"
                                    className="img-fluid"
                                    src={require("assets/img/banners/home.png")}
                                />
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}

export default HeroBanner;
