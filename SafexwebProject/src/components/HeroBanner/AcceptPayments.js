import React from "react";
import "assets/css/safexpay-web.css";
import "./AcceptPayments.css";
import Contactuspagenew from "components/Contactuspage/Contactuspage";

class HeroBanner extends React.Component {
    state={
        open:false
    }

    showcontactuspagenew =(props)=>{
        console.log("button click.....");
        this.setState({
            open:!this.state.open
        })

    }

    closepage=()=>{
        this.setState({
            open:!this.state.open
        })
    }

    render() {
        return (
            <>
                <Contactuspagenew open={this.state.open} click={this.closepage}/>
                <section className="dv-HeroBanner dv-AcceptPaymentHeroBanner">
                    <div className="first-bg"></div>
                    <div className="second-bg"></div>
                    <div className="container pt-5">
                        <div className="row">
                            <div className="col-md-6 col-12 text-md-left text-center pr-md-0 align-self-center" >
                                <h1 className="herobanner-heading"><strong>
                                    One platform to accept payments how you want</strong>
                                            </h1>
                                <p className="herobanner-content">
                                   Develop a customised experience by integrating with our APIs and SDKs OR simply create a payment button or send a link to your customers with no development effort.
                                            </p>
                               <div className="btn-wrapper">
                                    {/* <button
                                        className="btn btn-white-rounded mr-4">
                                        Get Started
                                        </button> //till here commit prev*/}
                                    <button
                                        className="btn btn-white-rounded"
                                        onClick={this.showcontactuspagenew}>
                                        TALK TO US 
                                        </button>
                                </div>
                            </div>
                            <div className="col-md-6 col-12 pl-0" >
                                <img
                                    alt=""
                                    className="img-fluid"
                                    src={require("assets/img/section/accept-payments.png")}
                                />
                            </div>
                        </div>
                    </div>
                
                </section>
            </>
        );
    }
}

export default HeroBanner;
