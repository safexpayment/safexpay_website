import React from "react";
import "assets/css/safexpay-web.css";
import "./DigitalBanking.css";
import Contactuspage from "components/Contactuspage/Contactuspage";

class HeroBanner extends React.Component {
    state={
        open:false
    }

    showcontactuspage =(props)=>{
        console.log("button click.....");
        this.setState({
            open:!this.state.open
        })

    }

    closepage=()=>{
        this.setState({
            open:!this.state.open
        })
    }

    render() {
        return (
            <>
                <Contactuspage open={this.state.open} click={this.closepage}/>
                <section className="dv-HeroBanner dv-DigitalBanking">
                    <div className="first-bg"></div>
                    <div className="second-bg"></div>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 col-12 text-md-left text-center pr-0 align-self-center">
                                <h1 className="herobanner-heading">
                                   <strong>Our platform + Your brand = <br />
                                Ready to sell in minutes !</strong> 
                                            </h1>
                                <p className="herobanner-content">
                                    We support deep customisation for all our products, pick and choose your features and you’re in business!
                                            </p>
                                <div className="btn-wrapper">
                                    <button
                                        className="btn btn-white-rounded mr-4" onClick={this.showcontactuspage}>
                                        TALK TO US
                                        </button>
        </div>
                            </div>
                            <div className="col-md-6 col-12 px-0">
                                <img
                                    alt=""
                                    className="img-fluid"
                                    src={require("assets/img/banners/platform.png")}
                                />
                            </div>
                        </div>
                    </div>

                </section>
            </>
        );
    }
}

export default HeroBanner;
