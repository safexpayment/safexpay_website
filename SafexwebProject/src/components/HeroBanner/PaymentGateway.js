import React from "react";
import "assets/css/safexpay-web.css";
import "./PaymentGateway.css";
import ToStart from "components/ToStart/ToStart.js";
import Contactuspage from "components/Contactuspage/Contactuspage";

class HeroBanner extends React.Component {
    state={
        open:false
    }

    showcontactuspage =(props)=>{
        console.log("button click.....");
        this.setState({
            open:!this.state.open
        })

    }

    closepage=()=>{
        this.setState({
            open:!this.state.open
        })
    }

    render() {
        return (
            <>
                <Contactuspage open={this.state.open} click={this.closepage}/>
                <section className="dv-HeroBanner dv-PaymentGateway">
                    <div className="first-bg"></div>
                    <div className="second-bg"></div>
                    <div className="third-bg"></div>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 col-12 text-md-left text-center pr-0 align-self-center pt-5">
                                <h1 className="herobanner-heading">
                                    <strong>Your business, your look &amp; feel,
Your platform!</strong>
                                            </h1>
                                <p className="herobanner-content">
                                Use our payment gateway platform out of the box 
Your business, your look &amp; feel,
Your platform!
to start acquiring merchants without worring about
integrating with banks, creating integration kits,
and merchant support systems.
                                </p>
                                <div className="btn-wrapper d-block">
                                    <button
                                        onClick={this.showcontactuspage}
                                        className="btn btn-white-rounded mr-4">
                                        SCHEDULE  A  DEMO &#10230;
                                    </button>                                    
        </div>
                            </div>
                            <div className="col-md-6 col-12 pt-5" >
                                <img
                                    alt=""
                                    className="img-fluid"
                                    src={require("assets/img/banners/payment-gateway.png")}
                                />
                            </div>
                        </div>
                    </div>
                    <ToStart />
                </section>
            </>
        );
    }
}

export default HeroBanner;
