import React from "react";
import "assets/css/safexpay-web.css";
import "./Terms.css";
class HeroBanner extends React.Component {

    render() {
        return (
            <>
                <section className="dv-HeroBanner dv-Terms">
                <h1 className="herobanner-heading text-center pt-5 font-w6" data-aos="fade-up" data-aos-duration="1000">
                                Terms of Use
                                            </h1>
                        
                </section>
            </>
        );
    }
}

export default HeroBanner;
