import React from "react";
import "assets/css/safexpay-web.css";
import "./FullToolKit.css";
class FullToolKit extends React.Component {

    render() {
        return (
            <>
                <section className="dv-FullToolKit">
                    <div className="first-bg"></div>
                    <div className="second-bg"></div>
                <div className="container">
                        <h2 className="section-heading" data-aos="zoom-in-up" data-aos-duration="1000">Full toolkit to manage your clients &amp; <br /> their requirements</h2>
                        <div className="row" data-aos="fade-up" data-aos-duration="1000">
                            <div className="col-md-6 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/merchant.png")} className="card-img-top" alt="Merchant" />
                                    <div className="card-body">
                                        <h5 className="card-title">Merchant Management</h5>
                                        <p className="card-text">Easily create &amp; manage your merchants, Use our spectacular dashboards to view transactions, get deep payment analytics &amp; manage all processes such as refunds, chargebacks &amp; disputes</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/payment-gateway-2.png")} className="card-img-top" alt="Payment Gateway" />
                                    <div className="card-body">
                                        <h5 className="card-title">Payment Gateway Management </h5>
                                        <p className="card-text">We let you oﬀer over 100 payment modes. You get to choose the price you create for your own clients. Let your clients use solutions with our advanced multi currency, recurring payments. </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/reconciliation.png")} className="card-img-top" alt="Reconciliation" />
                                    <div className="card-body">
                                        <h5 className="card-title">Reconciliation &amp; Settlement</h5>
                                        <p className="card-text">We reconcile all transactions and settle directly in your merchants account, your commission is deposited in your account! Get detailed reports and analytics to see the calculations &amp; payouts</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/security.png")} className="card-img-top" alt="Security" />
                                    <div className="card-body">
                                        <h5 className="card-title">Security &amp; Certiﬁcations</h5>
                                        <p className="card-text">We ensure transaction &amp; platform security with our 256 bit encryption &amp; PCI DSS 3.2.1certiﬁed platform. We perform regular vulnerability assessment &amp; penetration testing to prevent any misuse of the platform.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}

export default FullToolKit;
