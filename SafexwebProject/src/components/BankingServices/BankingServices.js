import React from "react";
import "assets/css/safexpay-web.css";
import "./BankingServices.css";
import Contactuspage from "components/Contactuspagenew/Contactuspagenew";

class BankingServices extends React.Component {
    state={
        open:false
    }

    showcontactuspage =(props)=>{
        console.log("button click.....");
        this.setState({
            open:!this.state.open
        })

    }

    closepage=()=>{
        this.setState({
            open:!this.state.open
        })
    }

    render() {
        return (
            <>
                <Contactuspage open={this.state.open} click={this.closepage}/>
                <section className="dv-BankingServices">
                    <div className="first-bg"></div>
                    <div className="second-bg"></div>
                    <div className="container">
                        <div className="row" data-aos="fade-up" data-aos-duration="1000">
                            <div className="col-md-6 col-12 pt-5">
                                <h2 className="section-heading text-center text-md-left mt-5 text-white">Earn By Providing Banking Services To Your Users!</h2>
                                <p className="content text-center text-md-left">Create co-branded cards/wallets for your users. Users can
use these cards to pay online or oﬄine anywhere. We also
provide a white labelled mobile app &amp; web portal for your
users to manage these cards and spend on bill payments,
mobile recharge, bus &amp; ﬂight ticket booking, gift card
purchases for over 50 brands. You earn every time your
customer uses your branded card.</p>
                                    <div className="btnDiv rep-text-center">
                                      <button
                                        className="btn btn-white-rounded my-5" onClick={this.showcontactuspage}>
                                        TALK TO US 
                                        </button>
                                    </div>
                            </div>
                            <div className="col-md-6 col-12">
                            <img
                                    alt="Banking Services"
                                    className="img-fluid sp-img"
                                    src={require("assets/img/section/banking-services.png")}
                                />
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}

export default BankingServices;
