import React from "react";
import "assets/css/safexpay-web.css"
import "./MoreHelp2.css";
import Contactuspage from "components/Contactuspage/Contactuspage";

class MoreHelp extends React.Component {
    state={
        open:false
    }

    showcontactuspage =(props)=>{
        console.log("button click.....");
        this.setState({
            open:!this.state.open
        })

    }

    closepage=()=>{
        this.setState({
            open:!this.state.open
        })
    }
    render() {
        return (
            <>
                <Contactuspage open={this.state.open} click={this.closepage}/>
                <section className="dv-MoreHelp dv-MoreHelp2">
                    <div className="container">
                        <h2 className="section-heading pb-0" data-aos="zoom-in-up" data-aos-duration="1000">Looking For Something More?</h2>
                        <p className="text-center font-weight-600 pb-5" data-aos="zoom-in-up" data-aos-duration="1000">Co-create your custom solution with us.</p>
                        <div className="card-deck" data-aos="fade-up" data-aos-duration="1000">
                            <div className="card">
                                <img src={require("assets/img/icons/gateway.png")} className="card-img-top" alt="Payment Gateway" />
                                <div className="card-body">
                                    <h5 className="card-title">Build your own payment gateway</h5>
                                    <p className="card-text">Our platform can be installed on
                                        your server or we can host a
                                        separate instance on our cloud
                                        infrastructure. We can manage the
                                        entire deployment and upgrade
                                        regularly with newer features.</p>
                                </div>
                                <div className="card-footer">
                                    <a className="more small-link-before" href="/white-label/payment-gateway">Learn more</a>
                                </div>
                            </div>
                            
                            <div className="card">
                                <img src={require("assets/img/icons/resell.png")} className="card-img-top" alt="Resell" />
                                <div className="card-body">
                                    <h5 className="card-title">Resell our services to your clients</h5>
                                    <p className="card-text">You can easily resell some or all of our services to your clients with your own custom pricing. Manage the entire process on our dashboard. You can rebrand our dashboards with your branding and provide it for your clients to manage their transactions and refunds.</p>
                                </div>
                                <div className="card-footer">
                                    <a className="more small-link-before" href="/partner/reseller">Learn more</a>
                                </div>
                            </div>
                            <div className="card">
                                <img src={require("assets/img/icons/mind.png")} className="card-img-top" alt="Help" />
                                <div className="card-body">
                                    <h5 className="card-title">Have something else in mind?</h5>
                                    <p className="card-text">Have a custom solution that you’relooking for? Tell us your requirements and we can co-create something. </p>
                                </div>
                               <div className="card-footer">
                                    <a className="more small-link-before" onClick={this.showcontactuspage}>Talk to us</a>
        </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}

export default MoreHelp;
