import React from "react";
import { Accordion, Button } from 'react-bootstrap';
import "assets/css/safexpay-web.css";
import "./FullyCustomised.css";

class FullyCustomised extends React.Component {

    constructor() {

        super();
        this.state = {

            imageURL: 'assets/img/icons/blank.png'

        }

    }

    dashboard_Image = () => {

        this.setState({

            imageURL: 'assets/img/icons/one.png'

        })
    }

    email_Image = () => {

        this.setState({

            imageURL: 'assets/img/icons/two.png'

        })
    }

    ckeckout_Image = () => {

        this.setState({

            imageURL: 'assets/img/icons/three.png'

        })
        document.getElementById('custom-img').setAttribute('src', this.imageURL);
    }



    render() {

        return (
            <>
                <section className="dv-FullyCustomised">
                    <div className="first-bg"></div>
                    <div className="second-bg"></div>
                    <div className="container-fluid pl-2" data-aos="fade-up" data-aos-duration="1000">
                        <div className="row">
                            <div className="col-md-5 offset-md-1 col-12">
                                <h2 className="section-heading mt-4 text-center text-md-left text-white">Fully Customised For Your Brand</h2>

                                <div className="accordion acc-FullyCustomised">
                                    <Accordion defaultActiveKey="0">
                                        <div
                                            className="acc-launcher"

                                        >
                                            <Accordion.Toggle as={Button} variant="link" eventKey="0" htmlFor="Dashboard" className="heading opened"
                                                onClick={() => this.setState({ imageURL: 'assets/img/icons/one.png' })}>
                                                <img
                                                    alt="Dashboard"
                                                    className="img-fluid"
                                                    src={require("assets/img/icons/dashboard.png")}
                                                />
                                Dashboard
                                </Accordion.Toggle>
                                            <Accordion.Collapse eventKey="0">
                                                <div className="description">
                                                Customize your dashboards and widgets for the best account overview. 
Every dashboard comes with  an Analytics. While the default Dashboard may suffice, the real usefulness of Dashboards lies in your ability to create and customize them the way you want.
                                        </div>
                                            </Accordion.Collapse>
                                        </div>

                                        <div className="acc-launcher"
                                            onClick={this.email_Image}
                                        >
                                            <Accordion.Toggle as={Button} variant="link" eventKey="1" htmlFor="EmailsSMSAlerts" className="heading">
                                                <img
                                                    alt="Emails/SMS Alerts"
                                                    className="img-fluid"
                                                    src={require("assets/img/icons/email.png")}
                                                /> Emails/SMS Alerts
                                </Accordion.Toggle>
                                            <Accordion.Collapse eventKey="1">
                                                <div className="description">
                                                Email and SMS notifications with pre-configured templates makes notifying your customers easy</div>
                                            </Accordion.Collapse>
                                        </div>
                                        <div className="acc-launcher">
                                            <Accordion.Toggle as={Button} variant="link" eventKey="2" htmlFor="Checkout Pages" className="heading"
                                                onClick={this.checkout_Image}>
                                                <img
                                                    alt="Checkout"
                                                    className="img-fluid"
                                                    src={require("assets/img/icons/checkout.png")}
                                                />Checkout Pages
                                </Accordion.Toggle>
                                            <Accordion.Collapse eventKey="2">
                                                <div className="description">
                                                The checkout process is one of the most important steps in the buyer journey. Optimize your checkout page with customized checkout fields based on your business needs and target audience and brand visual language. </div>
                                            </Accordion.Collapse>
                                        </div>
                                    </Accordion>
                                </div>
                            </div>
                            <div className="col-md-6 col-12 pr-md-0">
                                <div className="dv-img-show">
                                    <div className="img-gif">

                                    </div>
                                    <div className="img-dashboard">
                                        <img
                                            alt="dashboard"
                                            className="img-fluid sp-img"
                                            src={require("assets/img/banners/dashboard.gif")}
                                        />
                                    </div>
                                    <div className="img-emailssmsalerts d-none">
                                        <img
                                            alt="Email Smart Alerts"
                                            className="img-fluid sp-img"
                                            src={require("assets/img/banners/emailssmsalerts.gif")}
                                        />
                                    </div>
                                    <div className="img-checkoutpages d-none">
                                        <img
                                            alt="Checkout"
                                            className="img-fluid sp-img"
                                            src={require("assets/img/banners/checkoutpages.gif")}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}

export default FullyCustomised;
