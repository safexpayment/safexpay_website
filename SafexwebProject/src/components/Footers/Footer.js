import React from "react";
import {
    Modal
} from "reactstrap";
import "./Footer.css";

class Footer extends React.Component {
    state = {};
    toggleModal = state => {
        this.setState({
            [state]: !this.state[state]
        });
    };

    render() {
        return (
            <>
                <footer className="dv-Footer">
                    <div className="footer-left-bg"> </div>
                    <div className="footer-right-bg">
                        <div className="container footer-container">
                            <div className="row">
                                <div className="col-md-4 col-12 text-md-left text-center">
                                    <img 
                                        className="w-75 mb-3 footer-logo"
                                        alt="Safexpay"
                                        src={require("assets/img/logos/logo_wh.png")}
                                    />
                                    <p className="text-white mt-3 footer-text">
                                        Safexpay is a Mumbai-headquartered fintech focussed on solving problems for
                                        businesses who need to either accept online payments from or make payouts to
                                        various stakeholders. We also specialise in making our platform available to
                                        other merchant acquirers such as banks, financial institutions, fintechs &amp;
                                        international clients making us the leader in B2B2B white labelled payments.
                                        </p>
                                    <p className="copy mb-5">
                                        Copyright @ 2017-2021 Safexpay<br />
                                            All Rights Reserved
                                        </p>
                                </div>
                                <div className="col-md-8 col-12 row mx-0 pl-md-5 mb-3">
                                    
                                    <div className="col-sm-4 col-12 text-left">
                                        <h6 className="link-heading">PRODUCTS</h6>
                                        <h6 className="link-heading slink">For Businesses</h6>
                                        <a href="/business/accept-payments" className="link">Accept Payments</a>
                                        <a href="/business/make-payouts" className="link">Make Payouts</a>
                                        <a href="/business/digital-banking" className="link">Neo Bank</a>

                                        <h6 className="link-heading slink">White Label Platform</h6>
                                        <a href="/white-label/payment-gateway" className="link">Payment Gateway</a>
                                        <a href="/business/digital-banking#Platform" className="link">Payouts</a>

                                        <h6 className="link-heading slink">Partner Program</h6>
                                        <a href="/partner/reseller" className="link">Reseller</a>
                                    </div>
                                    <div className="col-sm-4 col-12 text-left">
                                        <h6 className="link-heading">COMPANY</h6>
                                        <a href="/company/about-us" className="link slink">About Us</a>                                        
                                        <a href="/career" className="link">Career</a>
                                        <a href="/company/privacy-policy" className="link">Privacy Policy</a>
                                        <a href="/company/terms-conditions" className="link">Terms &amp; Conditions</a>
                                        <h6 className="link-heading slink sc-txt">Security Certificates</h6>
                                        <div className="row col-12 mg-img-top">
                                        <img
                                            className="img-certificate ft-pdl0"
                                            onClick={() => this.toggleModal("pciModal")}
                                            alt="PCI Certificate"
                                            src={require("assets/img/logos/certificate-pci.png")}
                                        />
                                        <img
                                            className="img-certificate"
                                            onClick={() => this.toggleModal("dippModal")}
                                            alt="DIPP Gov"
                                            src={require("assets/img/logos/dipp-gov.png")}
                                        />
                                        </div>
                                        <div className="row col-12 mg-img-bot">
                                        <img
                                            className="img-certificate ft-pdl0"
                                            onClick={() => this.toggleModal("isoModal")}
                                            alt="ISO Certificate"
                                            src={require("assets/img/logos/certificate-iso.png")}
                                        />
                                        
                                        <img
                                            className="img-certificate dbst"
                                            
                                            alt="Dumbrad Street"
                                            src={require("assets/img/logos/dumbradstreet.png")}
                                        />
                                        
                                        </div>
                                    </div>
                                    <div className="col-sm-4 col-12">
                                        <h6 onClick={this.props.click} className="link-heading">CONTACT</h6>
                                        <a href="/supportus" className="link">Support</a>
                                        {/* <a href="/aboutus" className="link">Sales</a> */}

                                        <h6 className="link-heading slink">Social Connect</h6>
                                        <div className="social-wrap">
                                       <a href="https://twitter.com/safexpay?s=09" target="_blank"> <img
                                            alt="twitter"
                                            className="social-icon"
                                            src={require("assets/img/logos/twitter.png")}
                                        /></a>
                                        <a href="https://www.facebook.com/safexpay/" target="_blank"><img
                                            alt="facebook"
                                            className="social-icon"
                                            src={require("assets/img/logos/facebook.png")}
                                        /></a>
                                        <a href="https://instagram.com/thesafexpay?igshid=13ucm31yj4okk" target="_blank"><img
                                            alt="instagram"
                                            className="social-icon"
                                            src={require("assets/img/logos/instagram.png")}
                                        /></a>
                                        <a href="https://www.linkedin.com/company/safexpay" target="_blank"><img
                                            alt="linkedin"
                                            className="social-icon"
                                            src={require("assets/img/logos/linkedin.png")}
                                        /></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <Modal
                    className="modal-dialog-centered modal-dialog-scrollable modal-lg footerModal"
                    isOpen={this.state.pciModal}
                    toggle={() => this.toggleModal("pciModal")}
                ><div className="modal-header">
                        <h6 className="modal-title" id="modal-title-default">
                            PCI Certificate
                </h6>
                        <button
                            aria-label="Close"
                            className="close btn-close"
                            data-dismiss="modal"
                            type="button"
                            onClick={() => this.toggleModal("pciModal")}
                        >
                            <span aria-hidden={true}>×</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <img
                            alt="PCI Certificate"
                            className="img-fluid"
                            src={require("assets/img/logos/pci-full.png")}
                        />
                    </div>
                </Modal>

                <Modal
                    className="modal-dialog-centered modal-dialog-scrollable modal-lg footerModal"
                    isOpen={this.state.isoModal}
                    toggle={() => this.toggleModal("isoModal")}
                ><div className="modal-header">
                <h6 className="modal-title" id="modal-title-default">
                    PCI Certificate
        </h6>
                <button
                    aria-label="Close"
                    className="close btn-close"
                    data-dismiss="modal"
                    type="button"
                    onClick={() => this.toggleModal("isoModal")}
                >
                    <span aria-hidden={true}>×</span>
                </button>
            </div>
            <div className="modal-body">
                    <img
                        alt="ISO Certificate"
                        className="img-fluid"
                        src={require("assets/img/logos/iso-full.png")}
                    /></div>
                </Modal>

                <Modal
                    className="modal-dialog-centered modal-dialog-scrollable modal-lg footerModal"
                    isOpen={this.state.dippModal}
                    toggle={() => this.toggleModal("dippModal")}
                ><div className="modal-header">
                <h6 className="modal-title" id="modal-title-default">
                DPIIT Govt
        </h6>
                <button
                    aria-label="Close"
                    className="close btn-close"
                    data-dismiss="modal"
                    type="button"
                    onClick={() => this.toggleModal("dippModal")}
                >
                    <span aria-hidden={true}>×</span>
                </button>
            </div>
            <div className="modal-body">
                    
                    <img
                        alt="DIPP Govt"
                        className="img-fluid"
                        src={require("assets/img/logos/DIPP_Gov.png")}
                    />
                    </div>
                </Modal>

                <Modal
                    className="modal-dialog-centered modal-dialog-scrollable modal-lg footerModal"
                    isOpen={this.state.dsModal}
                    toggle={() => this.toggleModal("dsModal")}
                ><div className="modal-header">
                <h6 className="modal-title" id="modal-title-default">
                    Dun BradStreet
        </h6>
                <button
                    aria-label="Close"
                    className="close btn-close"
                    data-dismiss="modal"
                    type="button"
                    onClick={() => this.toggleModal("dsModal")}
                >
                    <span aria-hidden={true}>×</span>
                </button>
            </div>
            <div className="modal-body">
                    
                
                    </div>
                </Modal>
                
                </footer>

            </>
        );
    }
}

export default Footer;
