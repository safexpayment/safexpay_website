import React from "react";
import "assets/css/safexpay-web.css"
import "./Mission.css";

class Mission extends React.Component {
    render() {
        return (
            <>
                <section className="dv-Mission">
                    <div className="first-bg"></div>
                    <div className="container">
                        <h2 className="section-heading text-center mb-0 pb-0" data-aos="fade-up" data-aos-duration="1000">
                            Our Mission
                        </h2>
                        <h4 className="text-center pb-5 accText" data-aos="fade-up" data-aos-duration="1000">
                            Accelerate Economic growth of digital Platforms &amp; API  for every business.<br /><br /><br />
                        </h4>
                        <div className="row pt-5 pd-tp-0">
                            <div className="col-md-3 col-12 text-center text-md-left" data-aos="fade-up" data-aos-duration="1000">
                                <img src={require("assets/img/icons/safexpay.png")} className="card-img-top img-fluid" alt="" />
                                <h2 className="section-heading text-center text-md-left text-white ubuntuMedium">Safexpay Story</h2>
                            </div>
                            <div className="col-md-9 col-12" data-aos="fade-up" data-aos-duration="1000">
                                <p className="px-3 px-md-0 text-center text-md-left">
                                    We started our journey by creating payment products that allow businesses to
                                    accept payments digitally. Over the time, we realised that we will never be
                                    able to help all the businesses we want to help as it was impossible for us to
                                    reach them all. In 2018, this realisation led to us creating a white-labelled payment
                                    platform which could be used by other companies wanting to sell similar
                                    services as us across the World.
                                    We are now the leader in this space and multiple
                                    companies across the world use our platform. By 2019, we also wanted to
                                    help companies to move money to their stakeholders
                                    be it vendors, employees
                                    or customers, we launched our payout product helping businesses do this
                                    transaction in milliseconds. At the turn of the new decade, we are excited
                                    by the digital future that lies ahead and the possibilities it creates for us. Our
                                    team is working hard and will be launching more products &amp; services
                                    to keep the wheels of the economy moving by helping businesses ﬂourish.
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}
export default Mission;
