import React from "react";
import "assets/css/safexpay-web.css"
import "./ToolKit.css";

class ToolKit extends React.Component {

    render() {
        return (
            <>
                <section className="dv-ToolKit">
                    <div className="container">

                        <h2 className="section-heading" data-aos="fade-up" data-aos-duration="1000">Complete Card &amp; Wallet Tool Kit.</h2>
                        <div className="row" data-aos="fade-up" data-aos-duration="1000">
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/card.png")} className="card-img-top" alt="Physical or Virtual Card" />
                                    <div className="card-body">
                                        <p className="card-text">Physical Card Or Virtual Card.<br /><br /> </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/brand.png")} className="card-img-top" alt="Brand" />
                                    <div className="card-body">
                                        <p className="card-text">Co-branded With Your Brand &amp; Partner Bank.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/atm.png")} className="card-img-top" alt="ATM" />
                                    <div className="card-body">
                                        <p className="card-text">Withdraw From ATM. Spend Anywhere Online Or Oﬄine. </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/loading.png")} className="card-img-top" alt="Loading" />
                                    <div className="card-body">
                                        <p className="card-text">Easy Loading / Reloading Options : Online &amp; Oﬄine </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/rewards.png")} className="card-img-top" alt="Rewards" />
                                    <div className="card-body">
                                        <p className="card-text">Push Rewards Or Salaries Directly Into The Card.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="card">
                                    <img src={require("assets/img/icons/balance-2.png")} className="card-img-top" alt="Balance" />
                                    <div className="card-body">
                                        <p className="card-text">Balance, Passbook, Card Lock, Reset Pin.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>

            </>
        );
    }
}

export default ToolKit;
