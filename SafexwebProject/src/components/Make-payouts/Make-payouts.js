import React from "react";
import "assets/css/safexpay-web.css"
import "./Make-payouts.css";

class Makepayouts extends React.Component {
    render() {
        return (
            <>
                <section className="dv-Make-payouts pt-5 pb-5 mb-5" data-aos="fade-up" data-aos-duration="1000">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 rep-text-center">
                                <img src={require("assets/img/icons/make-payout.png")} className="img-make-payout" alt="" />
                                <h2 className="section-heading text-left">
                                    Make payouts your way.
                                </h2>
                                <p className="desc">Choose from diﬀerent types of payout options to transfer money as per your choice. </p>
                            </div>
                            <div className="col-md-6 dv-points-holder pt-5">
                                <div className="dv-points">
                                    <h3 className="heading">
                                        Single Payments
                                    </h3>
                                    <p className="desc">
                                        Option to add beneﬁciary and process the payments instantly using NEFT/RTGS /UPI.
                                    </p>
                                </div>
                                <div className="dv-points">
                                    <h3 className="heading">
                                        Bulk Payments
                                    </h3>
                                    <p className="desc">
                                        Option to bulk upload  payout in Excel Format and payout
                                        will be processed to user account instantly.
                                    </p>
                                </div>
                                <div className="dv-points">
                                    <h3 className="heading">
                                        Scheduled  Payments
                                    </h3>
                                    <p className="desc">
                                        Business can schedule the payments to avoid missing out
                                        on due dates for utility bills or to schedule a vendor
                                        payment for a later date.  
                                    </p>
                                </div>
                                <div className="dv-points">
                                    <h3 className="heading">
                                        Queuing Payment
                                    </h3>
                                    <p className="desc">
                                        No worries, If balance is low in your payout account, Still
                                        you can initiate the payout request, payout will be in
                                        queue till suﬃcient balance is available once suﬃcient
                                        balance is available it will be processed automatically.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
}
export default Makepayouts;
