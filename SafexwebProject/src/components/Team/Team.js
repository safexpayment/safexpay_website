import React from "react";
import "assets/css/safexpay-web.css"
import "./Team.css";

class Team extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Team">
                    <div className="container">

                        <h2 className="section-heading text-center pb-0" data-aos="zoom-in-up" data-aos-duration="1000">Built For Your Entire Team</h2>
                        <p className="text-center" data-aos="zoom-in-up" data-aos-duration="1000">Use our dashboards, merchant app and developer guides to learn about and grow your cash flows.</p>

                        <img src={require("assets/img/section/dashboard.png")} className="card-img-top img-fluid my-5" alt="" data-aos="fade-up" data-aos-duration="1000" />

                        <div className="card-deck">
                            <div className="card" data-aos="fade-up" data-aos-duration="1000">
                                <img src={require("assets/img/icons/finance.png")} className="card-img-top" alt="" />
                                <div className="card-body">
                                    <h5 className="card-title">Finance and Business <br /> &nbsp;
                                    </h5>
                                    <p className="card-text">Allyour revenue in one place. See how your business is growing. Download all yoour settlement details. All the fees deducted for processing are explained clearly.</p>
                                </div>
                            </div>
                            <div className="card" data-aos="fade-up" data-aos-duration="1000">
                                <img src={require("assets/img/icons/customer-service.png")} className="card-img-top" alt="" />
                                <div className="card-body">
                                    <h5 className="card-title">Operations &amp;<br />
Customer Service</h5>
                                    <p className="card-text">See real time transaction information to manage refunds or customer complaints. Send a link to someone who dropped out of your checkout to complete their transaction.</p>
                                </div>
                            </div>
                            <div className="card" data-aos="fade-up" data-aos-duration="1000">
                                <img src={require("assets/img/icons/engineer.png")} className="card-img-top" alt="" />
                                <div className="card-body">
                                    <h5 className="card-title">Product &amp; Engineering <br /> &nbsp;</h5>
                                    <p className="card-text">Manage the security of your account. Access your encryption keys. Read our detailed integration guides to see what more you can create for your customers. See how various payment modes are behaving.</p>
                                </div>
                            </div>
                        </div>


                    </div>
                </section>

            </>
        );
    }
}

export default Team;
