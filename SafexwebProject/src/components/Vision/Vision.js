import React from "react";
import "assets/css/safexpay-web.css"
import "./Vision.css";

class Vision extends React.Component {

    render() {
        return (
            <>
                <section className="dv-Vision">
                    <div className="container">
                        <h2 className="section-heading text-center pb-1" data-aos="fade-up" data-aos-duration="1000">
                            Our Vision
                        </h2>
                        <div className="row">
                            <div className="col-md-12 pl-md-5 pr-md-5" data-aos="fade-up" data-aos-duration="1000">
                                <h4 className="text-center pl-md-5 pr-md-5">
                                    To build digital platform infrastructure that will
                                    transform the payment and banking needs for every
                                    business across the globe.
                                </h4>
                            </div>
                        </div>
                    </div>
                </section>

            </>
        );
    }
}

export default Vision;
