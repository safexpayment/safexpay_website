import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";

// core components
import Navbar from "components/Navbars/Navbar.js";
import Footer from "components/Footers/Footer.js";
import HeroBanner from "components/HeroBanner/HeroBanner.js";
import Features from "components/Features/Features.js";
import Advantage from "components/Advantage/Advantage.js";
import Launcher from "components/Launcher/Launcher.js";
//import Team from "components/Team/Team.js";
import Quality from "components/Quality/Quality.js";
import Clients from "components/Clients/Clients.js";
import Help from "components/Help/Help.js";
import  Helmet  from "react-helmet";

//import $ from "jquery";

class Index extends React.Component {
    componentDidMount() {
        // document.documentElement.scrollTop = 0;
        // document.scrollingElement.scrollTop = 0;
        // this.refs.main.scrollTop = 0;
        // const wow = new WOW();
        // wow.init();
    }

    freshChatClick = () => {
        // if (window.fcWidget.isOpen() !== true) {
        //     window.fcWidget.open();
        // }
    };

    

    render() {
        return (
            <>
                <Helmet>
                    <meta name="title" content="SafexPay - Best Payment Gateway"/>
                    <meta name="description" content="SafexPay simplifies your payments, banking needs and grows your business. Be the bank for your customers or employees."/>
                    <meta name="keywords" content="payment gateway,best payment gateway india,online payment,accept payments,digital payment,top payment gateway,e-business"/>

                    <meta property="og:title" content="SafexPay - Simplify Your Payments"/>
                    <meta property="og:url" content="https://www.safexpay.com/"/>
                    <meta property="og:description" content="SafexPay simplifies your payments, banking needs and grows your business. Be the bank for your customers or employees." />
                    <meta property="og:image" itemprop="image" content="https://www.safexpay.com/static/media/logo.0c7d81d8.png"/>
                    <meta property="og:type" content="website" />
                    
                    <link rel="shortcut icon" type="image/x-icon" href="../src/assets/img/others/favicon.png"/>
                    <link rel="apple-touch-icon" href="../src/assets/img/others/favicon.png"/>

                    <title>SafexPay - Simplify Your Payments</title>
                </Helmet>
                <Navbar/>
                <main ref="main" className="dv-main">
                    <HeroBanner />
                    <Features />
                    <Advantage />
                   <Launcher />
                    {/* <Team /> */}
                    <Quality />
                    <Clients />
                    <Help />
                  </main>
                <Footer/>
            </>
        );
    }
}

export default Index;
