import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";

// core components
import Navbar from "components/Navbars/Navbar-reg.js";
import AgreementDoc from "components/Registration/Agreement.js";

class Agreement extends React.Component {
    componentDidMount() {
        // document.documentElement.scrollTop = 0;
        // document.scrollingElement.scrollTop = 0;
        // this.refs.main.scrollTop = 0;
        // const wow = new WOW();
        // wow.init();
    }

    freshChatClick = () => {
        // if (window.fcWidget.isOpen() !== true) {
        //     window.fcWidget.open();
        // }
    };

    render() {
        return (
            <>
                <div className="dv-personaldoc">
                    <Navbar />
                    <main ref="main" className="dv-main dv-personaldoc-container">                        
                    <div className="first-bg htg"></div>
                        <AgreementDoc />
                    </main>
                </div>
            </>
        );
    }
}

export default Agreement;