import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";

// core components
import Navbar from "components/Navbars/Navbar.js";
import Footer from "components/Footers/Footer.js";
import HeroBanner from "components/HeroBanner/Career.js";
import ApplyFormJdnew from "components/ApplyFromJdnew/ApplyFormJdnew";


import {NavLink} from "reactstrap";

class BusinessDevelopment extends React.Component {
    
    render() {
        return (
            <>
                <Navbar />
                <main ref="main" className="dv-main">
                    <HeroBanner />
                </main>
                <div className="container mt-5">
                    <div className="row">
                        {/* <div className="col-12 text-right">
                            <a href="/career" className="btn-back">Back</a>
                        </div> */}
                        <div className="col-12">
                            <h2 className="font-w7 ubuntuMono op-txt">Open Position</h2>
                            <a href="/career" className="btn-back">Back</a>
                            <div className="clearfix"></div>
                            <h3 className="font26 border-bottom py-3 mb-3">
                                At safexpay we aspire to build a global,  digital platform
                                infrastructure and product company focused for every business,
                                with an extraordinary culture that attracts innovative,
                                collaborative people.
                              </h3>
                            <p>
                                <strong className="font-w6">
                                    <span className="color-blue mrg-10">Business Development</span>
                                    Location: Mumbai / Gurgaon / Bangalore
 <br />
 Experience: 0+years (fresher) 
</strong> <br /><br />
<strong className="font-w6"><span className="color-blue mrg-10">Job description:</span></strong>
 <ul className="career-points">
     <li> Following the sales process to acquire new customers/Associates
</li>
     <li> Capturing meeting minutes and sending them to customer and Sales Head internally
</li>
     <li> Maintaining relationships with customers to ensure we understand their problems and reassure
them of constant support.
</li>
     <li> Be available for the customer whenever required
</li>
     <li> Follow the sales philosophy of the organization
</li>
     <li> Regularly performing the sales discipline processes of reports/report formats expected from time
to time</li>
 </ul><br />
<strong className="font-w6"><span className="color-blue mrg-10">Key Responsibilities:
</span></strong>
 <ul className="career-points">
     <li> Acquire new customers/Associates
</li>
     <li> Sell new products to new/existing Merchants
</li>
     <li> Maintaining relationship with Associates to get new business
</li>
     <li> Achieving revenue targets month on month</li>
 </ul><br />

 <strong className="font-w6"><span className="color-blue mrg-10">Core competencies:</span></strong>

 <ul className="career-points">
     <li> Implementing sales plan for achievement of organizational goal in terms of revenue and
proﬁtability targets
</li> <li>Generating business in new segments/markets/products in the allocated regions</li>
     <li> Should independently handle end to end sales</li>
 </ul><br />
 <strong className="font-w6"><span className="color-blue mrg-10">Interpersonal Skill:</span></strong>
 <ul className="career-points">
     <li> Excellent customer management skills
</li>
     <li> Excellent communication skill
</li>
     <li> Self-motivated leader
</li>
     <li> Positive attitude
</li>
     <li> Team player
</li>
     <li> Excellent presentation skill
</li>
     <li> Go getter</li>
 </ul>
                              </p>
                            
                                       
                        </div>
                    </div>
                </div>
                <ApplyFormJdnew pos="Business Development" loc="Mumbai / Gurgaon / Bangalore" exp="0 yrs (fresher)"/>
                <Footer />
            </>
        );
    }
}

export default BusinessDevelopment;
