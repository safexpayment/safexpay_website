import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";

// core components
import Navbar from "components/Navbars/Navbar.js";
import Footer from "components/Footers/Footer.js";
import HeroBanner from "components/HeroBanner/Policy.js";
import Policy from "components/Policy/Policy.js";
import  Helmet  from "react-helmet";
class PrivacyPolicy extends React.Component {
    componentDidMount() {
        // document.documentElement.scrollTop = 0;
        // document.scrollingElement.scrollTop = 0;
        // this.refs.main.scrollTop = 0;
        // const wow = new WOW();
        // wow.init();
    }

    freshChatClick = () => {
        // if (window.fcWidget.isOpen() !== true) {
        //     window.fcWidget.open();
        // }
    };

    render() {
        return (
            <>
                <Helmet>
                    <meta name="title" content="SafexPay - Privacy Policy"/>
                    <meta name="description" content="Payment gateway solution for India with robust, developer friendly API and simple economic pricing."/>
                    <meta name="keywords" content="safexpay customer care number,payment portals,e-wallets,payment gateway service,best payment gateway in india,payment gateway charges,payment processing"/>

                    <meta property="og:title" content="SafexPay - Instant Payouts"/>
                    <meta property="og:url" content="https://www.safexpay.com/company/privacy-policy"/>
                    <meta property="og:description" content="Payment gateway solution for India with robust, developer friendly API and simple economic pricing." />
                    <meta property="og:image" itemprop="image" content="https://www.safexpay.com/static/media/logo.0c7d81d8.png"/>
                    <meta property="og:type" content="website" />
                    
                    <link rel="shortcut icon" type="image/x-icon" href="../src/assets/img/others/favicon.png"/>
                    <link rel="apple-touch-icon" href="../src/assets/img/others/favicon.png"/>

                    <link rel="canonical" href="https://www.safexpay.com/company/privacy-policy" />

                    <title>Privacy Policy - SafexPay</title>
                </Helmet>
                <Navbar/>
                <main ref="main" className="dv-main">
                    <HeroBanner />
                    <Policy />
                  </main>
                <Footer/>
            </>
        );
    }
}

export default PrivacyPolicy;
