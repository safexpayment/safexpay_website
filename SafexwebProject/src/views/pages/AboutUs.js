import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";
import  Helmet  from "react-helmet";
// core components
import Navbar from "components/Navbars/Navbar.js";
import Footer from "components/Footers/Footer.js";
import Who from "components/Who/Who.js"; 
import Vision from "components/Vision/Vision.js";
import Mission from "components/Mission/Mission.js";
import Newsroom from "components/Newsroom/Newsroom.js";
import Location from "components/Location/Location.js";

class AboutUs extends React.Component {
    componentDidMount() {
        // document.documentElement.scrollTop = 0;
        // document.scrollingElement.scrollTop = 0;
        // this.refs.main.scrollTop = 0;
        // const wow = new WOW();
        // wow.init();
    }

    freshChatClick = () => {
        // if (window.fcWidget.isOpen() !== true) {
        //     window.fcWidget.open();
        // }
    };

    render() {
        return (
            <>
                <Helmet>
                    <meta name="title" content="SafexPay - Best Digital Payment Platform"/>
                    <meta name="description" content="SafexPay developed a digital platform infrastructure that will transform the payment and banking needs for every business across the globe."/>
                    <meta name="keywords" content="payment gateway integration,digital payment,payment gateway solutions,pricing,payment gateway pricing,e-wallets"/>
 
                    <meta property="og:title" content="SafexPay - Best Digital Payment Platform"/>
                    <meta property="og:url" content="https://www.safexpay.com/company/about-us"/>
                    <meta property="og:description" content="SafexPay developed a digital platform infrastructure that will transform the payment and banking needs for every business across the globe." />
                    <meta property="og:image" itemprop="image" content="https://www.safexpay.com/static/media/logo.0c7d81d8.png"/>
                    <meta property="og:type" content="website" />
                    
                    <link rel="shortcut icon" type="image/x-icon" href="../src/assets/img/others/favicon.png"/>
                    <link rel="apple-touch-icon" href="../src/assets/img/others/favicon.png"/>

                    <link rel="canonical" href="https://www.safexpay.com/company/about-us" />
 
                    <title>SafexPay - Best Digital Payment Platform</title>
                </Helmet>
                <Navbar/>
                <main ref="main" className="dv-main">
                    <Who />
                    <Vision />
                    <Mission />
                    <Newsroom />
                    <Location />
                  </main>
                <Footer/>
            </>
        );
    }
}

export default AboutUs;
