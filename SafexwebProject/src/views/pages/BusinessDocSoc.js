import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";

// core components
import Navbar from "components/Navbars/Navbar-reg.js";
import BusinessDocC from "components/Registration/BusinessDoc.js";

class BusinessDoc extends React.Component {
    componentDidMount() {
        // document.documentElement.scrollTop = 0;
        // document.scrollingElement.scrollTop = 0;
        // this.refs.main.scrollTop = 0;
        // const wow = new WOW();
        // wow.init();
    }

    freshChatClick = () => {
        // if (window.fcWidget.isOpen() !== true) {
        //     window.fcWidget.open();
        // }
    };

    render() {
        return (
            <>
                <div className="dv-businessdoc">
                    <Navbar />
                    <main ref="main" className="dv-main dv-businessdoc-container dv-businessdocsoc-container">                        
                    <div className="first-bg"></div>
                        <BusinessDocC name="Trust/ Society" />
                    </main>
                </div>
            </>
        );
    }
}

export default BusinessDoc;