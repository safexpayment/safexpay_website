import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";

// core components
import Navbar from "components/Navbars/Navbar.js";
import Footer from "components/Footers/Footer.js";
import HeroBanner from "components/HeroBanner/Reseller.js";
import FullToolKit from "components/FullToolKit/FullToolKit.js";
import FullyCustomised from "components/FullyCustomised/FullyCustomised.js";
import MoreHelpreseller from "components/MoreHelp-reseller/MoreHelp-reseller.js";
import Helmet from "react-helmet";
import Help from "components/Help/Help.js";

class Reseller extends React.Component {
    componentDidMount() {
        // document.documentElement.scrollTop = 0;
        // document.scrollingElement.scrollTop = 0;
        // this.refs.main.scrollTop = 0;
        // const wow = new WOW();
        // wow.init();
    }

    freshChatClick = () => {
        // if (window.fcWidget.isOpen() !== true) {
        //     window.fcWidget.open();
        // }
    };

    render() {
        return (
            <>
                <Helmet>
                    <meta name="title" content="SafexPay - Manage Reseller"/>
                    <meta name="description" content="Use our payment gateway platform to grow your digital business. Neo Banking. Fully customized for your business."/>
                    <meta name="keywords" content="payment gateway,payment gateway solution,payment gateway integration india,online payment gateway,online payment gateway service provider"/>
 
                    <meta property="og:title" content="SafexPay - Manage Reseller"/>
                    <meta property="og:url" content="https://www.safexpay.com/partner/reseller"/>
                    <meta property="og:description" content="Use our payment gateway platform to grow your digital business. Neo Banking. Fully customized for your business." />
                    <meta property="og:image" itemprop="image" content="https://www.safexpay.com/static/media/logo.0c7d81d8.png"/>
                    <meta property="og:type" content="website" />
                    
                    <link rel="shortcut icon" type="image/x-icon" href="../src/assets/img/others/favicon.png"/>
                    <link rel="apple-touch-icon" href="../src/assets/img/others/favicon.png"/>

                    <link rel="canonical" href="https://www.safexpay.com/partner/reseller" />
 
                    <title>SafexPay - Manage Reseller</title>
                </Helmet>
                <Navbar/>
                <main ref="main" className="dv-main">
                    <HeroBanner />
                    <FullToolKit />
                    <FullyCustomised />
                    <MoreHelpreseller />
                    <Help />
                  </main>
                <Footer/>
            </>
        );
    }
}

export default Reseller;
