import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";

// core components
import Navbar from "components/Navbars/Navbar-reg.js";
import TempLogin from "components/Registration/TempLogin.js";

class Activation extends React.Component {
    componentDidMount() {
        // document.documentElement.scrollTop = 0;
        // document.scrollingElement.scrollTop = 0;
        // this.refs.main.scrollTop = 0;
        // const wow = new WOW();
        // wow.init();
    }

    freshChatClick = () => {
        // if (window.fcWidget.isOpen() !== true) {
        //     window.fcWidget.open();
        // }
    };

    render() {
        return (
            <>
                <div className="dv-main-Activation">
                    <Navbar />
                    <main ref="main" className="dv-main dv-login-container">
                    <div className="first-bg"></div>
                        <TempLogin />
                    </main>
                </div>
            </>
        );
    }
}

export default Activation;