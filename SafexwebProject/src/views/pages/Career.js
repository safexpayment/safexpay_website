import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";
import  Helmet  from "react-helmet";
// core components
import Navbar from "components/Navbars/Navbar.js";
import Footer from "components/Footers/Footer.js";
import HeroBanner from "components/HeroBanner/Career.js";
import AllOpenings from "components/Openings/AllOpenings.js";

class Career extends React.Component {
    componentDidMount() {
        // document.documentElement.scrollTop = 0;
        // document.scrollingElement.scrollTop = 0;
        // this.refs.main.scrollTop = 0;
        // const wow = new WOW();
        // wow.init();
    }

    freshChatClick = () => {
        // if (window.fcWidget.isOpen() !== true) {
        //     window.fcWidget.open();
        // }
    };

    render() {
        return (
            <>
                <Helmet>
                    <meta name="title" content="SafexPay Careers - Explore the Jobs"/>
                    <meta name="description" content="At SafexPay we embrace innovation and are always on the lookout for people who can bring in new perspectives, innovative ideas and answers to questions we don’t even have yet!"/>
                    <meta name="keywords" content="payment gateway integration,digital payment,payment gateway solutions,pricing,payment gateway pricing,e-wallets"/>
 
                    <meta property="og:title" content="SafexPay Careers - Explore the Jobs"/>
                    <meta property="og:url" content="https://www.safexpay.com/career"/>
                    <meta property="og:description" content="At SafexPay we embrace innovation and are always on the lookout for people who can bring in new perspectives, innovative ideas and answers to questions we don’t even have yet!" />
                    <meta property="og:image" itemprop="image" content="https://www.safexpay.com/static/media/logo.0c7d81d8.png"/>
                    <meta property="og:type" content="website" />
                    
                    <link rel="shortcut icon" type="image/x-icon" href="../src/assets/img/others/favicon.png"/>
                    <link rel="apple-touch-icon" href="../src/assets/img/others/favicon.png"/>
                    <link rel="canonical" href="https://www.safexpay.com/career"/>
                    <title>SafexPay Careers - Explore the Jobs</title>
                </Helmet>

                <Navbar/>
                <main ref="main" className="dv-main">
                    <HeroBanner />
                  </main>
                  <AllOpenings />
                <Footer/>
            </>
        );
    }
}

export default Career;
