import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";

// core components
import Navbar from "components/Navbars/Navbar.js";
import Footer from "components/Footers/Footer.js";
import HeroBanner from "components/HeroBanner/Career.js";
import ApplyFormJdnew from "components/ApplyFromJdnew/ApplyFormJdnew";


import {NavLink} from "reactstrap";

class IOSDeveloper extends React.Component {
    
    render() {
        return (
            <>
                <Navbar />
                <main ref="main" className="dv-main">
                    <HeroBanner />
                </main>
                <div className="container mt-5">
                    <div className="row">
                        {/* <div className="col-12 text-right">
                            <a href="/career" className="btn-back">Back</a>
                        </div> */}
                        <div className="col-12">
                            <h2 className="font-w7 ubuntuMono op-txt">Open Position</h2>
                            <a href="/career" className="btn-back">Back</a>
                            <div className="clearfix"></div>
                            <h3 className="font26 border-bottom py-3 mb-3">
                                At safexpay we aspire to build a global,  digital platform
                                infrastructure and product company focused for every business,
                                with an extraordinary culture that attracts innovative,
                                collaborative people.
                              </h3>
                            <p>
                                <strong className="font-w6">
                                    <span className="color-blue mrg-10">IOS developer</span>
                                    Location: Mumbai / Gurgaon / Bangalore
 <br />
 Experience: 2+ years 
</strong> <br /><br />
<strong className="font-w6"><span className="color-blue mrg-10">Job description:</span></strong>
 <ul className="career-points">
     <li>We are looking for an iOS developer responsible for the development and maintenance of
applications aimed at a range of iOS devices including mobile phones and tablet computers
</li>
     <li>Your primary focus will be development of iOS applications and their integration with back-
end services
</li>
     <li>You will be working alongside other engineers and developers working on
different layers of the infrastructure
</li>
     <li>Commitment to collaborative problem
solving, sophisticated design, and the creation of quality products is essential
</li>
 </ul><br />
<strong className="font-w6"><span className="color-blue mrg-10">Key Responsibilities:
</span></strong>
 <ul className="career-points">
     <li> Design and build advanced applications for the IOS platform
</li>
     <li>Collaborate with cross-functional teams to define, design, and ship new features
</li>
     <li>Work with outside data sources and APIs
</li>
     <li>Unit-test code for robustness, including edge cases, usability, and general reliability</li>
     <li>Work on bug fixing and improving application performance</li>
     <li>Continuously discover, evaluate, and implement new technologies to maximize
development efficiency</li>
 </ul><br />
 
 <strong className="font-w6"><span className="color-blue mrg-10">Requirements:</span></strong>
 <ul className="career-points">
     <li> BE/MCA degree in Computer Science, Engineering or a related subject
</li>
     <li> Proficient with Objective-C or Swift
</li>
     <li> Experience with iOS frameworks such as Core Data, Core Animation, etc
</li>
     <li> Experience with offline storage, threading, and performance tuning
</li>
     <li> Familiarity with RESTful APIs to connect iOS applications to back-end services
</li>
     <li> Knowledge of other web technologies and UI/UX standards
</li>
     <li> Understanding of Apple’s design principles and interface guidelines</li>
     <li> Knowledge of low-level C-based libraries is preferred</li>
     <li> Experience with performance and memory tuning with tools</li>
     <li> Familiarity with cloud message APIs and push notifications</li>
     <li> Knack for benchmarking and optimization</li>
     <li> Proficient understanding of code versioning tools</li>
     <li> Familiarity with continuous integration</li>
     <li> Solid understanding of the full mobile development life cycle</li>
 </ul>
                              </p>          
                        </div>
                    </div>
                </div>
                <ApplyFormJdnew pos="IOS developer" loc="Mumbai / Gurgaon / Bangalore" exp="2+yrs"/>
                <Footer />
            </>
        );
    }
}

export default IOSDeveloper;
