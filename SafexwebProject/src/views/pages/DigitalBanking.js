import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";

// core components
import Navbar from "components/Navbars/Navbar.js";
import Footer from "components/Footers/Footer.js";
import HeroBanner from "components/HeroBanner/DigitalBanking.js";
import Platform from "components/Platform/Platform.js";
import BankingServices from "components/BankingServices/BankingServices.js";
import ToolKit from "components/ToolKit/ToolKit.js";
import ValueAdded from "components/ValueAdded/ValueAdded.js";
import FullControl from "components/FullControl/FullControl.js";
import Helmet from "react-helmet";
import Help from "components/Help/Help.js";

class MakePayments extends React.Component {
    componentDidMount() {
        // document.documentElement.scrollTop = 0;
        // document.scrollingElement.scrollTop = 0;
        // this.refs.main.scrollTop = 0;
        // const wow = new WOW();
        // wow.init();
    }

    freshChatClick = () => {
        // if (window.fcWidget.isOpen() !== true) {
        //     window.fcWidget.open();
        // }
    };

    render() {
        return (
            <>
                <Helmet>
                    <meta name="title" content="SafexPay - Digital Banking"/>
                    <meta name="description" content="Digtal Banking through our SafexPay payment gateway. Get a full toolkit with our APIs or directly reusing our dashboards to create these experiences."/>
                    <meta name="keywords" content="payment portals,e-wallets,payment gateway service,advanced analytics,recurring payments,digital payment,mobile wallets,payment system for ecommerce"/>
 
                    <meta property="og:title" content="SafexPay - Secure Digital Banking"/>
                    <meta property="og:url" content="https://www.safexpay.com/business/digital-banking"/>
                    <meta property="og:description" content="Digtal Banking through our SafexPay payment gateway. Get a full toolkit with our APIs or directly reusing our dashboards to create these experiences." />
                    <meta property="og:image" itemprop="image" content="https://www.safexpay.com/static/media/logo.0c7d81d8.png"/>
                    <meta property="og:type" content="website" />
                    
                    <link rel="shortcut icon" type="image/x-icon" href="../src/assets/img/others/favicon.png"/>
                    <link rel="apple-touch-icon" href="../src/assets/img/others/favicon.png"/>

                    <link rel="canonical" href="https://www.safexpay.com/business/digital-banking" />
 
                    <title>SafexPay - Secure Digital Banking</title>
                </Helmet>
                <Navbar/>
                <main ref="main" className="dv-main">
                    <HeroBanner />
                    <Platform />
                    <BankingServices />
                    <ToolKit />
                    <ValueAdded />
                    <FullControl />
                    <Help />
                  </main>
                <Footer/>
            </>
        );
    }
}

export default MakePayments;
