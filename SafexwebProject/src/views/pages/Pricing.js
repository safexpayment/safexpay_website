import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";

// core components
import Navbar from "components/Navbars/Navbar.js";
import Banner from "components/HeroBanner/Pricing.js";
import Footer from "components/Footers/Footer.js";
import StartupPricing from "components/StartupPricing/StartupPricing.js";
import PaymentSolutions from "components/PaymentSolutions/PaymentSolutions.js";
import PricingS from "components/Pricing/Pricing.js";
import UsedCases from "components/UsedCases/UsedCases.js";
import  Helmet  from "react-helmet";
import Help from "components/Help/Help.js";

import Contactuspage from "components/Contactuspage/Contactuspage";

class Pricing extends React.Component {
    componentDidMount() {
        // document.documentElement.scrollTop = 0;
        // document.scrollingElement.scrollTop = 0;
        // this.refs.main.scrollTop = 0;
        // const wow = new WOW();
        // wow.init();
    }

    freshChatClick = () => {
        // if (window.fcWidget.isOpen() !== true) {
        //     window.fcWidget.open();
        // }
    };
    state={
        open:false
    }

    showcontactuspage =(props)=>{
        console.log("button click.....");
        this.setState({
            open:!this.state.open
        })

    }

    closepage=()=>{
        this.setState({
            open:!this.state.open
        })
    }

    render() {
        return (
            <>
                <Helmet>
                    <meta name="title" content="SafexPay - Start-up Pricing"/>
                    <meta name="description" content="SafexPay provides you one price for any of our payment solutions. Start-up pricing for all the payment methods."/>
                    <meta name="keywords" content="payment gateway integration,digital payment,payment gateway solutions,pricing,payment gateway pricing,e-wallets"/>
 
                    <meta property="og:title" content="SafexPay - Start-up Pricing"/>
                    <meta property="og:url" content="https://www.safexpay.com/pricing"/>
                    <meta property="og:description" content="SafexPay provides you one price for any of our payment solutions. Start-up pricing for all the payment methods." />
                    <meta property="og:image" itemprop="image" content="https://www.safexpay.com/static/media/logo.0c7d81d8.png"/>
                    <meta property="og:type" content="website" />
                    
                    <link rel="shortcut icon" type="image/x-icon" href="../src/assets/img/others/favicon.png"/>
                    <link rel="apple-touch-icon" href="../src/assets/img/others/favicon.png"/>

                    <link rel="canonical" href="https://www.safexpay.com/pricing" />
 
                    <title>SafexPay - Start-up Pricing</title>
                </Helmet>

                <Contactuspage open={this.state.open} click={this.closepage}/>
                <Navbar/>
                <main ref="main" className="dv-main">
                    <Banner />
                    <StartupPricing />                    
                    <PaymentSolutions />
                    <PricingS />
                    <div className="container">
                        <div className="row" data-aos="zoom-in-up" data-aos-duration="1000">
                            <div className="col-md-9">
                                <h2 className="section-heading rep-text-center text-left">Looking For Customized Pricing Or International Payments?</h2>                        
                            </div>
                            <div className="col-md-3 pt-2">
                                <div className="btnDiv rep-text-center">
                                    <button className="btn btn-blue-rounded  mt-5  mg-tp0" onClick={this.showcontactuspage}>Talk To Us</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <UsedCases />
                    <div className="container">
                        <div className="row mx-0" data-aos="zoom-in-up" data-aos-duration="1000">
                            <div className="col-md-7">
                            <h2 className="section-heading rep-text-center text-left pb-0">Get started with your own digital banking.</h2>
                                <p className="text-lightblue ubuntuMedium rep-text-center">Your Bank, your platform, your brand, your look &amp; feel !</p>
                            </div>
                            <div className="col-md-4 offset-md-1 pt-2">
                                <div className="btnDiv rep-text-center">    
                                    <button className="btn btn-blue-rounded mt-5  mg-tp0" onClick={this.showcontactuspage}>Request for custom price</button>
                                </div>
                            </div>
                        </div>
                        <div className="row mx-0 pt-5" data-aos="zoom-in-up" data-aos-duration="1000">
                            <div className="col-md-7">
                                <h2 className="section-heading rep-text-center text-left pb-0">Setup your own Payment Gateway</h2>  
                                <p className="text-lightblue ubuntuMedium rep-text-center">Your platform, your brand, your look &amp; feel !</p>                      
                            </div>
                            <div className="col-md-4 offset-md-1 pt-2">
                                <div className="btnDiv rep-text-center">
                                    <button className="btn btn-blue-rounded  mt-5  mg-tp0" onClick={this.showcontactuspage}>Request for custom price</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Help />
                  </main>
                <Footer/>
            </>
        );
    }
}

export default Pricing;
