import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";

// core components
import Navbar from "components/Navbars/Navbar.js";
import Footer from "components/Footers/Footer.js";
import HeroBanner from "components/HeroBanner/Career.js";
import ApplyFormJdnew from "components/ApplyFromJdnew/ApplyFormJdnew";


import {NavLink} from "reactstrap";

class ProductManager extends React.Component {
    componentDidMount() {
        // document.documentElement.scrollTop = 0;
        // document.scrollingElement.scrollTop = 0;
        // this.refs.main.scrollTop = 0;
        // const wow = new WOW();
        // wow.init();
    }

    freshChatClick = () => {
        // if (window.fcWidget.isOpen() !== true) {
        //     window.fcWidget.open();
        // }
    };

    render() {
        return (
            <>
                <Navbar />
                <main ref="main" className="dv-main">
                    <HeroBanner />
                </main>
                <div className="container mt-5">
                    <div className="row">
                        {/* <div className="col-12 text-right">
                            <a href="/career" className="btn-back">Back</a>
                        </div> */}
                        <div className="col-12">
                            <h2 className="font-w7 ubuntuMono op-txt">Open Position</h2>
                            <a href="/career" className="btn-back">Back</a>
                            <div className="clearfix"></div>
                            <h3 className="font26 border-bottom py-3 mb-3">
                            At safexpay we aspire to build a global,  digital platform
infrastructure and product company focused for every business,
with an extraordinary culture that attracts innovative,
collaborative people.
                              </h3>
                            <p>
                                <strong className="font-w6">
                                    <span className="color-blue mrg-10"> Product Manager</span> 
Location: Mumbai <br />
Experience: 10+ years’ experience in ﬁntech product management and market strategy <br /><br />
                                    <span className="color-blue mrg-10"> Qualiﬁcations
:</span>  
                                    Bachelor degree in a relevant ﬁeld or equivalent experience
 <br /><br />
                                    <span className="color-blue mrg-10"> Key Responsibilities :</span>
</strong>

<ul className="career-points">
    <li>Drive the entire vision and delivery of new products, features to keep the business
relevant and competitive.</li>
    <li>
    Conduct market and Product research to identify potential features or releases.</li>
    <li>
Manage the roadmap, prioritize product opportunities, reﬁne feature ideas and
maintain existing features to drive business goals.
</li>
    <li>Maintain and update existing features.
    </li>
    <li>Produce high-level product requirements, iterate with your design and engineering
teams.
</li>
    <li>Engage stakeholders in leadership, research, data, and support, and a coordinate across
teams to maintain product and strategy alignment.
</li>
    <li>Coordinate go-to-market strategies when launching a new product or feature.
    </li>
</ul>

<strong className="font-w6"><span className="color-blue mrg-10">Skills:</span>
</strong>
<ul className="career-points">
    <li>
    Technical background will be a plus.
    </li>
    <li>Relevant experience in a Fintech Company, especially in the Payments, Digital Banking, Open
banking and API banking .
</li>
    <li>Well-versed with Fintech Innovations in the South East Asian Market.
    </li>
    <li>Proven track record of working with designers, engineers, and various stakeholders to
coordinate, plan, execute, and track product releases from beginning to end.
</li>
    <li>Strong problem-solving skills and willingness to think outside the box and roll up one’s sleeves
to get the job done.
</li>
    <li>High energy, hands-on, detailed-oriented and able to work independently.
    </li>
    <li>High degree of self-initiative, proactive and self-driven to drive results.
    </li>
    <li>Able to think outside the box, innovate and diﬀerentiate.
    </li>
    <li>Good organizing skills.
    </li>
    <li>Good communication and interpersonal skills.
    </li>
    <li>Team handling skills
    </li>
    <li>Self-starter, ability to work and thrive within a fast paced, culturally diverse and hands-on
environment.
    </li>    
</ul>

                              </p><br />
                             
                                    
                        </div>
                    </div>
                </div>
                <ApplyFormJdnew pos="Product Manager" loc="mumbai" exp="10+yrs"/>
                <Footer />
            </>
        );
    }
}

export default ProductManager;
