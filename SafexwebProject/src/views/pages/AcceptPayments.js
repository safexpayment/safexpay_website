import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";

// core components
import Navbar from "components/Navbars/Navbar.js";
import Footer from "components/Footers/Footer.js";
import HeroBanner from "components/HeroBanner/AcceptPayments.js";
import PaymentOptions from "components/PaymentOptions/PaymentOptions.js";
import PaymentGatewayFeatures from "components/PaymentGatewayFeatures/PaymentGatewayFeatures.js";
import PaymentMethods from "components/PaymentMethods/PaymentMethods.js";
//import Rates from "components/Rates/Rates.js";
import StartupPricing from "components/StartupPricing/StartupPricing.js";
import MoreHelp2 from "components/MoreHelp2/MoreHelp2.js";
import Help from "components/Help/Help.js";
import Contactuspagepay from "components/Contactuspagepay/Contactuspagepay";
import  Helmet  from "react-helmet";


class AcceptPayments extends React.Component {
   

    state={
        open:false
    }

    showcontactuspage =(props)=>{
        console.log("button click.....");
        this.setState({
            open:!this.state.open
        })

    }

    closepage=()=>{
        this.setState({
            open:!this.state.open
        })
    }


    render() {
        return (
            <>
                <Helmet>
                    <meta name="title" content="SafexPay - Unlimited Payment Options"/>
                    <meta name="description" content="SafexPay provides you the multiple options to accept payments. Fast, Standard and Fully customised payment experience."/>
                    <meta name="keywords" content="loan disbursal,recurring payments,payment gateway process,payment gateway integration,payment gateway website,payment gateway charges,payment gateway solutions"/>

                    <meta property="og:title" content="SafexPay - Customized Payment Experience"/>
                    <meta property="og:url" content="https://www.safexpay.com/business/accept-payments"/>
                    <meta property="og:description" content="SafexPay provides you the multiple options to accept payments. Fast, Standard and Fully customised payment experience." />
                    <meta property="og:image" itemprop="image" content="https://www.safexpay.com/static/media/logo.0c7d81d8.png"/>
                    <meta property="og:type" content="website" />
                    
                    <link rel="shortcut icon" type="image/x-icon" href="../src/assets/img/others/favicon.png"/>
                    <link rel="apple-touch-icon" href="../src/assets/img/others/favicon.png"/>

                    <link rel="canonical" href="https://www.safexpay.com/business/accept-payments" />

                    <title>SafexPay - Customized Payment Experience</title>
                </Helmet>
                <Contactuspagepay open={this.state.open} click={this.closepage}/>
                <Navbar />
                <main ref="main" className="dv-main">
                    <HeroBanner />
                    <PaymentOptions />
                    <PaymentGatewayFeatures />
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 offset-md-1 col-12">
                                <h2 className="section-heading mt-4 text-center text-md-left font-s30 pb-0">
                                    FlexiQR, the most ﬂexible QR solution!
                                </h2>
                                <p className="rep-text-center">
                                    No need for your customers to install any app,
                                    just scan using the phone camera and pay with
                                    100+ payment modes. Create a QR on the ﬂy
                                    with our APIs or generate in bulk to display in
                                    physical stores.
                                </p>
                                <div className="btnDiv rep-text-center">
                                    <button className="btn btn-blue-rounded mt-2" onClick={this.showcontactuspage}>TALK TO US</button>
                                </div>
                            </div>
                            <div className="col-md-4 col-12">
                                <img
                                    alt="Flexi QR"
                                    className="img-fluid img-paymentfeature"
                                    src={require("assets/img/section/flexiqr.png")}
                                />
                            </div>

                        </div></div>
                    <div className="container my-5 py-3">
                        <div className="shadow-box" data-aos="fade-up" data-aos-duration="1000">
                            <h2 className="section-heading text-center text-md-left py-0 font-s30">
                                Digital onboarding : <br />
Go live in less than 10min, faster settlements
                        </h2>
                            <div className="btnDiv rep-text-center">
                                <button className="btn btn-blue-rounded mt-2" onClick={this.showcontactuspage}>TALK TO US</button>
                            </div>
                        </div>
                    </div>
                    <PaymentMethods />
                    <StartupPricing />
                    <div className="container py-3" data-aos="fade-up" data-aos-duration="1000">
                        <div className="row m-5">
                            <div className="col-md-8">
                                <h2 className="section-heading text-center text-md-left p-0 m-0 font-s30 smfont">
                                    Looking For Customized Pricing Or
                                    International Payments?
</h2>
                            </div>
                            <div className="col-md-4">
                                <div className="rep-text-center">
                                    <button className="btn btn-blue-rounded  mt-4" onClick={this.showcontactuspage}>Talk To Us</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <MoreHelp2 />
                    <Help />
                </main>
                <Footer />
            </>
        );
    }
}

export default AcceptPayments;
