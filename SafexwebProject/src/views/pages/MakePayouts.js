import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";

// core components
import Navbar from "components/Navbars/Navbar.js";
import Footer from "components/Footers/Footer.js";
import HeroBanner from "components/HeroBanner/MakePayouts.js";
import Makepayouts from "components/Make-payouts/Make-payouts.js";
import SendMoney from "components/SendMoney/SendMoney.js";
import UsedCases from "components/UsedCases/UsedCases.js";
import Pricing from "components/Pricing/Pricing.js";
import Bankingexperience from "components/Banking-experience/Banking-experience.js";
import Help from "components/Help/Help.js";
import  Helmet  from "react-helmet";

import Contactuspage from "components/Contactuspagepay/Contactuspagepay";

class MakePayments extends React.Component {
    componentDidMount() {
        // document.documentElement.scrollTop = 0;
        // document.scrollingElement.scrollTop = 0;
        // this.refs.main.scrollTop = 0;
        // const wow = new WOW();
        // wow.init();
    }

    freshChatClick = () => {
        // if (window.fcWidget.isOpen() !== true) {
        //     window.fcWidget.open();
        // }
    };
    state={
        open:false
    }

    showcontactuspage =(props)=>{
        console.log("button click.....");
        this.setState({
            open:!this.state.open
        })

    }

    closepage=()=>{
        this.setState({
            open:!this.state.open
        })
    }

    render() {
        return (
            <>
                <Helmet>
                    <meta name="title" content="SafexPay - Make Instant Payouts"/>
                    <meta name="description" content="Transfer Money 24*7 with Safexpay Payout instantly anywhere. Make payouts your way."/>
                    <meta name="keywords" content="payment portals,e-wallets,payment gateway service,best payment gateway in india,payment gateway charges,payment processing"/>

                    <meta property="og:title" content="SafexPay - Instant Payouts"/>
                    <meta property="og:url" content="https://www.safexpay.com/business/make-payouts"/>
                    <meta property="og:description" content="Transfer Money 24*7 with Safexpay Payout instantly anywhere. Make payouts your way." />
                    <meta property="og:image" itemprop="image" content="https://www.safexpay.com/static/media/logo.0c7d81d8.png"/>
                    <meta property="og:type" content="website" />
                    
                    <link rel="shortcut icon" type="image/x-icon" href="../src/assets/img/others/favicon.png"/>
                    <link rel="apple-touch-icon" href="../src/assets/img/others/favicon.png"/>

                    <link rel="canonical" href="https://www.safexpay.com/business/make-payouts" />

                    <title>SafexPay - Instant Payouts</title>
                </Helmet>
                <Contactuspage open={this.state.open} click={this.closepage}/>
                <Navbar />
                <main ref="main" className="dv-main">
                    <HeroBanner />
                    <Makepayouts />
                    <SendMoney />
                    <UsedCases />
                    <Pricing />
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="mx-md-5 px-md-5">
                                    <div className="shadow-box mx-md-5 mt-5 mb-5" data-aos="fade-up" data-aos-duration="1000">
                                        <h2 className="section-heading text-center text-md-left py-0">Looking For Customized Pricing</h2>
                                        <div className="btnDiv rep-text-center">
                                            <button className="btn btn-blue-rounded mt-4"  onClick={this.showcontactuspage}>TALK TO US</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <Bankingexperience />
                    <Help />
                </main>
                <Footer />
            </>
        );
    }
}

export default MakePayments;
