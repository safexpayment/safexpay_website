import React from "react";
import WOW from "wow.js";
// nodejs library that concatenates classes
// reactstrap components
import {
    Button,
    Card,
    CardBody,
    Container,
    Row,
    Col,
    Table,
} from "reactstrap";

// core components
//import Header from "components/Header/Header";
import Header from "components/Header/Header";
import Footer from "components/Footers/Footer";
import PricingTable from "views/elements/PricingTable";
import FreshSale from "views/elements/FreshSale";

class Home extends React.Component {
    state={
        open:false
    }

    showcontactuspage =(props)=>{
        console.log("button click.....");
        this.setState({
            open:!this.state.open
        })

    }

    closepage=()=>{
        this.setState({
            open:!this.state.open
        })
    }

    componentDidMount() {
        document.documentElement.scrollTop = 0;
        document.scrollingElement.scrollTop = 0;
        this.refs.main.scrollTop = 0;
        const wow = new WOW();
        wow.init();
    }

    render() {
        return (
            <>
                <Header/>
                <Contactuspage open={this.state.open} click={this.closepage}/>
                <main ref="main" className="dv-main">
                    <div className="position-relative">
                        {/* shape Hero */}
                        <section className="section section-lg section-shaped pb-250">
                            <div className="banner-shape"/>
                            <Container className="py-lg-md d-flex">
                                <div className="col px-0">
                                    <Row>
                                        <Col lg="6" className="pt-5 pb-0 pr-0">
                                            <h1 className="display-3 text-white">
                                                One platform to accept payments how you want
                                            </h1>
                                            <p className="lead text-white">
                                                Develop a customised experience by integrating with our
                                                APIs and SDKs OR simply create a payment button or
                                                send a link to your customers with no development effort.
                                            </p>
                                            <div className="btn-wrapper">
                                                <FreshSale/>
                                            </div>
                                        </Col>
                                        <Col lg="6" className="pl-0 mt-lg--6">
                                            <img
                                                alt="..."
                                                className="img-fluid"
                                                src={require("assets/custom/img/pages/payment_gateway/1.png")}
                                            />
                                        </Col>
                                    </Row>
                                </div>
                            </Container>
                        </section>
                    </div>
                    <section className="section">
                        <Container>
                            <Row className="justify-content-center text-center mt-5">
                                <Col lg="12">
                                    <h3 className="ubuntu-ff-m">Multiple Options To Accept Payments</h3>
                                </Col>
                            </Row>
                        </Container>
                    </section>
                    <div className="position-relative bg-style-5">
                        <section className="section section-shaped pb-lg-100">
                            <div className="shape shape-style-8 shape-default"/>
                            <Container>
                                <Row className="row-grid align-items-center">
                                    <Col className="order-lg-1 text-center" lg="6">
                                        <img
                                            alt="..."
                                            className="img-fluid w-60 mb-5"
                                            src={require("assets/custom/img/pages/payment_gateway/2.png")}
                                        />
                                    </Col>
                                    <Col className="order-lg-2" lg="6">
                                        <div className="d-flex px-lg-3 pb-lg-5">
                                            <div className="pl-4 text-right">
                                                <h3 className="text-white text-right ubuntu-ff-m">
                                                    Fully Customised Payment Experience
                                                </h3>
                                                <p className="text-white text-right pl-lg-5 font-weight-600 letter-spacing-1">
                                                    Give power to your designers and
                                                    developers to create something magical.
                                                </p>
                                                <p
                                                    className="text-white mt-1 mb-0"
                                                >
                                                    Website <i className="fa fa-arrow-circle-left ml-2"/>
                                                </p>
                                                <p
                                                    className="text-white mt-1"
                                                >
                                                    Mobile App <i className="fa fa-arrow-circle-left ml-2"/>
                                                </p>
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                            </Container>
                            {/*<div className="separator separator-bottom" style={{height: "300px"}}>
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    preserveAspectRatio="none"
                                    version="1.1"
                                    viewBox="0 0 2560 400"
                                    x="0"
                                    y="0"
                                >
                                    <polygon
                                        className="fill-secondary"
                                        points="0 400,2560 400,2560 0"
                                    />
                                </svg>
                            </div>*/}
                        </section>
                        <section className="section bg-transparent pt-lg-0 pb-lg-0 mt-lg--5 mb-lg--4">
                            <Container>
                                <Row className="row-grid align-items-center mb-0">
                                    <Col className="" md="6">
                                        <div className="pr-md-5">
                                            <h3 className="ubuntu-ff-m">Fast, Standard And Beautiful Checkout
                                                Experience</h3>
                                            <p className="font-weight-600 letter-spacing-1">
                                                Fast integration, all error validations in built,
                                                super success rates, minor customisations!
                                            </p>
                                            <p
                                                className="font-weight-normal mt-1 mb-0"
                                            >
                                                <i className="fa fa-arrow-circle-right mr-2"/> Dashboard
                                            </p>
                                            <p
                                                className="font-weight-normal mt-1"
                                            >
                                                <i className="fa fa-arrow-circle-right mr-2"/> Mobile App
                                            </p>
                                        </div>
                                    </Col>
                                    <Col className="text-center" md="6">
                                        <img
                                            alt="..."
                                            className="img-fluid w-60 mt--5"
                                            src={require("assets/custom/img/pages/payment_gateway/3.png")}
                                        />
                                    </Col>
                                    <Col className="" md="6">
                                        <img
                                            alt="..."
                                            className="img-fluid w-90 mt--3"
                                            src={require("assets/custom/img/pages/payment_gateway/4.png")}
                                        />
                                    </Col>
                                    <Col className="pb-2" md="6">
                                        <div className="pr-md-5 text-right">
                                            <h3 className="ubuntu-ff-m">Using Plugins To Develop
                                                Your Cart Or Application?</h3>
                                            <p className="font-weight-600 letter-spacing-1">
                                                Just enter the keys provided and get
                                                accepting payments in minutes.
                                            </p>
                                            <p
                                                className="font-weight-normal mt-1 mb-0"
                                            >
                                                Dashboard <i className="fa fa-arrow-circle-left ml-2"/>
                                            </p>
                                            <p
                                                className="font-weight-normal mt-1"
                                            >
                                                Mobile App <i className="fa fa-arrow-circle-left ml-2"/>
                                            </p>
                                        </div>
                                    </Col>
                                </Row>
                                <Row className="row-grid align-items-center mt-0">

                                </Row>
                            </Container>
                        </section>
                        <section className="section section-shaped pt-lg-7 pb-lg-0 pt-lg-0">
                            <div className="shape shape-style-9 shape-default"/>
                            <Container>
                                <Row className="row-grid align-items-center">
                                    <Col className="order-lg-2 text-center pt-lg-4 pb-lg-7 pr-5" lg="6">
                                        <img
                                            alt="..."
                                            className="img-fluid w-100"
                                            src={require("assets/custom/img/pages/payment_gateway/5.png")}
                                        />
                                    </Col>
                                    <Col className="order-lg-1 mb-lg--3" lg="6">
                                        <div className="d-flex px-3">
                                            <div className="pl-4 pt-6">
                                                <h3 className="text-white ubuntu-ff-m">Send Link On Sms Or
                                                    Email</h3>
                                                <p className="text-white font-weight-600 letter-spacing-1">
                                                    Use our dashboard to send links individually or in
                                                    bulk OR integrate our APIs and automate your
                                                    payment requests.
                                                </p>
                                                <p
                                                    className="font-weight-normal text-white mt-1 mb-0"
                                                >
                                                    <i className="fa fa-arrow-circle-right mr-2"/> Desktop
                                                </p>
                                                <p
                                                    className="font-weight-normal text-white mt-1 mb-0"
                                                >
                                                    <i className="fa fa-arrow-circle-right mr-2"/> Merchant App
                                                </p>
                                                <p
                                                    className="font-weight-normal text-white mt-1"
                                                >
                                                    <i className="fa fa-arrow-circle-right mr-2"/> API
                                                </p>
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                            </Container>
                        </section>
                        <section className="section bg-transparent pt-lg-0 pb-300">
                            <Container>
                                <Row className="row-grid align-items-center">
                                    <Col className="order-lg-1" lg="6">
                                        <div className="d-flex px-3">
                                            <div className="pl-4">
                                                <h3 className="ubuntu-ff-m pr-lg-5">
                                                    Make Your Payment Button In {"<"} 2 Minutes</h3>
                                                <p className="font-weight-600 letter-spacing-1">
                                                    No developer no problem. Just use our dashboard
                                                    and make a payment button to add on any webpage.
                                                </p>
                                                <p
                                                    className="font-weight-normal mt-1"
                                                >
                                                    <i className="fa fa-arrow-circle-right mr-2"/> Dashboard
                                                </p>
                                            </div>
                                        </div>
                                    </Col>
                                    <Col className="order-lg-2 text-center" lg="6">
                                        <img
                                            alt="..."
                                            className="img-fluid w-50 mt-lg--4 mb-lg--7"
                                            src={require("assets/custom/img/pages/payment_gateway/6.png")}
                                        />
                                    </Col>
                                </Row>
                            </Container>
                        </section>
                    </div>
                    <section className="section section-lg">
                        <Container>
                            <Row className="justify-content-center text-center mb-5">
                                <Col lg="12">
                                    <h3 className="ubuntu-ff-m">Unlimited payment Methods</h3>
                                </Col>
                            </Row>
                            <Row className="row-grid align-items-center">
                                <Table className="mb-0">
                                    <tbody>
                                    <tr>
                                        <th className="border-bottom">Debit, Credit & International Card</th>
                                    </tr>
                                    </tbody>
                                </Table>
                            </Row>
                            <Row className="row-grid align-items-center">
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/7.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/8.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/9.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/10.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/11.png")}
                                    />
                                </Col>
                            </Row>
                            <Row className="row-grid align-items-center">
                                <Table className="mb-0">
                                    <tbody>
                                    <tr>
                                        <th className="border-bottom">50+ Netbanking Options</th>
                                    </tr>
                                    </tbody>
                                </Table>
                            </Row>
                            <Row className="row-grid align-items-center">
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/14.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/15.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/16.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/17.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/18.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/19.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3" className="mt-lg-4">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/53.png")}
                                    />
                                </Col>
                            </Row>
                            <Row className="row-grid align-items-center">
                                <Table className="mb-0">
                                    <tbody>
                                    <tr>
                                        <th className="border-bottom">UPI</th>
                                    </tr>
                                    </tbody>
                                </Table>
                            </Row>
                            <Row className="row-grid align-items-center">
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/21.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/22.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/23.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/54.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/26.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/27.png")}
                                    />
                                </Col>
                            </Row>
                            <Row className="row-grid align-items-center">
                                <Table className="mb-0">
                                    <tbody>
                                    <tr>
                                        <th className="border-bottom">Wallets & Pay Later</th>
                                    </tr>
                                    </tbody>
                                </Table>
                            </Row>
                            <Row className="row-grid align-items-center border-bottom pb-4">
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/28.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/25.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/29.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/30.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/31.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/32.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3" className="mt-lg-4">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/33.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3" className="mt-lg-4">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/34.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3" className="mt-lg-4">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/35.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3" className="mt-lg-4">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/36.png")}
                                    />
                                </Col>
                                <Col xs="6" lg="2" md="3" className="mt-lg-4">
                                    <img
                                        alt="..."
                                        className="img-center w-100"
                                        src={require("assets/custom/img/pages/payment_gateway/37.png")}
                                    />
                                </Col>
                            </Row>
                            <Row className="row-grid align-items-center text-center px-lg-9 mt-5">
                                <Col lg="4">
                                    <img
                                        alt="..."
                                        className="img-center w-40"
                                        src={require("assets/custom/img/pages/payment_gateway/38.png")}
                                    />
                                    <p className="ubuntu-ff-m mt-3">50+ Currencies</p>
                                </Col>
                                <Col lg="4">
                                    <img
                                        alt="..."
                                        className="img-center w-40"
                                        src={require("assets/custom/img/pages/payment_gateway/40.png")}
                                    />
                                    <p className="ubuntu-ff-m mt-3" style={{lineHeight: 1}}>EMI<br/><span
                                        className="small">(Cards & Cardless)</span></p>
                                </Col>
                                <Col lg="4">
                                    <img
                                        alt="..."
                                        className="img-center w-40"
                                        src={require("assets/custom/img/pages/payment_gateway/39.png")}
                                    />
                                    <p className="ubuntu-ff-m mt-3">Bharat QR</p>
                                </Col>
                            </Row>
                        </Container>
                    </section>
                    <div className="position-relative bg-style-6">
                        <section className="section bg-transparent">
                            <Container>
                                <Row className="justify-content-center text-center">
                                    <Col lg="8">
                                        <h2 className="ubuntu-ff-m mb-5">Priced Right For Your Scale</h2>
                                    </Col>
                                </Row>
                                <Row className="justify-content-center text-center">
                                    <Col lg="9">
                                        <PricingTable/>
                                    </Col>
                                </Row>
                            </Container>
                        </section>
                        <section className="section bg-transparent">
                            <Container>
                                <Row className="justify-content-center text-center">
                                    <Col lg="8">
                                        <h2 className="ubuntu-ff-m mb-0">Looking For Something More?</h2>
                                        <p className="ubuntu-ff-m text-dark mt-0">
                                            Co-create your custom solution with us.
                                        </p>
                                    </Col>
                                </Row>
                                <Row className="justify-content-center mt-5">
                                    <Col lg="12">
                                        <Row className="row-grid">
                                            <Col lg="4">
                                                <Card className="shadow-lg shadow--hover border-0 h-100">
                                                    <CardBody className="py-5">
                                                        <img
                                                            alt="..."
                                                            className="w-25 mt--4 mb-2"
                                                            src={require("assets/custom/img/pages/payment_gateway/50.png")}
                                                        />
                                                        <h5 className="text-default ubuntu-ff-m">
                                                            Build your own payment gateway
                                                        </h5>
                                                        <p className="description mt-3 mb-5 text-default">
                                                            Our platform can be installed on your server or we can host
                                                            a
                                                            separate instance on our cloud infrastructure. We can manage
                                                            the
                                                            entire deployment and upgrade regularly with newer features.
                                                        </p>
                                                        <Button
                                                            className="mt-4 position-absolute bottom-4 btn-default"
                                                            href="/products/white-label-platform/payment-gateway"
                                                        >
                                                            <i className="fa fa-chevron-circle-right mr-3"/>Lear More
                                                        </Button>
                                                    </CardBody>
                                                </Card>
                                            </Col>
                                            <Col lg="4">
                                                <Card className="shadow-lg shadow--hover border-0 h-100">
                                                    <CardBody className="py-5">
                                                        <img
                                                            alt="..."
                                                            className="w-25 mt--4 mb-2"
                                                            src={require("assets/custom/img/pages/payment_gateway/51.png")}
                                                        />
                                                        <h5 className="text-default ubuntu-ff-m">
                                                            Resell our services to your clients
                                                        </h5>
                                                        <p className="description mt-3 mb-5 text-default">
                                                            You can easily resell some or all of our services to your
                                                            clients with your own custom pricing. Manage the entire
                                                            process
                                                            on our dashboard. You can rebrand our dashboards with your
                                                            branding and provide it for your clients to manage their
                                                            transactions and refunds.
                                                        </p>
                                                        <Button
                                                            className="mt-4 position-absolute bottom-4 btn-default"
                                                            href="/partner-platform/reseller"
                                                        >
                                                            <i className="fa fa-chevron-circle-right mr-3"/>Learn More
                                                        </Button>
                                                    </CardBody>
                                                </Card>
                                            </Col>
                                            <Col lg="4">
                                                <Card className="shadow-lg shadow--hover border-0 h-100">
                                                    <CardBody className="py-5">
                                                        <img
                                                            alt="..."
                                                            className="w-25 mt--4 mb-2"
                                                            src={require("assets/custom/img/pages/payment_gateway/52.png")}
                                                        />
                                                        <h5 className="text-default ubuntu-ff-m">
                                                            Have something else in mind?
                                                        </h5>
                                                        <p className="description mt-3 mb-5 text-default">
                                                            Have something else in mind? Have a custom solution that
                                                            you’re
                                                            looking for? Tell us your requirements and we can co-create
                                                            something.
                                                        </p>
                                                        <Button
                                                            className="mt-4 position-absolute bottom-4 btn-default"
                                                            href="#pablo"
                                                            onClick={e => e.preventDefault()}
                                                        >
                                                            <i className="fa fa-chevron-circle-right mr-3"  onClick={this.showcontactuspage}/>Talk To Us
                                                        </Button>
                                                    </CardBody>
                                                </Card>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </Container>
                        </section>
                        <section className="section">
                            <Container>
                                <Row className="row-grid align-items-center">
                                    <Col className="order-md-2" md="6">
                                        <img
                                            alt="..."
                                            className="img-fluid"
                                            src={require("assets/custom/img/pages/home/21.png")}
                                        />
                                    </Col>
                                    <Col className="order-md-1" md="6">
                                        <div className="pr-md-5">
                                            <h3 className="ubuntu-ff-m">Ready to boost your business with Safexpay?</h3>
                                            <p className="ubuntu-ff-m">
                                                Need more information to help you decide, call our helpline or drop us a
                                                note and we will get back to you
                                            </p>
                                            <div className="btn-wrapper">
                                                <Button
                                                    className="btn-primary btn-icon mb-3 mb-sm-0"
                                                    color="default"
                                                    href="#pablo"
                                                >
                                                    {/* <span className="btn-inner--icon mr-1">
                                                      <i className="fa fa-phone"/>
                                                    </span> */}
                                                    <span className="btn-inner--text"  onClick={this.showcontactuspage}>TALK TO US</span>
                                                </Button>
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                            </Container>
                        </section>
                    </div>
                </main>
                <Footer/>
            </>
        );
    }
}

export default Home;
