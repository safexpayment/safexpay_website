import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";

// core components
import Navbar from "components/Navbars/Navbar.js";
import Footer from "components/Footers/Footer.js";
import HeroBanner from "components/HeroBanner/Career.js";
import ApplyFormJdnew from "components/ApplyFromJdnew/ApplyFormJdnew";


import {NavLink} from "reactstrap";

class TeamLead extends React.Component {
   
    render() {
        return (
            <>
                <Navbar />
                <main ref="main" className="dv-main">
                    <HeroBanner />
                </main>
                <div className="container mt-5">
                    <div className="row">
                        {/* <div className="col-12 text-right">
                            <a href="/career" className="btn-back">Back</a>
                        </div> */}
                        <div className="col-12">
                            <h2 className="font-w7 ubuntuMono op-txt">Open Position</h2>
                            <a href="/career" className="btn-back">Back</a>
                            <div className="clearfix"></div>
                            <h3 className="font26 border-bottom py-3 mb-3">
                                At safexpay we aspire to build a global,  digital platform
                                infrastructure and product company focused for every business,
                                with an extraordinary culture that attracts innovative,
                                collaborative people.
                              </h3>
                            <p>
                                <strong className="font-w6">
                                    <span className="color-blue mrg-10">Team Lead</span>
                                    Location: Mumbai 
 <br />
 Experience: 6+ years<br /><br />
 <span className="color-blue mrg-10"> Skills: </span> 
Technical Skills: Java, J2EE, Spring MVC, Spring Boot, Hibernate, MySql/Oracle, Angular JS/React JS,
AWS, CI/CD, Jquery, Javascript, Node.js, Erlang, Scala, Python <br/>
Project Management Skills: Agile, Waterfall, Scrum <br/>
Documentation: Software Architecture Document, PCI DSS <br/><br />
<span class="color-blue mrg-10"> Job Description </span>
</strong>


 <ul className="career-points">
     <li> Candidate should have experience in developing applications using JAVA/J2EE programming skills</li>
     <li>Strong in DB knowledge</li>
     <li> Should be good with OOPS Concept</li>
     <li>Should be good in design and application development using object oriented programming</li>
     <li>Should be good in writing secure and optimized code</li>
     <li>Experience in any application framework like Spring and ORM tools like Hibernate</li>
     <li>Good understanding/experience on software development methodology such as Agile, TDD etc</li>
     <li>Good understanding of source control tools and its use (SVN, Git etc...)</li>
     <li>Experience in web services would be a plus but not mandatory</li>
     <li>Experience in multithreaded applications and micro service based development would be a plus</li>
     <li>Excellent written and verbal communication skills</li>
 </ul>

                              </p>
                             
                              <ApplyFormJdnew pos="Team Lead" loc="mumbai" exp="6+yrs"/>
                        </div>
                    </div>
                </div>
                <Footer />
            </>
        );
    }
}

export default TeamLead;
