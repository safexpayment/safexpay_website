import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";

// core components
import Navbar from "components/Navbars/Navbar.js";
import Footer from "components/Footers/Footer.js";
import HeroBanner from "components/HeroBanner/Career.js";
import ApplyFormJdnew from "components/ApplyFromJdnew/ApplyFormJdnew";

import {NavLink} from "reactstrap";

class AppSupportEngineer extends React.Component {
   
    render() {
        return (
            <>
                <Navbar />
                <main ref="main" className="dv-main">
                    <HeroBanner />
                </main>
                <div className="container mt-5">
                    <div className="row">
                        {/* <div className="col-12 text-right">
                            <a href="/career" className="btn-back">Back</a>
                        </div> */}
                        <div className="col-12">
                            <h2 className="font-w7 ubuntuMono op-txt">Open Position</h2>
                            <a href="/career" className="btn-back">Back</a>
                            <div className="clearfix"></div>
                            <h3 className="font26 border-bottom py-3 mb-3">
                                At safexpay we aspire to build a global,  digital platform
                                infrastructure and product company focused for every business,
                                with an extraordinary culture that attracts innovative,
                                collaborative people.
                              </h3>
                            <p>
                                <strong className="font-w6">
                                    <span className="color-blue mrg-10">App Support Engineer</span>
                                    Location: Mumbai 
 <br />
 Experience: 2+ years<br /><br />
 <span className="color-blue mrg-10"> Skills: </span> 
 Centos 7.2, Linux, Mysql Database, HyperV <br/><br />
<span class="color-blue mrg-10"> Job Description </span>
</strong>


 <ul className="career-points">
    <li>Knowledge of Centos 7.2 and Linux at least 2 years</li>
    <li>Knowledge of Mysql Database (user creation, giving Rights)</li>
    <li>Windows 2012 r2 user creation</li>
    <li>Running query on Database</li>
    <li>Knowledge of creating scripts for moving logs to another drive</li>
    <li>Knowledge of taking Backup of logs</li>
    <li>Knowledge of HyperV</li>
    <li>Must be comfortable to work in Shifts</li>
    <li>Must be comfortable to work on Weekends</li>
 </ul>
</p>      
                        </div>
                    </div>
                </div>
                <ApplyFormJdnew pos="App Support Engineer" loc="mumbai" exp="2+yrs"/>
                <Footer />
            </>
        );
    }
}

export default AppSupportEngineer;
