import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";

// core components
import Navbar from "components/Navbars/Navbar.js";
import Footer from "components/Footers/Footer.js";
import HeroBanner from "components/HeroBanner/PaymentGateway.js";
import FullyCustomised from "components/FullyCustomised/FullyCustomised.js";
import SupportingAggregators from "components/SupportingAggregators/SupportingAggregators.js";
import Neobanking from "components/Neo-banking/Neo-banking.js";
import BankPartners from "components/Bank-Partners/Bank-Partners.js";
import BusinessApp from "components/Business-App/Business-App.js";
import MoreHelp from "components/MoreHelp/MoreHelp.js";
import Helmet from "react-helmet";
import Help from "components/Help/Help.js";
import Contactuspage from "components/Contactuspagepay/Contactuspagepay";


class PaymentGateway extends React.Component {
    componentDidMount() {
        // document.documentElement.scrollTop = 0;
        // document.scrollingElement.scrollTop = 0;
        // this.refs.main.scrollTop = 0;
        // const wow = new WOW();
        // wow.init();
    }

    freshChatClick = () => {
        // if (window.fcWidget.isOpen() !== true) {
        //     window.fcWidget.open();
        // }
    };

    state={
        open:false
    }

    showcontactuspage =(props)=>{
        console.log("button click.....");
        this.setState({
            open:!this.state.open
        })

    }

    closepage=()=>{
        this.setState({
            open:!this.state.open
        })
    }

    render() {
        return (
            <>
                <Helmet>
                    <meta name="title" content="SafexPay - Payment Gateway"/>
                    <meta name="description" content="Use our payment gateway platform to grow your digital business. Neo Banking. Fully customized for your business."/>
                    <meta name="keywords" content="advanced analytics,digital payment,white label solutions,white label payment gateways,payment gateway integration india,e-wallets,payment gateway service"/>
 
                    <meta property="og:title" content="SafexPay - Payment Gateway"/>
                    <meta property="og:url" content="https://www.safexpay.com/white-label/payment-gateway"/>
                    <meta property="og:description" content="Use our payment gateway platform to grow your digital business. Neo Banking. Fully customized for your business." />
                    <meta property="og:image" itemprop="image" content="https://www.safexpay.com/static/media/logo.0c7d81d8.png"/>
                    <meta property="og:type" content="website" />
                    
                    <link rel="shortcut icon" type="image/x-icon" href="../src/assets/img/others/favicon.png"/>
                    <link rel="apple-touch-icon" href="../src/assets/img/others/favicon.png"/>

                    <link rel="canonical" href="https://www.safexpay.com/white-label/payment-gateway" />
 
                    <title>SafexPay - Payment Gateway</title>
                </Helmet>
                <Contactuspage open={this.state.open} click={this.closepage}/>
                <Navbar/>
                <main ref="main" className="dv-main">
                    <HeroBanner />
                    <FullyCustomised />
                    <SupportingAggregators />
                    
                    <div className="container" data-aos="fade-up" data-aos-duration="1000">
                        <div className="row m-5">
                            <div className="col-md-8">
                                <h2 className="section-heading text-center text-md-left p-0 m-0 font-s30">Ready to go-live in less than 7 days</h2>
                                <p className="color-lightblue rep-text-center">Setup your own Payment Gateway</p>
                            </div>
                            <div className="col-md-4">
                                <div className="btnDiv rep-text-center">
                                    <button className="btn btn-blue-rounded" onClick={this.showcontactuspage}>Request for custom price</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Neobanking />
                    <BankPartners />
                    <div className="container" data-aos="fade-up" data-aos-duration="1000">
                        <div className="row m-5">
                            <div className="col-md-8">
                                <h2 className="section-heading text-center text-md-left p-0 m-0 font-s30">Ready to go-live in less than 7 days</h2>
                                <p className="color-lightblue rep-text-center">Setup your Neo Bank platform.</p>
                            </div>
                            <div className="col-md-4">
                                <div className="btnDiv rep-text-center">
                                    <button className="btn btn-blue-rounded" onClick={this.showcontactuspage}>Request for custom price</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <BusinessApp />
                    <div className="container busi-div" data-aos="fade-up" data-aos-duration="1000">
                        <div className="row m-5">
                            <div className="col-md-8">
                                <h2 className="section-heading text-center text-md-left p-0 m-0 font-s30">Ready to go-live in less than 7 days</h2>
                                <p className="color-lightblue rep-text-center">Setup your own While-label merchant app.</p>
                            </div>
                            <div className="col-md-4">
                                <div className="btnDiv rep-text-center">
                                    <button className="btn btn-blue-rounded" onClick={this.showcontactuspage}>Request for custom price</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <MoreHelp />
                    <Help />
                  </main>
                <Footer/>
            </>
        );
    }
}

export default PaymentGateway;
