import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";

// core components
import Navbar from "components/Navbars/Navbar-reg.js";
import SetPasswordForm from "components/Registration/SetPassword.js";

class SetPassword extends React.Component {
    componentDidMount() {
        // document.documentElement.scrollTop = 0;
        // document.scrollingElement.scrollTop = 0;
        // this.refs.main.scrollTop = 0;
        // const wow = new WOW();
        // wow.init();
    }

    freshChatClick = () => {
        // if (window.fcWidget.isOpen() !== true) {
        //     window.fcWidget.open();
        // }
    };

    render() {
        return (
            <>
                <div className="dv-main-SetPassword">
                    <Navbar />
                    <main ref="main" className="dv-main dv-login-container">
                        <SetPasswordForm />
                    </main>
                </div>
            </>
        );
    }
}

export default SetPassword;