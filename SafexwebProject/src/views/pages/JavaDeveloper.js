import React from "react";
//import Swiper from 'react-id-swiper';
//import 'swiper/css/swiper.css';
//import WOW from "wow.js";

// core components
import Navbar from "components/Navbars/Navbar.js";
import Footer from "components/Footers/Footer.js";
import HeroBanner from "components/HeroBanner/Career.js";
import {NavLink} from "reactstrap";
import ApplyFormJdnew from "components/ApplyFromJdnew/ApplyFormJdnew";
import {Link} from "react-router-dom";


class JavaDeveloper extends React.Component {
    componentDidMount() {
        // document.documentElement.scrollTop = 0;
        // document.scrollingElement.scrollTop = 0;
        // this.refs.main.scrollTop = 0;
        // const wow = new WOW();
        // wow.init();
    }

    freshChatClick = () => {
        // if (window.fcWidget.isOpen() !== true) {
        //     window.fcWidget.open();
        // }
    };

    render() {
        return (
            <>
                <Navbar />
                <main ref="main" className="dv-main">
                    <HeroBanner />
                </main>
                <div className="container mt-5">
                    <div className="row">
                        {/* <div className="col-12 text-right">
                            <a href="/career" className="btn-back">Back</a>
                        </div> */}
                        <div className="col-12">
                            <h2 className="font-w7 ubuntuMono op-txt">Open Position</h2>
                            <a href="/career" className="btn-back">Back</a>
                            <div className="clearfix"></div>
                            <h3 className="font26 border-bottom py-3 mb-3">
                                At safexpay we aspire to build a global,  digital platform
                                infrastructure and product company focused for every business,
                                with an extraordinary culture that attracts innovative,
                                collaborative people.
                              </h3>
                            <p>
                                <strong className="font-w6">
                                    <span className="color-blue mrg-10">Java Developer / Sr. Java Developer</span>
                                    Location: Mumbai <br />
Experience: Junior Developer 2+ years /  Senior Developer 4+ years  <br /> <br />
<span className="color-blue mrg-10"> Skills:</span>
Technical Skills: Java, J2EE, Spring MVC, Spring Boot, Hibernate, MySql/ Oracle, Angular JS, Jquery,
Javascript, HTML <br />
                                    Primary Skills:
Core Java, JSP, Spring, Hibernate, MySQL/Oracle, Jquery, Javascript <br />
                                    Secondary Skills:
Spring Boot, Mongo DB, Active MQ, AWS, CI/CD, Angular JS, HTML <br /><br />
                                  
<span class="color-blue mrg-10"> Job Description: </span>
</strong>

<ul className="career-points">
<li>Candidate should have experience in developing applications using JAVA/J2EE programming skills. </li>
<li>Strong in DB knowledge </li>
<li>Should be good with OOPS Concept </li>
<li>Should be good in design and application development using object oriented programming </li>
<li>Should be good in writing secure and optimized code </li>
<li>Experience in any application framework like Spring and ORM tools like Hibernate </li>
<li>Good understanding/experience on software development methodology such as Agile, TDD etc</li>
<li>Good understanding of source control tools and its use (SVN, Git etc...)</li>
<li>Experience in web services would be a plus but not mandatory </li>
<li>Experience in multithreaded applications and micro service based development would be a plus</li>
<li>Excellent written and verbal communication skills </li>
<li>Pro-active, self-learner, thorough, organized and energetic </li>
<li>Able to work across teams as well as independently </li>
<li>Perform code review of peers </li>
<li>Good in analytical and communication skills </li>
<li>Good experience in the Fintech domain (Payments/ Banking/ Wallets/ Insurance/ Financial Markets)</li>
</ul>
                              </p>                     
                        </div>
                    </div>

                </div>
                <ApplyFormJdnew pos="Java Developer" loc="mumbai" exp="2+yrs"/>
                <Footer />
            </>
        );
    }
}

export default JavaDeveloper;
