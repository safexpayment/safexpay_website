import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
//import Contact from "./components/ContactLogo/ContactLogo";

//import "assets/vendor/nucleo/css/nucleo.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "assets/vendor/font-awesome/css/font-awesome.min.css";
import "assets/scss/argon-design-system-react.scss?v1.1.0";
//import "assets/custom/css/style.scss";

import "jquery/dist/jquery.js";


import Index from "views/Index.js";
import AcceptPayments from "views/pages/AcceptPayments.js";
import MakePayouts from "views/pages/MakePayouts.js";
import DigitalBanking from "views/pages/DigitalBanking.js";
import PaymentGateway from "views/pages/PaymentGateway.js"; 
import Payouts from "views/pages/Payouts.js";
import Reseller from "views/pages/Reseller.js";
import Pricing from "views/pages/Pricing.js";
import AboutUs from "views/pages/AboutUs.js";
import PrivacyPolicy from "views/pages/PrivacyPolicy.js";
import TermsConditions from "views/pages/TermsConditions.js";
import Registration from "views/pages/Registration.js";
import Login from "views/pages/Login.js";
import SetPassword from "views/pages/SetPassword.js";
import BusinessDetails from "views/pages/Business.js";
import AddressDetails from "views/pages/Address.js";
import BankDetails from "views/pages/Bank.js";
import SuccessDetails from "views/pages/Success.js";
import WelcomeDetails from "views/pages/Welcome.js";
import PersonalDocIndDetails from "views/pages/PersonalDocInd.js";
import BusinessDocIndDetails from "views/pages/BusinessDocInd.js";
import PersonalDocPropDetails from "views/pages/PersonalDocProp.js";
import BusinessDocPropDetails from "views/pages/BusinessDocProp.js";
import PersonalDocPartDetails from "views/pages/PersonalDocPart.js";
import BusinessDocPartDetails from "views/pages/BusinessDocPart.js";
import PersonalDocPubDetails from "views/pages/PersonalDocPub.js";
import BusinessDocPubDetails from "views/pages/BusinessDocPub.js";
import PersonalDocSocDetails from "views/pages/PersonalDocSoc.js";
import BusinessDocSocDetails from "views/pages/BusinessDocSoc.js";
import AgreementDoc from "views/pages/Agreement.js";


import MyProfile from "views/pages/MyProfile.js";
import Career from "views/pages/Career.js";
import JavaDeveloper from "views/pages/JavaDeveloper.js";
import ProductManager from "views/pages/ProductManager.js";
import BusinessDevelopment from "views/pages/BusinessDevelopment.js";
import TeamLead from "views/pages/TeamLead.js";
import AppSupportEngineer from "views/pages/AppSupportEngineer.js";
import VPEngineering from "views/pages/VPEngineering.js";
import IOSDeveloper from "views/pages/IOSDeveloper.js";

import Supportus from "./components/Supportus/Supportus";

import Contactuspage from "components/Contactuspage/Contactuspage";

import ApplyNow from "components/ApplyNow/ApplyNow";

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route path="/" exact render={props => <Index {...props} />} />
      <Route
        path="/business/accept-payments"
        exact
        render={props => <AcceptPayments {...props} />}
      />
      <Route
        path="/business/payment-gateway"
        exact
        render={props => <PaymentGateway {...props} />}
      />
      <Route
        path="/business/make-payouts"
        exact
        render={props => <MakePayouts {...props} />}
      />
      <Route
        path="/business/digital-banking"
        exact
        render={props => <DigitalBanking {...props} />}
      />
      <Route
        path="/white-label/payment-gateway"
        exact
        render={props => <PaymentGateway {...props} />}
      />
      <Route
        path="/white-label/payouts"
        exact
        render={props => <Payouts {...props} />}
      />
      <Route
        path="/partner/reseller"
        exact
        render={props => <Reseller {...props} />}
      />
       <Route
        path="/pricing"
        exact
        render={props => <Pricing {...props} />}
      />
	    <Route
        path="/company/about-us"
        exact
        render={props => <AboutUs {...props} />}
      />
	    <Route
        path="/company/privacy-policy"
        exact
        render={props => <PrivacyPolicy {...props} />}
      />
	    <Route
        path="/company/terms-conditions"
        exact
        render={props => <TermsConditions {...props} />}
      />
	    <Route
        path="/register"
        exact
        render={props => <Registration {...props} />}
      />      
	    <Route
        path="/login"
        exact
        render={props => <Login {...props} />}
      />     
	    <Route
        path="/setpassword"
        exact
        render={props => <SetPassword {...props} />}
      />     
	    <Route
        path="/business"
        exact
        render={props => <BusinessDetails {...props} />}
      />    
	    <Route
        path="/address"
        exact
        render={props => <AddressDetails {...props} />}
      />    
	    <Route
        path="/bank"
        exact
        render={props => <BankDetails {...props} />}
      />    
	    <Route
        path="/success"
        exact
        render={props => <SuccessDetails {...props} />}
      />    
	    <Route
        path="/welcome"
        exact
        render={props => <WelcomeDetails {...props} />}
      />    
	    <Route
        path="/personalind"
        exact
        render={props => <PersonalDocIndDetails {...props} />}
      />    
	    <Route
        path="/businessind"
        exact
        render={props => <BusinessDocIndDetails {...props} />}
      />      
	    <Route
        path="/personalprop"
        exact
        render={props => <PersonalDocPropDetails {...props} />}
      />    
	    <Route
        path="/businessprop"
        exact
        render={props => <BusinessDocPropDetails {...props} />}
      />  
         
	    <Route
        path="/personalpart"
        exact
        render={props => <PersonalDocPartDetails {...props} />}
      />    
	    <Route
        path="/businesspart"
        exact
        render={props => <BusinessDocPartDetails {...props} />}
      />  
         
	    <Route
        path="/personalpub"
        exact
        render={props => <PersonalDocPubDetails {...props} />}
      />    
	    <Route
        path="/businesspub"
        exact
        render={props => <BusinessDocPubDetails {...props} />}
      />  
         
	    <Route
        path="/personalsoc"
        exact
        render={props => <PersonalDocSocDetails {...props} />}
      />    
	    <Route
        path="/businesssoc"
        exact
        render={props => <BusinessDocSocDetails {...props} />}
      />  
       <Route
        path="/agreementdoc"
        exact
        render={props => <AgreementDoc {...props} />}
      />    
	    <Route
        path="/myprofile"
        exact
        render={props => <MyProfile {...props} />}
      />      
	    <Route
        path="/career"
        exact
        render={props => <Career {...props} />}
      />        
	    <Route
        path="/career/java-developer"
        exact
        render={props => <JavaDeveloper {...props} />}
      />         
	    <Route
        path="/career/product-manager"
        exact
        render={props => <ProductManager {...props} />}
      />        
	    <Route
        path="/career/business-development"
        exact
        render={props => <BusinessDevelopment {...props} />}
      />  
      <Route
        path="/career/team-lead"
        exact
        render={props => <TeamLead {...props} />}
      /> 
      <Route
        path="/career/app-support-engineer"
        exact
        render={props => <AppSupportEngineer {...props} />}
      /> 
      <Route
        path="/career/vp-engineer"
        exact
        render={props => <VPEngineering {...props} />}
      /> 
      <Route
        path="/career/ios-developer"
        exact
        render={props => <IOSDeveloper {...props} />}
      /> 
     <Route
        path="/supportus"
        exact
        render={props => <Supportus {...props} />}
      />
      <Route
        path="/contactuspage"
        exact
        render={props => <Contactuspage {...props} />}
      />
      <Route
        path="/applynow"
        exact
        render={props => <ApplyNow {...props} />}
      />  
      <Redirect to="/" />
    </Switch>
   {/* <Contact/>*/}
  </BrowserRouter>,
  document.getElementById("root")
);

