import React from 'react';
import { Route } from 'react-router';
 
export default (
    <Route>
        <Route path='/' />
        <Route path="/business/accept-payments" />
        <Route path="/business/payment-gateway" />
        <Route path="/business/make-payouts" />
        <Route path="/business/digital-banking" />
        <Route path="/white-label/payment-gateway" />
        <Route path="/white-label/payouts" />
        <Route path="/partner/reseller" />
        <Route path="/pricing" />
	    <Route path="/company/about-us" />
	    <Route path="/company/privacy-policy" />
	    <Route path="/company/terms-conditions" />
	    <Route path="/register" />      
	    <Route path="/login" />     
	    <Route path="/setpassword" />     
	    <Route path="/business" />    
	    <Route path="/address" />    
	    <Route path="/bank" />    
	    <Route path="/success" />    
	    <Route path="/welcome" />    
	    <Route path="/personalind" />    
	    <Route path="/businessind" />      
	    <Route path="/personalprop" />    
	    <Route path="/businessprop" />  
	    <Route path="/personalpart" />    
	    <Route path="/businesspart" />  
	    <Route path="/personalpub" />    
	    <Route path="/businesspub" />  
	    <Route path="/personalsoc" />    
	    <Route path="/businesssoc" />  
        <Route path="/agreementdoc" />    
	    <Route path="/myprofile" />      
	    <Route path="/career" />        
	    <Route path="/career/java-developer" />         
	    <Route path="/career/product-manager" />        
	    <Route path="/career/business-development" />  
        <Route path="/career/team-lead" /> 
        <Route path="/career/app-support-engineer"/> 
        <Route path="/career/vp-engineer"/> 
        <Route path="/career/ios-developer" /> 
        <Route path="/supportus" />
        <Route path="/contactuspage" />
        <Route path="/applynow" />  
    </Route>
);